export const branch = {
  self: {
    get: (data: any) => ({
      type: "GET_BRANCH_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_BRANCH_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_BRANCH_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  }
};
