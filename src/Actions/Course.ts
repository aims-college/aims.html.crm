export const course = {
  self: {
    get: (data: any) => ({
      type: "GET_COURSE_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_COURSE_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_COURSE_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  type: {
    get: (data: any) => ({
      type: "GET_COURSE_TYPE_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_COURSE_TYPE_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_COURSE_TYPE_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  faculty: {
    get: (data: any) => ({
      type: "GET_FACULTY_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_FACULTY_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_FACULTY_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  create: {
    post: (data: any) => ({
      type: "CREATE_COURSE",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "CREATE_COURSE_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "CREATE_COURSE_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
};
