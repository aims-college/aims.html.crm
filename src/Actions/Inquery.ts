export const inquery = {
  self: {
    get: (data: any) => ({
      type: "GET_INQUIRY_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_INQUIRY_DATA_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "GET_INQUIRY_DATA_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  byId: {
    get: (data: any) => ({
      type: "GET_INQUIRY_BY_ID",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_INQUIRY_BY_ID_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "GET_INQUIRY_BY_ID_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  reports: {
    get: (data: any) => ({
      type: "GET_REPORTS_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_REPORTS_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_REPORTS_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  edit: {
    get: (data: any) => ({
      type: "EDIT_INQUIRY_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "EDIT_INQUIRY_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "EDIT_INQUIRY_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  update: {
    post: (data: any) => ({
      type: "UPDATE_INQUIRY",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "UPDATE_INQUIRY_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "UPDATE_INQUIRY_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  create: {
    post: (data: any) => ({
      type: "CREATE_INQUIRY",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "CREATE_INQUIRY_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "CREATE_INQUIRY_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  transfer: {
    get: (data: any) => ({
      type: "GET_TRANSFER_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_TRANSFER_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_TRANSFER_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  approve: {
    post: (data: any) => ({
      type: "GET_TRANSFER_APPROVE_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_TRANSFER_APPROVE_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_TRANSFER_APPROVE_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  type: {
    get: (data: any) => ({
      type: "GET_INQUIRY_TYPE_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_INQUIRY_TYPE_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_INQUIRY_TYPE_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
};
