export const payments = {
  all: {
    get: (data: any) => ({
      type: "GET_PAYMENTS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_PAYMENTS_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_PAYMENTS_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  save: {
    post: (data: any) => ({
      type: "SAVE_PAYMENT",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "SAVE_PAYMENT_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "SAVE_PAYMENT_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  paymentsByRegId: {
    get: (data: any) => ({
      type: "GET_PAYMENTS_BY_REG_ID",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_PAYMENTS_BY_REG_ID_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_PAYMENTS_BY_REG_ID_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  currencies: {
    get: (data: any) => ({
      type: "GET_ALL_CURRENCIES",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_ALL_CURRENCIES_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_ALL_CURRENCIES_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  busy: {
    get: (data: any) => ({
      type: "GET_BUSY_PAYMENTS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_BUSY_PAYMENTS_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_BUSY_PAYMENTS_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  bankAccounts: {
    get: (data: any) => ({
      type: "GET_BANK_ACCOUNTS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_BANK_ACCOUNTS_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_BANK_ACCOUNTS_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  outstanding: {
    get: (data: any) => ({
      type: "GET_OUTSTANDING",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_OUTSTANDING_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_OUTSTANDING_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  outstandingStudent: {
    get: (data: any) => ({
      type: "GET_OUTSTANDING_STUDENT",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_OUTSTANDING_STUDENT_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_OUTSTANDING_STUDENT_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  drawer: {
    close: () => ({
      type: "PRINT_DRAWER_CLOSE",
    }),
  },
};
