export const registration = {
  registrationNumber: {
    get: (data: any) => ({
      type: "GET_REGISTRATION_NUMBER_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_REGISTRATION_NUMBER_DATA_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "GET_REGISTRATION_NUMBER_DATA_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  discounts: {
    get: (data: any) => ({
      type: "GET_DISCOUNTS_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_DISCOUNTS_DATA_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "GET_DISCOUNTS_DATA_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  save: {
    post: (data: any) => ({
      type: "SAVE_REGISTRATION",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "SAVE_REGISTRATION_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "SAVE_REGISTRATION_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  update: {
    post: (data: any) => ({
      type: "UPDATE_REGISTRATION",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "UPDATE_REGISTRATION_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "UPDATE_REGISTRATION_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  all: {
    get: (data: any) => ({
      type: "GET_REGISTRATIONS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_REGISTRATIONS_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "GET_REGISTRATIONS_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  byId: {
    get: (data: any) => ({
      type: "GET_REGISTRATION_BY_ID",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_REGISTRATION_BY_ID_SUCCESS",
      payload: {
        data,
        isLoading: false,
      },
    }),
    fail: (data: any) => ({
      type: "GET_REGISTRATION_BY_ID_FAIL",
      payload: {
        data,
        isLoading: false,
      },
    }),
  },
  reports: {
    get: (data: any) => ({
      type: "GET_REGISTRATION_REPORTS_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_REGISTRATION_REPORTS_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_REGISTRATION_REPORTS_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  busy: {
    get: (data: any) => ({
      type: "GET_BUSY_REGISTRATIONS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_BUSY_REGISTRATIONS_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_BUSY_REGISTRATIONS_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  busyInstallments: {
    get: (data: any) => ({
      type: "GET_BUSY_INSTALLMENTS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_BUSY_INSTALLMENTS_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_BUSY_INSTALLMENTS_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  drawer: {
    close: () => ({
      type: "PRINT_DRAWER_CLOSE",
    }),
  },
};
