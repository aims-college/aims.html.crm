export const user = {
  users:{
    get: (data: any) => ({
      type: "GET_USERS_DATA",
      payload: {
        data,
        isLoading: true,
      },
    }),
    success: (data: any) => ({
      type: "GET_USERS_DATA_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "GET_USERS_DATA_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  },
  login:{
    post: (data: any) => ({
      type: "POST_LOGIN",
      payload: {
        data,
        isLoading: true,
      }
    }),
    success: (data: any) => ({
      type: "POST_LOGIN_SUCCESS",
      payload: {
        data,
        isLoading: true,
      },
    }),
    fail: (data: any) => ({
      type: "POST_LOGIN_FAIL",
      payload: {
        data,
        isLoading: true,
      },
    }),
  }
};
