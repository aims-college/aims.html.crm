import React from "react";
import { Layout } from "./Layout";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  NewInquery,
  EditInquery,
  Transfer,
  ViewInquery,
  ReportsInquery,
} from "Components/Inquery";
import { Home } from "Components/Inquery/Home";
import { New as NewRegistration } from "Components/Registration/New";
import { Edit as EditRegistration } from "Components/Registration/Edit";
import {
  RegistrationHome,
  ReportsRegistration,
} from "Components/Registration/";
import { Courses } from "Components/Course/";
import { Discounts } from "Components/Discounts/";
import {
  PaymentHome,
  ReportsOutstanding,
  ReportsOutstandingStudent,
} from "Components/Payments/";
import { BusyExportPay } from "Components/Payments/";
import {
  BusyExportReg,
  BusyExportInstallments,
} from "Components/Registration/";

import { DiscountApproval } from "Components/DiscountApproval/";

import { NewPayement as NewPayment } from "Components/Payments/New";
import { PrivateRoute } from "AuthRoute";

import { Welcome } from "./Welcome";
import { MenuTile } from "./MenuTile";

function App({ history }: any) {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact>
            <Welcome />
          </Route>
          <PrivateRoute path="/Home" exact component={MenuTile} />
          <Layout>
            <PrivateRoute
              path="/inqueries/:id(\d+)"
              exact
              component={EditInquery}
            />

            <PrivateRoute
              path="/inqueries/create"
              exact
              component={NewInquery}
            />

            <PrivateRoute
              path="/inqueries/transfer"
              exact
              component={Transfer}
            />

            <PrivateRoute
              path="/inqueries/:id(\d+)/view"
              exact
              component={ViewInquery}
            />

            <PrivateRoute
              path="/inqueries/reports"
              exact
              component={ReportsInquery}
            />

            <PrivateRoute path="/inqueries" exact component={Home} />

            <PrivateRoute
              path="/registrations"
              exact
              component={RegistrationHome}
            />
            <PrivateRoute
              path="/registrations/:id(\d+)"
              exact
              component={NewRegistration}
            />

            <PrivateRoute
              path="/registrations/:id(\d+)/edit"
              exact
              component={EditRegistration}
            />
            <PrivateRoute
              path="/registrations/reports"
              exact
              component={ReportsRegistration}
            />

            <PrivateRoute
              path="/discount-approval"
              exact
              component={DiscountApproval}
            />
            <PrivateRoute path="/payments" exact component={PaymentHome} />
            <PrivateRoute
              path="/payments-export"
              exact
              component={BusyExportPay}
            />
            <PrivateRoute
              path="/registrations-export"
              exact
              component={BusyExportReg}
            />
            <PrivateRoute
              path="/payments/outstanding"
              exact
              component={ReportsOutstanding}
            />
            <PrivateRoute
              path="/payments/outstanding/students"
              exact
              component={ReportsOutstandingStudent}
            />

            <PrivateRoute path="/payments/new" exact component={NewPayment} />
            <PrivateRoute path="/courses" exact component={Courses} />

            <PrivateRoute path="/discounts" exact component={Discounts} />
            <PrivateRoute
              path="/installments-export"
              exact
              component={BusyExportInstallments}
            />
          </Layout>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
