import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Popconfirm,
  Menu,
  PageHeader,
  Table,
  Tag,
  Input,
  Drawer,
  Row,
  Col,
  Space,
  message,
} from "antd";
import {
  $Select,
  $Input,
  $DatePicker,
  $RangePicker,
  $AmountLabel,
  $Radio,
} from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import {
  DownOutlined,
  PhoneOutlined,
  MailOutlined,
  PlusCircleFilled,
} from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import * as API from "Services";

import moment from "moment";
const { TextArea } = Input;
const Courses = ({
  getFaculties,
  course,
  getCourses,
  getUsers,
  createCourse,
}: any) => {
  const [filter, setFilter] = useState({
    faculty_id: "all",
  });

  const [confirmComment, setConfirmComment] = useState<string>("");
  const [visible, setVisible] = useState<boolean>(false);
  const [visibleEdit, setVisibleEdit] = useState<boolean>(false);

  const history = useHistory();

  useEffect(() => {
    getFaculties();
    getCourses();
    getUsers();
  }, []);

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "",
        dataIndex: "id",
        key: "id",
      },
      {
        title: "Course Name",
        dataIndex: "name",
        key: "name",
      },
      {
        title: "Duration",
        dataIndex: "duration",
        key: "duration",
        width: 110,
      },
      {
        title: "Course Fee",
        dataIndex: "course_fee",
        key: "course_fee",
        render: (text: string, record: any) => {
          return <$AmountLabel value={record.course_fee} />;
        },
        width: 110,
      },
      {
        title: "Course Code",
        dataIndex: "code",
        key: "code",
        width: 110,
      },
      {
        title: "Faculty Name",
        dataIndex: "faculty_name",
        key: "faculty_name",
        width: 110,
      },
      {
        title: "Course Level",
        dataIndex: "course_level",
        key: "course_level",
        width: 100,
      },
      {
        title: "Course Type",
        dataIndex: "course_type_name",
        key: "course_type_name",
        width: 100,
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status",
        render: (text: string) => {
          return text == "1" ? (
            <Tag color="blue">Active</Tag>
          ) : (
            <Tag color="red">InActive</Tag>
          );
        },
        width: 100,
      },
    ];
  };

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const showDrawerEdit = () => {
    setVisibleEdit(true);
  };

  const onCloseEdit = () => {
    setVisibleEdit(false);
  };

  return (
    <>
      <Formik
        initialValues={{
          no_of_installments: 0,
          name: "",
          code: "",
          description: "",
          faculty_id: "",
          registration_fee: 0,
          course_fee: 0,
          duration: 0,
          course_level: null,
          isEdit: false,
          courseId: 0,
        }}
        onSubmit={(values) => {
          if (values.isEdit) {
            API.course.edit
              .post(values)
              .then(() => {
                message.success("Course Updated Success");
                onCloseEdit();
                getCourses();
              })
              .catch(() => message.warn("Course Updated Failed"));
          } else {
            createCourse(values);
          }
        }}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Courses"
                    extra={[
                      <Button
                        size="small"
                        type="primary"
                        shape="round"
                        icon={<PlusCircleFilled />}
                        onClick={() => showDrawer()}
                      >
                        Create Course
                      </Button>,
                    ]}
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={48}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$Select
                        name="faculty_id"
                        size="small"
                        formitem={{
                          label: "Faculty",
                        }}
                        options={course.faculties.data}
                        optionValue="id"
                        optionText="faculty"
                        defaultValue="all"
                        value={filter.faculty_id}
                        allOption={true}
                        onChange={(faculty_id: any) => {
                          if (faculty_id == "all") {
                            setFilter({ ...filter, faculty_id: "all" });
                            getCourses({ faculty_id: "" });
                          } else {
                            setFilter({ ...filter, faculty_id });
                            getCourses({ faculty_id });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={course.courses.data}
                      loading={course.courses.isLoading}
                      rowKey={(record) => record.id}
                      onRow={(record, rowIndex) => {
                        console.log(record);
                        return {
                          onDoubleClick: (event) => {
                            setFieldValue("course_id", record.id);
                            setFieldValue("status", record.status);
                            setFieldValue("name", record.name);
                            setFieldValue("code", record.code);
                            setFieldValue(
                              "faculty_id",
                              Number(record.faculty_id)
                            );
                            setFieldValue("type_id", record.type_id);
                            setFieldValue("description", record.description);
                            setFieldValue(
                              "registration_fee",
                              record.registration_fee
                            );
                            setFieldValue("course_fee", record.course_fee);
                            setFieldValue("duration", record.duration);
                            setFieldValue("course_level", record.course_level);
                            setFieldValue("isEdit", true);
                            showDrawerEdit();
                          },
                        };
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>

            <Drawer
              title="Create Course"
              width={720}
              closable={false}
              onClose={onClose}
              visible={visible}
              footer={
                <Space>
                  <Button onClick={onClose}>Cancel</Button>
                  <Button onClick={() => handleSubmit({ ...values, isEdit: false })} type="primary">
                    Save
                  </Button>
                </Space>
              }
            >
              <Form layout="vertical">
                <Row gutter={24}>
                  <Col span={16}>
                    <Form.Item>
                      <$Input label="Course Name" name="name" required />
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item>
                      <$Input label="Course Code" name="code" required />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={14}>
                    <Form.Item>
                      <$Input label="Course Description" name="description" />
                    </Form.Item>
                  </Col>
                  <Col span={1}></Col>
                  <Col span={7}>
                    <Form.Item>
                      <$Select
                        required
                        name="faculty_id"
                        size="medium"
                        formitem={{
                          label: "Faculty",
                        }}
                        options={course.faculties.data}
                        optionValue="id"
                        optionText="faculty"
                        allOption={false}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <Form.Item>
                      <$Input label="Duration" name="duration" required />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item>
                      <$Input label="Course Fee" name="course_fee" required />
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={12}>
                    <Form.Item label="Course Type" required>
                      <$Radio
                        options={[
                          { value: "1", label: "Diploma" },
                          { value: "2", label: "Bachelors" },
                          { value: "3", label: "Masters" },
                          { value: "4", label: "Doctorate" },
                          { value: "7", label: "Certificate" },
                        ]}
                        optionType="button"
                        optionText="label"
                        optionValue="value"
                        buttonStyle="solid"
                        value={values.type_id}
                        radioButton={true}
                        name="type_id"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item label="Course Level" required>
                      <$Radio
                        options={[
                          { value: "postgradudate", label: "postgradudate"},
                          { value: "undergraduate", label: "undergraduate"},
                          { value: "cambridge", label: "cambridge"},
                        ]}
                        optionType="button"
                        optionText="label"
                        optionValue="value"
                        buttonStyle="solid"
                        radioButton={true}
                        value={values.course_level}
                        name="course_level"
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Drawer>

            <Drawer
              title="Edit Course"
              width={720}
              closable={true}
              onClose={onCloseEdit}
              visible={visibleEdit}
              footer={
                <Space>
                  <Button onClick={onCloseEdit}>Cancel</Button>
                  <Button
                    onClick={() => handleSubmit({ ...values, isEdit: true })}
                    type="primary"
                  >
                    Update
                  </Button>
                </Space>
              }
            >
              <Form layout="vertical">
                <Row gutter={24}>
                  <Col span={16}>
                    <Form.Item>
                      <$Input label="Course Name" name="name" required />
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item>
                      <$Input label="Course Code" name="code" required />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={16}>
                    <Form.Item>
                      <$Input label="Course Description" name="description" />
                    </Form.Item>
                  </Col>
                  <Col span={1}></Col>
                  <Col span={7}>
                    <Form.Item>
                      <$Select
                        required
                        name="faculty_id"
                        size="medium"
                        formitem={{
                          label: "Faculty",
                        }}
                        options={course.faculties.data}
                        optionValue="id"
                        optionText="faculty"
                        allOption={false}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <Form.Item>
                      <$Input label="Duration" name="duration" required />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item>
                      <$Input label="Course Fee" name="course_fee" required />
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={24}>
                    <Form.Item label="Course Type" required>
                      <$Radio
                        options={[
                          { value: "1", label: "Diploma" },
                          { value: "2", label: "Bachelors" },
                          { value: "3", label: "Masters" },
                          { value: "4", label: "Doctorate" },
                          { value: "7", label: "Certificate" },
                        ]}
                        optionType="button"
                        optionText="label"
                        optionValue="value"
                        buttonStyle="solid"
                        radioButton={true}
                        value={values.type_id}
                        disabled
                        name="type_id"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item label="Course Level" required>
                      <$Radio
                        options={[
                          { value: "postgradudate", label: "postgradudate"},
                          { value: "undergraduate", label: "undergraduate"},
                          { value: "cambridge", label: "cambridge"},
                        ]}
                        optionType="button"
                        optionText="label"
                        optionValue="value"
                        buttonStyle="solid"
                        radioButton={true}
                        value={values.course_level}
                        disabled
                        name="course_level"
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Drawer>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { course, user } = state;

  return {
    course,
    user,
  };
};

const mapDispatchToProps = {
  getCourses: Actions.course.self.get,
  getUsers: Actions.user.users.get,
  getFaculties: Actions.course.faculty.get,
  createCourse: Actions.course.create.post,
};

export default connect(mapStateToProps, mapDispatchToProps)(Courses);
