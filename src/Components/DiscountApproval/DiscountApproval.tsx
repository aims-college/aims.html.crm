import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Popconfirm,
  Menu,
  PageHeader,
  Table,
  Tag,
  Input,
  message,
} from "antd";
import { $Select, $Input, $DatePicker, $RangePicker } from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import { registration } from "Services";
const { TextArea } = Input;

const DiscountApproval = ({
  getTransfers,
  inquiry,
  getInqueryTypes,
  course,
  getCourses,
  getUsers,
  user,
}: any) => {
  const [filter, setFilter] = useState({
    nic: "",
    status: "pending",
    name: "",
    inquiry_type_id: "",
    course_id: "",
    contact_no: "",
    start_date: moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });

  const [confirmComment, setConfirmComment] = useState<string>("");
  const [approvals, setApprovals] = useState<Array<any>>([]);
  const [isApprovalsLoading, setApprovalsLoading] = useState<boolean>(false);

  const history = useHistory();

  const approveTransfer = (params: any) => {
    registration.approvedOrRejectedDiscount
      .post(params)
      .then(() => {
        message.success("Successfully change the status");
        getApprovals();
      })
      .catch(() => {
        message.warn("Something wrong!");
      });
  };

  const getApprovals = () => {
    setApprovalsLoading(true);
    registration.approval
      .get()
      .then((response) => {
        if (response.hasOwnProperty("Error")) {
          message.warning("Unauthorized");
          setApprovals([]);
          setApprovalsLoading(false);
          return;
        }

        setApprovals(response.data);
        setApprovalsLoading(false);
      })
      .catch(() => {
        setApprovals([]);
        setApprovalsLoading(false);
      });
  };

  useEffect(() => {
    getApprovals();
  }, []);

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "Inquiry Id",
        dataIndex: "inquiry_id",
        key: "inquiry_id",
        render: (text: string) => {
          return <Tag>{`INQ-${String(text).padStart(5, "0")}`}</Tag>;
        },
        width: 80,
      },
      {
        title: "Registration No",
        key: "student_registration_id",
        dataIndex: "student_registration_id",
        render: (text: any, record: any, index: number) => {
          return text ? (
            <Tag className={`tag-status-${text}`}>{text}</Tag>
          ) : (
            "N/A"
          );
        },
        width: 110,
        ellipsis: true,
      },

      {
        title: "Course Fee",
        dataIndex: "course_fee",
        key: "course_fee",
        width: 110,
      },
      {
        title: "Course Duration",
        dataIndex: "course_duration",
        key: "course_duration",
        width: 110,
      },
      {
        title: "Discount Amount",
        dataIndex: "discount_amount",
        key: "discount_amount",
        width: 110,
      },
      {
        title: "Reason",
        dataIndex: "status_reason",
        key: "status_reason",
        width: 110,
      },
      {
        title: "Status",
        dataIndex: "discount_approval_status",
        key: "discount_approval_status",
        width: 110,
        render: (text: any, record: any, index: number) => {
          if (text == 0) {
            return <Tag color={"yellow"}>pending</Tag>;
          }

          if (text == 1) {
            return <Tag color={"green"}>Approved</Tag>;
          }

          if (text == 2) {
            return <Tag color={"red"}>Rejected</Tag>;
          }
        },
      },
      {
        title: "",
        key: "confirm",
        dataIndex: "isConfirm",
        width: 100,
        render: (text: any, record: any, index: number) => (
          <>
            <Popconfirm
              disabled={
                record.discount_approval_status == 2 ||
                record.discount_approval_status == 1
              }
              title={
                <>
                  <TextArea
                    rows={2}
                    onChange={(e) => {
                      setConfirmComment(e.target.value);
                    }}
                  ></TextArea>
                </>
              }
              okButtonProps={{
                disabled: confirmComment.length === 0,
                onClick: () => {
                  setConfirmComment("");
                  approveTransfer({
                    discount_approval_id: record.id,
                    discount_approval_status: 1,
                    status_reason: confirmComment,
                  });
                },
              }}
              cancelButtonProps={{
                disabled: confirmComment.length === 0,
                onClick: () => {
                  setConfirmComment("");
                  approveTransfer({
                    discount_approval_id: record.id,
                    discount_approval_status: 2,
                    status_reason: confirmComment,
                  });
                },
                danger: true,
              }}
              okText="Approve"
              cancelText="Reject"
            >
              <Button
                type="primary"
                size="small"
                disabled={
                  record.discount_approval_status == 2 ||
                  record.discount_approval_status == 1
                }
              >
                Take an Action
              </Button>
            </Popconfirm>
          </>
        ),
      },
    ];
  };

  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Discount Approval"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={approvals}
                      loading={isApprovalsLoading}
                      rowKey={(record) => record.id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, transfer } = state;

  return {
    inquiry,
    transfer,
    course,
    user,
  };
};

const mapDispatchToProps = {
  getTransfers: Actions.inquery.transfer.get,
  getInqueryTypes: Actions.inquery.type.get,
  getCourses: Actions.course.self.get,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(DiscountApproval);
