import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Popconfirm,
  Menu,
  PageHeader,
  Table,
  Tag,
  Input,
  Drawer,
  Row,
  Col,
  Space,
  message,
} from "antd";
import {
  $Select,
  $Input,
  $DatePicker,
  $RangePicker,
  $AmountLabel,
  $Radio,
  $TextArea,
} from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import {
  DownOutlined,
  PhoneOutlined,
  MailOutlined,
  PlusCircleFilled,
} from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import { discounts } from "Services";
import Validations from "./Validations";

const { TextArea } = Input;
const Courses = ({
  getFaculties,
  course,
  getCourses,
  getUsers,
  createCourse,
}: any) => {
  const [filter, setFilter] = useState({
    faculty_id: "",
  });

  const [confirmComment, setConfirmComment] = useState<string>("");
  const [visible, setVisible] = useState<{
    isOpen: boolean;
    title: string;
    isNew: boolean;
  }>({
    isOpen: false,
    isNew: false,
    title: "Create Discount",
  });
  const [data, setData] = useState<Array<any>>([]);

  const history = useHistory();

  useEffect(() => {
    getFaculties();
    getCourses();
    getUsers();

    discounts.self.get({}).then(({ data }: any) => {
      setData(data);
    });
  }, []);

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "",
        dataIndex: "id",
        key: "id",
      },
      {
        title: "Discount Name",
        dataIndex: "name",
        key: "name",
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description",
      },
      {
        title: "Discount",
        dataIndex: "discount",
        key: "discount",
        width: 80,
      },
      {
        title: "Start Date",
        dataIndex: "start_date",
        key: "start_date",
        render: (text: string, record: any) => {
          return <Tag color="blue">{moment(text).format("YYYY/MM/DD")}</Tag>;
        },
        width: 100,
      },
    ];
  };

  const showNewDrawer = () => {
    setVisible({ isOpen: true, isNew: true, title: "Create Discount" });
  };

  const onClose = () => {
    setVisible({ isOpen: false, title: "Create Discount", isNew: false });
  };

  return (
    <>
      <Formik
        initialValues={{
          name: "",
          course_id: 1,
          discount: "",
          start_date: "",
          status: 1,
          id: -1,
          end_date: moment()
        }}
        validateOnBlur
        validateOnChange
        validateOnMount
        validationSchema={Validations}
        onSubmit={(values) => {
          if (visible.isNew) {
            discounts.create
              .post(values)
              .then(() => {
                message.success("Discount created Successfully");
                onClose();
              })
              .catch(() => {
                message.warn("Discount created Unsuccessfully");
              });
          } else if (!visible.isNew) {
            discounts.update
              .post({...values, discount_id: values.id})
              .then(() => {
                message.success("Discount updated Successfully");
                onClose();
              })
              .catch(() => {
                message.warn("Discount updated Unsuccessfully");
              });
          }
        }}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Discounts"
                    extra={[
                      <Button
                        size="small"
                        type="primary"
                        shape="round"
                        icon={<PlusCircleFilled />}
                        onClick={() => showNewDrawer()}
                      >
                        Create Discount
                      </Button>,
                    ]}
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={48}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$Select
                        name="faculty_id"
                        size="small"
                        formitem={{
                          label: "Faculty",
                        }}
                        options={course.faculties.data}
                        optionValue="id"
                        optionText="faculty"
                        defaultValue="all"
                        value={filter.faculty_id}
                        allOption={true}
                        onChange={(faculty_id: any) => {
                          if (faculty_id == "all") {
                            setFilter({ ...filter, faculty_id: "" });
                            getCourses({ faculty_id });
                          } else {
                            setFilter({ ...filter, faculty_id });
                            getCourses({ faculty_id });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={data}
                      loading={course.courses.isLoading}
                      rowKey={(record) => record.id}
                      onRow={(record, rowIndex) => {
                        return {
                          onDoubleClick: (event) => {
                            setFieldValue("name", record.name);
                            setFieldValue("discount", record.discount);
                            setFieldValue("start_date", record.start_date);
                            setVisible({
                              isOpen: true,
                              isNew: false,
                              title: "Edit Discount",
                            });
                          },
                        };
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>

            <Drawer
              title={visible.title}
              width={720}
              closable={false}
              onClose={onClose}
              visible={visible.isOpen}
              footer={
                <Space>
                  <Button onClick={onClose}>Cancel</Button>
                  <Button onClick={handleSubmit} type="primary">
                    Save
                  </Button>
                </Space>
              }
            >
              <Form layout="vertical">
                <Row gutter={24}>
                  <Col span={8}>
                    <Form.Item>
                      <$Input
                        label="Discount Name"
                        name="name"
                        required
                        value={values.name}
                        disabled={!visible.isNew}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={16}>
                    <Form.Item>
                      <$TextArea
                        label="Description"
                        name="description"
                        required
                        value={values.description}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={4}>
                    <Form.Item>
                      <$Input
                        label="Discount"
                        name="discount"
                        required
                        value={values.discount}
                        suffix={"%"}
                        disabled={!visible.isNew}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={8}></Col>
                </Row>

                <Row>
                  <Col span={16}>
                    <Form.Item>
                      <$DatePicker
                        name="start_date"
                        value={values.start_date}
                        label="Start Date"
                        required
                        disabled={!visible.isNew}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={16}>
                    <Form.Item label='Status'>
                      <$Radio
                        options={[
                          { value: 1, label: "Active" },
                          { value: 0, label: "Deactive" },
                        ]}
                        optionType="button"
                        optionText="label"
                        optionValue="value"
                        buttonStyle="solid"
                        radioButton={true}
                        size="small"
                        defaultValue={1}
                        name="status"
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Drawer>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { course, user } = state;

  return {
    course,
    user,
  };
};

const mapDispatchToProps = {
  getCourses: Actions.course.self.get,
  getUsers: Actions.user.users.get,
  getFaculties: Actions.course.faculty.get,
  createCourse: Actions.course.create.post,
};

export default connect(mapStateToProps, mapDispatchToProps)(Courses);
