import { message } from "antd";
import * as Yup from "yup";

export default () => {
  return Yup.object().shape({
    name: Yup.string()
      .required("Name is required")
      .typeError("Name is invalid"),
    discount: Yup.number()
      .required("Discount is required")
      .typeError("Discount is invalid"),
    start_date: Yup.date().required("Start Date is required").typeError("Start Date is invalid"),
  });
};
