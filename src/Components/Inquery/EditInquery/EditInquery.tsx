import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import { $Select, $Input, $DatePicker, $DateLabel } from "Components/antd";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Upload,
  Form,
  Input,
  Row,
  Col,
  Select,
  Modal,
  Skeleton,
  Typography,
  Button,
  Timeline,
  Drawer,
  Tag,
  message,
  Divider,
  Image,
  Card,
  Alert,
} from "antd";
import "./NewInquery.scss";
import { connect } from "react-redux";
import {
  DownOutlined,
  PlusOutlined,
  ClockCircleOutlined,
} from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import ValidationScema from "./Validations";
import { checkURL } from "Functions";

const EditInquery: React.FC<any> = ({
  inquiry,
  getInqueryTypes,
  editInquery,
  course,
  updateInquery,
  getUsers,
  user,
}: any) => {
  const { Option } = Select;
  const [fetching, setFetching] = React.useState(false);
  const { Dragger } = Upload;
  const history = useHistory();
  const [status, setStatus] = useState("pending");
  const [singleCommentEnable, setSingleCommentEnable] =
    useState<boolean>(false);

  const [visibleHandlerDrawer, setVisibleHandlerDrawer] =
    useState<boolean>(false);

  const [upload, setUpload] = useState({
    previewVisible: false,
    previewImage: "",
    previewTitle: "",
    fileList: [],
  });

  const handleCancel = () => setUpload({ ...upload, previewVisible: false });

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setUpload({
      ...upload,
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf("/") + 1),
    });
  };

  const handleChange = ({ fileList }: any) => {
    setUpload({ ...upload, fileList });
  };

  useEffect(() => {
    if (history.location.state) {
      editInquery(history.location.state as any);
      const { status, inquiry_followups } = history.location.state as any;
      setStatus(status);
      getUsers();
    }
  }, [history.location.key]);

  function getBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  const { TextArea } = Input;
  const statusMenu = (
    <Menu
      onClick={(e) => {
        setStatus(e.key.toString());
      }}
    >
      <Menu.Item key="pending">
        <Tag className="tag-status-pending">Pending</Tag>
      </Menu.Item>
      <Menu.Item key="non_registered">
        <Tag className="tag-status-non_registered">Non Registered</Tag>
      </Menu.Item>
      <Menu.Item key="not_answered">
        <Tag className="tag-status-not_answered">Not Answered</Tag>
      </Menu.Item>
    </Menu>
  );

  const Editor = ({ onChange, onSubmit, submitting, value, values }: any) => (
    <>
      <Form.Item>
        <TextArea rows={4} onChange={onChange} value={value} />
      </Form.Item>
      <div className="d-flex align-items-center">
        <div className="mr-3">
          <Form.Item>
            <$DatePicker
              name={`postponed_date`}
              value={values?.postponed_date}
              label="Date to be Postpone"
              disabledDate={(d: any) => d < moment()}
              onBlur={() => {
                values?.postponed_date && setStatus("postponed");
              }}
            />
          </Form.Item>
        </div>
        <div>
          <Form.Item>
            <Button
              className="mt-2"
              htmlType="submit"
              type="primary"
              onClick={onSubmit}
              disabled={!value}
            >
              Add Follow-up
            </Button>
          </Form.Item>
        </div>
      </div>
    </>
  );

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const updateData = (values: any) => {
    const inquiry_handler = [];

    /**
     * @description no assinger
     */

    /**
     * @description new assinger
     */

    if (
      values.assignToId &&
      values.authUserId &&
      values.assignToId !== values.authUserId
    ) {
      // auth user
      inquiry_handler.push({
        front_officer_id: values.authUserId,
        status: "requested",
        status_reason: "", // need to update this line
        inquiry_handler_id: values.inquiry_handlers?.filter(
          (data: any) => data.status == "handling"
        )[0].id,
      });

      //assign to
      inquiry_handler.push({
        front_officer_id: values.assignToId,
        status: "pending",
        status_reason: null,
      });
    }

    //inquiry references

    const inquiry_references = () => {
      if (upload && Array.isArray(upload.fileList)) {
        return upload.fileList.map(
          ({ name, status, response: { file_path } }: any) => {
            if (status === "done") {
              return {
                document_name: name,
                description: name,
                url: file_path,
                is_new: 1,
                is_deleted: 0,
              };
            }
          }
        );
      } else {
        return [];
      }
    };

    return {
      inquiry_id: values.id,
      ...values,
      status,
      inquiry_references: [...inquiry_references()],
      inquiry_followups: values.inquiry_followups.filter(
        ({ isNew }: { isNew: boolean }) => isNew === true
      ),
      inquiry_handler,
    };
  };
  const d: any = [
    {
      uid: "2",
      name: "yyy.png",
      status: "done",
      url: "http://www.baidu.com/yyy.png",
    },
  ];

  return (
    <>
      <Formik
        initialValues={{
          ...inquiry.edit.data,
          comment: "",
          inquiry_references: inquiry.edit.data?.inquiry_references?.map(
            ({ url, id, document_name }: any) => ({
              url,
              uid: id.toString(),
              document_name: document_name,
            })
          ),
          postponed_date: null,
          inquiry_handler: [],
          authUserId: Number(localStorage.getItem("user_id")),
          assignToId: Number(
            inquiry.edit.data?.inquiry_handlers?.filter(
              (data: any) => data.status == "handling"
            )[0]?.front_officer_id
          ),
        }}
        enableReinitialize
        validateOnBlur
        validateOnMount
        validateOnChange
        validationSchema={ValidationScema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            updateInquery(updateData(values));
          } catch (error) {
            console.error(error);
          }
        }}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
          dirty,
          isValid,
          errors,
        }: any) => (
          <>
            <div className="space-content pb-5">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title={`Edit Lead - INQ-${
                      (values?.id && String(values?.id).padStart(5, "0")) || 0
                    }`}
                    subTitle={
                      values.inquiry_date && (
                        <$DateLabel value={values.inquiry_date} />
                      )
                    }
                    extra={[
                      <Dropdown overlay={statusMenu} key="1">
                        <div
                          className="status-dropdown-link tag-status-pending d-flex align-items-center"
                          onClick={(e) => e.preventDefault()}
                        >
                          <div className="flex-fill">{status}</div>{" "}
                          <DownOutlined />
                        </div>
                      </Dropdown>,
                    ]}
                  />
                </div>
              </Affix>
              <Skeleton loading={inquiry.edit.isLoading}>
                <div className="mt-4">
                  <Form layout="vertical">
                    <div className="d-flex">
                      <div className="flex-fill edit-left-inq">
                        <Affix offsetTop={56} style={{ zIndex: 0 }}>
                          <Row gutter={24} className="mb-5">
                            <Col span={12}>
                              <div>
                                <$Select
                                  name="inquiry_type_id"
                                  size="medium"
                                  formitem={{
                                    label: "Lead Source",
                                  }}
                                  options={inquiry.type.data}
                                  optionValue="id"
                                  optionText="description"
                                  defaultValue="all"
                                  allOption={true}
                                  disabled
                                />
                              </div>
                              <div className="d-flex">
                                <Form.Item
                                  label="Contact Number"
                                  required
                                  style={{ width: 180 }}
                                >
                                  <$Input name="contact_no" paceholder='0777123456'/>
                                </Form.Item>
                              </div>
                              <div>
                                <Form.Item label="Lead Name" required>
                                  <div className="d-flex">
                                    <div>
                                      <$Select
                                        style={{ width: 80 }}
                                        className="mr-1"
                                        name="inquirer_type"
                                        size="medium"
                                        options={[
                                          {
                                            label: "Mr",
                                            value: "Mr",
                                          },
                                          {
                                            label: "Mrs",
                                            value: "Mrs",
                                          },
                                          {
                                            label: "Miss",
                                            value: "Miss",
                                          },
                                          {
                                            label: "Ven",
                                            value: "Ven",
                                          },
                                        ]}
                                        optionValue="value"
                                        optionText="label"
                                        allOption={false}
                                      />
                                    </div>
                                    <div className="flex-fill">
                                      {" "}
                                      <$Input name="inquirer_name"  />
                                    </div>
                                  </div>
                                </Form.Item>
                              </div>
                              <div className="d-flex">
                                <Form.Item
                                  label="NIC/Passport"
                                  required
                                  style={{ width: 180 }}
                                >
                                  <$Input name="nic" />
                                </Form.Item>
                              </div>
                              <div>
                                <Form.Item label="Email">
                                  <$Input name="email" />
                                </Form.Item>
                              </div>
                              <div>
                                <$Select
                                  name="course_id"
                                  size="medium"
                                  formitem={{
                                    label: "Programe",
                                  }}
                                  options={course.courses.data.filter(({status}:{status:string}) => status === "1")}
                                  optionValue="id"
                                  optionText="name"
                                  allOption={false}
                                  required
                                />
                              </div>
                            </Col>
                            <Col span={12}>
                              <div className="d-flex justify-content-between align-items-center">
                                <h3>Transfer History</h3>
                                <Button
                                  size="small"
                                  type="link"
                                  onClick={(e: any) =>
                                    setVisibleHandlerDrawer(true)
                                  }
                                >
                                  Check
                                </Button>
                              </div>
                              <Avatar.Group>
                                {values?.inquiry_handlers?.map(
                                  ({
                                    assigned_date,
                                    handler_name,
                                    status_reason,
                                    ...restData
                                  }: any) => {
                                    return (
                                      <Avatar
                                        key={restData.id}
                                        style={{
                                          backgroundColor: `#ccc`,
                                        }}
                                      >
                                        {handler_name &&
                                          handler_name.charAt(0).toUpperCase()}
                                        {handler_name &&
                                          handler_name.split(" ")[1] &&
                                          handler_name
                                            .split(" ")[1]
                                            .charAt(0)
                                            .toUpperCase()}
                                      </Avatar>
                                    );
                                  }
                                )}
                              </Avatar.Group>

                              <Drawer
                                title="Transfer Timeline"
                                placement={"right"}
                                width={500}
                                onClose={(e: any) =>
                                  setVisibleHandlerDrawer(false)
                                }
                                visible={visibleHandlerDrawer}
                              >
                                <Timeline mode="left">
                                  {values?.inquiry_handlers?.map(
                                    ({
                                      assigned_date,
                                      handler_name,
                                      status_reason,
                                      id,
                                      ...restData
                                    }: any) => {
                                      return (
                                        <Timeline.Item
                                          key={id}
                                          color="green"
                                          dot={
                                            restData.status == "pending" && (
                                              <ClockCircleOutlined
                                                style={{ fontSize: "16px" }}
                                              />
                                            )
                                          }
                                        >
                                          <div className="d-flex flex-column pb-2">
                                            <div className="d-flex justify-content-between mb-2">
                                              <div className="font-weight-bold">
                                                <$DateLabel
                                                  value={assigned_date}
                                                />
                                              </div>
                                              <div>
                                                {" "}
                                                <Tag className="m-0">
                                                  {restData.status} on{" "}
                                                </Tag>
                                              </div>
                                            </div>
                                            <div className="d-flex align-items-center">
                                              <Avatar
                                                className="mr-2"
                                                style={{
                                                  backgroundColor: `#ccc`,
                                                }}
                                              >
                                                {handler_name &&
                                                  handler_name
                                                    .charAt(0)
                                                    .toUpperCase()}
                                                {handler_name &&
                                                  handler_name.split(" ")[1] &&
                                                  handler_name
                                                    .split(" ")[1]
                                                    .charAt(0)
                                                    .toUpperCase()}
                                              </Avatar>{" "}
                                              {handler_name && (
                                                <span>{handler_name}</span>
                                              )}
                                            </div>
                                            <Divider
                                              className="m-0 mt-1 mb-1"
                                              dashed
                                            />
                                            <div
                                              className="text-muted"
                                              style={{ paddingLeft: "2.5rem" }}
                                            >
                                              {status_reason && status_reason}
                                            </div>
                                          </div>
                                        </Timeline.Item>
                                      );
                                    }
                                  )}
                                </Timeline>
                              </Drawer>

                              <div>
                                <$Select
                                  name="assignToId"
                                  size="medium"
                                  formitem={{
                                    label: "Assign to",
                                  }}
                                  options={user.users.data}
                                  optionValue="id"
                                  optionText="fullName"
                                  defaultValue={"self"}
                                  allOption={false}
                                  disabled={status == "registered"}
                                />
                              </div>
                              <div>
                                <Form.Item label="Reference">
                                  <div>
                                    {typeof values?.inquiry_references !=
                                      "undefined" &&
                                      values?.inquiry_references.length > 0 && (
                                        <Card
                                          title="Uploaded List"
                                          className="mb-3"
                                          size="small"
                                          bordered
                                        >
                                          {typeof values?.inquiry_references !=
                                            "undefined" &&
                                            values?.inquiry_references.length >
                                              0 &&
                                            Array.isArray(
                                              values?.inquiry_references
                                            ) &&
                                            values?.inquiry_references?.map(
                                              (item: any) => (
                                                <Card.Grid
                                                  style={{
                                                    width: "25%",
                                                    textAlign: "center",
                                                  }}
                                                  key={item.uid}
                                                >
                                                  {!checkURL(item.url) &&
                                                    item.document_name}
                                                  {checkURL(item.url) && (
                                                    <Image src={item.url} />
                                                  )}
                                                </Card.Grid>
                                              )
                                            )}
                                        </Card>
                                      )}

                                    <Upload
                                      action={`${process.env.REACT_APP_API_URL}/file_upload`}
                                      headers={{
                                        Authorization: `Bearer ${localStorage.getItem(
                                          "token"
                                        )}`,
                                      }}
                                      listType="picture-card"
                                      fileList={upload.fileList}
                                      onPreview={handlePreview}
                                      onChange={handleChange}
                                    >
                                      {upload.fileList.length >= 8
                                        ? null
                                        : uploadButton}
                                    </Upload>
                                    <Modal
                                      visible={upload.previewVisible}
                                      title={upload.previewTitle}
                                      footer={null}
                                      onCancel={handleCancel}
                                    >
                                      <img
                                        alt="example"
                                        style={{ width: "100%" }}
                                        src={upload.previewImage}
                                      />
                                    </Modal>
                                  </div>
                                </Form.Item>
                              </div>
                            </Col>
                          </Row>
                        </Affix>
                      </div>
                      <div>
                        <div className="flw-comment ml-5 pl-5">
                          <h2>Follow-up</h2>
                          {values?.inquiry_followups?.map(
                            (
                              {
                                id,
                                followup_round,
                                comment,
                                followup_date,
                                postponed_date,
                                ...restData
                              }: any,
                              index: number
                            ) => {
                              if (comment) {
                                return (
                                  <>
                                    <Comment
                                      className="mb-4"
                                      author={
                                        <Tag color="magenta">
                                          {restData.status}
                                        </Tag>
                                      }
                                      avatar={
                                        <Avatar
                                          alt="Han Solo"
                                          style={{
                                            color: "#fff",
                                            backgroundColor: "#fa8c16",
                                          }}
                                        >
                                          {index + 1}
                                        </Avatar>
                                      }
                                      content={
                                        <div>
                                          <p>{comment}</p>
                                        </div>
                                      }
                                      datetime={<span>{followup_date}</span>}
                                    />

                                    {postponed_date && (
                                      <Alert
                                        message={
                                          <>
                                            Lead has been postponed to{" "}
                                            <$DateLabel
                                              value={postponed_date}
                                            />{" "}
                                          </>
                                        }
                                        type="info"
                                      />
                                    )}
                                  </>
                                );
                              }
                            }
                          )}
                          {!singleCommentEnable && (
                            <Comment
                              avatar={
                                <Avatar
                                  style={{
                                    color: "#fff",
                                    backgroundColor: "#fa8c16",
                                  }}
                                >
                                  {values?.inquiry_followups?.length + 1}
                                </Avatar>
                              }
                              content={
                                <Editor
                                  onChange={(e: any) => {
                                    setFieldValue("comment", e.target.value);
                                  }}
                                  onSubmit={() => {
                                    setFieldValue("comment", "");
                                    setFieldValue("postponed_date", "");
                                    setSingleCommentEnable(true);
                                    setFieldValue("inquiry_followups", [
                                      ...values?.inquiry_followups,
                                      {
                                        comment: values.comment,
                                        isNew: true,
                                        postponed_date: values.postponed_date,
                                        status,
                                        followup_date:
                                          moment().format("YYYY-MM-DD"),
                                      },
                                    ]);
                                  }}
                                  value={values.comment}
                                  values={values}
                                />
                              }
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </Form>
                </div>
              </Skeleton>
            </div>

            <div className="form-bottom d-flex justify-content-end">
              {!inquiry.edit.isLoading && (
                <Button
                  type="primary"
                  className="mr-2"
                  loading={inquiry.edit.isLoading}
                  onClick={(e) => {
                    handleSubmit(e);
                    if (!isValid) {
                      message.error("Plese fix errors to proceed!");
                    }
                  }}
                  disabled={isSubmitting || isValidating || !dirty || !isValid}
                >
                  Save
                </Button>
              )}
              <Button
                type="default"
                onClick={() => {
                  resetForm();
                }}
              >
                Reset
              </Button>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user } = state;
  return {
    inquiry,
    course,
    user,
  };
};

const mapDispatchToProps = {
  getInqueries: Actions.inquery.self.get,
  getInqueryTypes: Actions.inquery.type.get,
  getCourses: Actions.course.self.get,
  editInquery: Actions.inquery.edit.get,
  updateInquery: Actions.inquery.update.post,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditInquery);
