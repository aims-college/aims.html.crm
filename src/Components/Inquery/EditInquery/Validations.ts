import * as Yup from "yup";
import moment from "moment";

export default () => {
  return Yup.object().shape({
    inquiry_type_id: Yup.string()
      .required("Lead type is required")
      .typeError("Invalid type id"),
    contact_no: Yup.number()
      .required("Contact Number is required")
      .typeError("Contact Number is invalid"),
    inquirer_type: Yup.string()
      .required("Lead Name Type is required")
      .typeError("Lead Name Type is invalid"),
    inquirer_name: Yup.string()
      .required("Lead Owner Name is required")
      .typeError("Lead Owner Name is invalid"),
    nic: Yup.string().required("NIC is required").typeError("NIC is invalid"),
    email: Yup.string().email().nullable(),
    course_id: Yup.string()
      .required("Programe is required")
      .typeError("Programe is invalid"),
  });
};
