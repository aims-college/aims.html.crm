import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Badge,
  Tooltip,
} from "antd";
import { ColumnsType } from "antd/lib/table";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
import { $Select, $Input, $DatePicker, $RangePicker } from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import * as Actions from "Actions";

import {
  DownOutlined,
  PhoneOutlined,
  MailOutlined,
  MenuOutlined,
  UserSwitchOutlined,
} from "@ant-design/icons";
import { useHistory, useParams } from "react-router-dom";
import moment from "moment";
import queryString from "query-string";

const Home = ({
  getFaculties,
  getInqueries,
  inquiry,
  getInqueryTypes,
  course,
  getCourses,
  getUsers,
  user,
}: any) => {
  const history = useHistory();
  const params = queryString.parse(history.location.search);

  const [filter, setFilter] = useState<any>({
    nic: "",
    status: "pending",
    name: "",
    faculty_id: "",
    contact_no: "",
    course_id: params.course_id ? Number(params.course_id) : "",
    inquiry_type_id: params.inquiry_type_id
    ? Number(params.inquiry_type_id)
    : "",
    user_id: params.user_id ? Number(params.user_id) : "",
    start_date: params.start_date
      ? moment(params.start_date).format("YYYY-MM-DD")
      : moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: params.end_date
      ? moment(params.end_date).format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD"),
  });

  const [excel, setExcel] = useState<any>([{ data: [], columns: [] }]);

  useEffect(() => {
    getInqueries(filter);
    getInqueryTypes();
    getCourses();
    getUsers();
    getFaculties();
  }, []);

  useEffect(() => {
    let columns: any = [];
    const dataSource: any = [];

    inquiry.inquiries.data.map((data: any, index: number) => {
      if (index === 0) {
        columns = Object.keys(data).map((column: any) =>
          column.replace(/_/g, " ").toUpperCase()
        );
      }
    });

    inquiry.inquiries.data.map((data: any, index: number) => {
      dataSource.push(
        Object.values(data)
          .filter((value) => !Array.isArray(value))
          .map((value: any) => ({ value: value?.toString() }))
      );
    });

    setExcel([{ data: dataSource, columns }]);
  }, [inquiry.inquiries.data]);

  const statusMenu = (
    <Menu
      onClick={(e) => {
        setFilter({ ...filter, status: e.key.toString() });
        getInqueries({ ...filter, status: e.key.toString() });
      }}
    >
      <Menu.Item key="pending">
        <Tag className="tag-status-pending">Pending</Tag>
      </Menu.Item>
      <Menu.Item key="registered">
        <Tag className="tag-status-registered">Registered</Tag>
      </Menu.Item>
      <Menu.Item key="non_registered">
        <Tag className="tag-status-non_registered">Non Registered</Tag>
      </Menu.Item>
      <Menu.Item key="not_answered">
        <Tag className="tag-status-not_answered">Not Answered</Tag>
      </Menu.Item>
      <Menu.Item key="postponed">
        <Tag className="tag-status-postponed">Postponed</Tag>
      </Menu.Item>
    </Menu>
  );

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "",
        width: "20px",
        render: (text: any, record: any) =>
          record.status != "registered" && (
            <a href={`/registrations/${record.id}`}>
              <UserSwitchOutlined />
            </a>
          ),
      },
      {
        title: "",
        dataIndex: "id",
        key: "id",
        render: (text: string) => {
          return <Tag>{`INQ-${String(text).padStart(5, "0")}`}</Tag>;
        },
        width: 80,
      },
      {
        title: "Status",
        key: "status",
        dataIndex: "status",
        render: (text: any, record: any, index: number) => {
          return <Tag className={`tag-status-${text}`}>{text}</Tag>;
        },
        width: 110,
        ellipsis: true,
      },
      {
        title: "Lead Type",
        dataIndex: "inquiry_type_name",
        key: "inquiry_type_name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Lead Date",
        dataIndex: "inquiry_date",
        key: "inquiry_date",
        width: 110,
      },
      {
        title: "Course Name",
        dataIndex: "course_name",
        key: "course_name",
        width: 140,
      },
      {
        title: "Lead Owner",
        dataIndex: "inquiry_handlers",
        key: "inquiry_handlers",
        width: 140,
        render: (text: string, record: any) => {
          if (record && record?.inquiry_handlers) {
            const handler = record?.inquiry_handlers?.filter(
              (data: any) => data.status == "handling"
            )[0];

            const requested = record?.inquiry_handlers?.filter(
              (data: any) => data.status == "requested"
            )[0];

            return (
              <>
                {requested?.handler_name && (
                  <Badge
                    status="processing"
                    text={requested?.handler_name}
                  ></Badge>
                )}
                {handler?.handler_name && (
                  <Badge status="success" text={handler?.handler_name}></Badge>
                )}
              </>
            );
          }
        },
      },
      {
        title: "Lead Name",
        dataIndex: "inquirer_name",
        key: "inquirer_name",
        render: (text: any, record: any, index: number) => {
          return (
            <div className="d-flex flex-column">
              <div>
                <strong className="mr-1">{record.inquirer_type}</strong>
                <strong>{text}</strong>
              </div>
              <div className="d-flex align-items-center">
                {record.email && (
                  <a className="text-nowrap" href={`mailto:${record.email}`}>
                    <span className="mr-1">
                      <MailOutlined />
                    </span>
                    {record.email}
                  </a>
                )}
                {record.email && <Divider type="vertical"></Divider>}
                <div className="text-nowrap">
                  <PhoneOutlined className="mr-1 text-muted" />
                  <a href={`tel:${record.contact_no}`}>{record.contact_no}</a>
                </div>
              </div>
            </div>
          );
        },
      },
    ];
  };
  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          authUserId: localStorage.getItem("user_id"),
          dateRange:
            params.start_date && params.end_date
              ? [
                  moment(params.start_date),
                  moment(params.end_date),
                ]
              : [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Leads"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={48}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <div className="mb-3">
                        <Dropdown overlay={statusMenu}>
                          <div
                            className="status-dropdown-link tag-status-pending d-flex align-items-center"
                            onClick={(e) => {
                              e.preventDefault();
                            }}
                          >
                            <div className="flex-fill">{filter.status}</div>{" "}
                            <DownOutlined />
                          </div>
                        </Dropdown>
                      </div>
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        value={[values.start_date, values.end_date]}
                        onChange={(dateRange: any) => {
                          if (dateRange) {
                            setFilter(() => {
                              return {
                                ...filter,
                                start_date: dateRange[0].format("YYYY-MM-DD"),
                                end_date: dateRange[1].format("YYYY-MM-DD"),
                              };
                            });
                            getInqueries({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                          }
                        }}
                      />
                      <$Input
                        size="small"
                        placeholder="Name"
                        name="name"
                        value={filter.name}
                        onBlur={(e: any) => {
                          setFilter({ ...filter, name: e.target.value });
                          getInqueries({ ...filter, name: e.target.value });
                        }}
                      />
                      <$Input
                        size="small"
                        placeholder="NIC"
                        name="nic"
                        value={filter.nic}
                        onBlur={(e: any) => {
                          setFilter({ ...filter, nic: e.target.value });
                          getInqueries({ ...filter, nic: e.target.value });
                        }}
                      />
                      <$Input
                        size="small"
                        placeholder="Contact Number"
                        value={filter.contact_no}
                        name="contact_no"
                        onBlur={(e: any) => {
                          setFilter({ ...filter, contact_no: e.target.value });
                          getInqueries({
                            ...filter,
                            contact_no: e.target.value,
                          });
                        }}
                      />
                      <$Select
                        name="inquiry_type_id"
                        size="small"
                        formitem={{
                          label: "Type",
                        }}
                        options={inquiry.type.data}
                        optionValue="id"
                        optionText="description"
                        defaultValue="all"
                        allOption={true}
                        onChange={(inquiry_type_id: any) => {
                          if (inquiry_type_id == "all") {
                            setFilter({ ...filter, inquiry_type_id: "" });
                            getInqueries({ ...filter, inquiry_type_id: "" });
                          } else {
                            setFilter({ ...filter, inquiry_type_id });
                            getInqueries({ ...filter, inquiry_type_id });
                          }
                        }}
                      />

                      <$Select
                        name="faculty_id"
                        size="small"
                        formitem={{
                          label: "Faculty",
                        }}
                        options={course.faculties.data}
                        optionValue="id"
                        optionText="faculty"
                        defaultValue="all"
                        allOption={true}
                        onChange={(faculty_id: any) => {
                          if (faculty_id == "all") {
                            getInqueries({ ...filter, faculty_id: "" });
                            setFilter({ ...filter, faculty_id: "" });
                          } else {
                            getInqueries({ ...filter, faculty_id });
                            setFilter({ ...filter, faculty_id });
                            getCourses({ faculty_id });
                          }
                        }}
                      />

                      <$Select
                        name="course_id"
                        size="small"
                        formitem={{
                          label: "Course",
                        }}
                        dropdownMatchSelectWidth={true}
                        options={course.courses.data.filter(
                          ({ status }: { status: string }) => status === "1"
                        )}
                        optionValue="id"
                        optionText="name"
                        defaultValue="all"
                        allOption={true}
                        onChange={(course_id: any) => {
                          if (course_id == "all") {
                            getInqueries({ ...filter, course_id: "" });
                            setFilter({ ...filter, course_id: "" });
                          } else {
                            getInqueries({ ...filter, course_id });
                            setFilter({ ...filter, course_id });
                          }
                        }}
                      />

                      <$Select
                        name="user_id"
                        size="small"
                        formitem={{
                          label: "Handler",
                        }}
                        options={user.users.data}
                        placeholder="Select User"
                        optionValue="id"
                        optionText="fullName"
                        allOption={true}
                        onChange={(user_id: any) => {
                          if (user_id == "all") {
                            getInqueries({ ...filter, user_id: "" });
                            setFilter({ ...filter, user_id: "" });
                          } else {
                            getInqueries({ ...filter, user_id });
                            setFilter({ ...filter, user_id });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <ExcelFile
                      element={
                        <Button type="dashed" style={{ float: "right" }}>
                          Export Data
                        </Button>
                      }
                    >
                      <ExcelSheet dataSet={excel} name="Leads"></ExcelSheet>
                    </ExcelFile>

                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={inquiry.inquiries.data}
                      loading={inquiry.inquiries.isLoading}
                      rowKey={(record) => record.id}
                      onRow={(record, rowIndex) => {
                        return {
                          onDoubleClick: (event) => {
                            if (
                              record.inquiry_handlers.length > 0 &&
                              record.inquiry_handlers?.some(
                                ({ front_officer_id, status }: any) => {
                                  return (
                                    front_officer_id == values.authUserId &&
                                    status == "handling"
                                  );
                                }
                              )
                            ) {
                              history.push(`/inqueries/${record.id}`, record);
                            } else {
                              history.push(
                                `/inqueries/${record.id}/view`,
                                record
                              );
                            }
                          },
                        };
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user } = state;

  return {
    inquiry,
    course,
    user,
  };
};

const mapDispatchToProps = {
  getInqueries: Actions.inquery.self.get,
  getInqueryTypes: Actions.inquery.type.get,
  getFaculties: Actions.course.faculty.get,
  getCourses: Actions.course.self.get,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
