import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import { $Select, $Input, $DatePicker, $DateLabel } from "Components/antd";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Upload,
  Form,
  Input,
  Row,
  Col,
  Select,
  Modal,
  Skeleton,
  Spin,
  Button,
  Tag,
  message,
  Alert,
} from "antd";
import "./NewInquery.scss";
import { connect } from "react-redux";
import { DownOutlined, PlusOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import ValidationScema from "./Validations";

const NewInquery: React.FC<any> = ({
  inquiry,
  getInqueryTypes,
  course,
  createInquery,
  getCourses,
  getUsers,
  user,
}: any) => {
  const { Option } = Select;
  const [fetching, setFetching] = React.useState(false);
  const { Dragger } = Upload;
  const history = useHistory();
  const [status, setStatus] = useState("pending");

  useEffect(() => {
    getInqueryTypes();
    getCourses();
    getUsers();
  }, []);

  const [upload, setUpload] = useState<any>({
    previewVisible: false,
    previewImage: "",
    previewTitle: "",
    fileList: [],
  });

  const handleCancel = () => setUpload({ ...upload, previewVisible: false });

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setUpload({
      ...upload,
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf("/") + 1),
    });
  };

  const handleChange = ({ fileList }: any) => {
    setUpload({ ...upload, fileList });
  };

  function getBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  const { TextArea } = Input;
  const statusMenu = (
    <Menu
      onClick={(e) => {
        setStatus(e.key.toString());
      }}
    >
      <Menu.Item key="pending">
        <Tag className="tag-status-pending">Pending</Tag>
      </Menu.Item>
      <Menu.Item key="non_registered">
        <Tag className="tag-status-non_registered">Non Registered</Tag>
      </Menu.Item>
      <Menu.Item key="not_answered">
        <Tag className="tag-status-not_answered">Not Answered</Tag>
      </Menu.Item>
    </Menu>
  );

  const Editor = ({
    onChange,
    onSubmit,
    submitting,
    value,
    name,
    values,
    setFieldValue,
  }: any) => {
    return (
      <>
        <Form.Item>
          <TextArea rows={4} onChange={onChange} value={value} />
        </Form.Item>
        
        <div className="d-flex align-items-center">
        <div className="mr-3">
          <Form.Item>

            <$DatePicker
              name={`postponed_date`}
              value={values?.postponed_date}
              label="Date to be Postpone"
              onBlur={() => {
                setStatus("postponed");
              }}
            />
          </Form.Item>
        </div>
        <div>
          <Form.Item
          >
            <Button
              className='mt-2'
              htmlType="submit"
              type="primary"
              onClick={onSubmit}
              disabled={!value}
            >
              Add Follow-up
            </Button>

          </Form.Item>
        </div>

      </div>
      </>
    );
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const createHandlerData = (values: any) => {
    const inquiry_handler = [];

    /**
     * @description no assinger
     */

    /**
     * @description new assinger
     */

    if (
      values.authUserId &&
      values.assignToId &&
      values.authUserId != values.assignToId
    ) {
      // auth user
      inquiry_handler.push({
        front_officer_id: values.authUserId,
        status: "requested",
        status_reason: "",
      });

      //assign to
      inquiry_handler.push({
        front_officer_id: values.assignToId,
        status: "pending",
        status_reason: null,
      });
    } else if (
      values.authUserId &&
      values.assignToId &&
      values.authUserId == values.assignToId
    ) {
      inquiry_handler.push({
        front_officer_id: values.authUserId,
        status: "handling",
        status_reason: "",
      });
    }

    return {
      ...values,
      status,
      inquiry_date: moment(),
      inquiry_references:
        upload &&
        Array.isArray(upload.fileList) &&
        upload.fileList.map(
          ({ name, status, response: { file_path } }: any) => {
            if (status === "done") {
              return {
                document_name: name,
                description: name,
                url: file_path,
              };
            }
          }
        ),
      inquiry_followups: values.inquiry_followups.filter(
        ({ isNew }: { isNew: boolean }) => isNew === true
      ),
      inquiry_handler,
    };
  };

  return (
    <>
      <Formik
        initialValues={{
          ...inquiry.new.data,
          comment: "",
          postponed_date: null,
          authUserId: Number(localStorage.getItem("user_id")),
          assignToId: Number(localStorage.getItem("user_id")),
        }}
        enableReinitialize
        validateOnBlur
        validateOnMount
        validateOnChange
        validationSchema={ValidationScema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          createInquery(createHandlerData(values));
        }}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
          dirty,
          isValid,
          errors
        }: any) => (
          <>
            <div className="space-content pb-5">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title={`New Lead`}
                    subTitle={<$DateLabel value={moment()} />}
                    extra={[
                      <Dropdown overlay={statusMenu} key={1}>
                        <div
                          className="status-dropdown-link tag-status-pending d-flex align-items-center"
                          onClick={(e) => e.preventDefault()}
                        >
                          <div className="flex-fill">{status}</div>{" "}
                          <DownOutlined />
                        </div>
                      </Dropdown>,
                    ]}
                  />
                </div>
              </Affix>
              <Skeleton loading={course.courses.isLoading}>
                <div className="mt-4">
                  <Form layout="vertical">
                    <Row gutter={24} className="mb-5">
                      <Col span={6}>
                        <div>
                          <$Select
                            name="inquiry_type_id"
                            size="medium"
                            formitem={{
                              label: "Lead Type",
                            }}
                            options={inquiry.type.data.filter(
                              ({ type }: any) =>
                                type !== "web" && type !== "social_media"
                            )}
                            optionValue="id"
                            optionText="description"
                            defaultValue="all"
                            allOption={true}
                          />
                        </div>
                        <div className="d-flex">
                          <Form.Item
                            label="Contact Number"
                            required
                            style={{ width: 180 }}
                          >
                            <$Input name="contact_no" paceholder='0777123456'/>
                          </Form.Item>
                        </div>
                        <div>
                          <Form.Item label="Lead Name" required>
                            <div className="d-flex">
                              <div>
                                <$Select
                                  style={{ width: 80 }}
                                  className="mr-1"
                                  name="inquirer_type"
                                  size="medium"
                                  options={[
                                    {
                                      label: "Mr",
                                      value: "Mr",
                                    },
                                    {
                                      label: "Mrs",
                                      value: "Mrs",
                                    },
                                    {
                                      label: "Miss",
                                      value: "Miss",
                                    },
                                    {
                                      label: "Ven",
                                      value: "Ven",
                                    },
                                  ]}
                                  optionValue="value"
                                  optionText="label"
                                  allOption={false}
                                  required
                                />
                              </div>
                              <div className="flex-fill">
                                {" "}
                                <$Input name="inquirer_name"  />
                              </div>
                            </div>
                          </Form.Item>
                        </div>
                        <div className="d-flex">
                          <Form.Item
                            label="NIC/Passport"
                            required
                            style={{ width: 180 }}
                          >
                            <$Input name="nic" />
                          </Form.Item>
                        </div>
                        <div>
                          <Form.Item label="Email">
                            <$Input name="email" />
                          </Form.Item>
                        </div>
                        <div>
                          <$Select
                            name="course_id"
                            size="medium"
                            formitem={{
                              label: "Programe",
                            }}
                            options={course.courses.data.filter(({status}:{status:string}) => status === "1")}
                            optionValue="id"
                            optionText="name"
                            allOption={false}
                            required
                          />
                        </div>
                      </Col>
                      <Col span={8}>
                        <div>
                          <$Select
                            name="assignToId"
                            size="medium"
                            formitem={{
                              label: "Assign to",
                            }}
                            options={user.users.data}
                            optionValue="id"
                            optionText="fullName"
                            allOption={false}
                            defaultValue={"self"}
                          />
                        </div>
                        <div>
                          <Form.Item label="Reference">
                            <div>
                              <Upload
                                action={`${process.env.REACT_APP_API_URL}/file_upload`}
                                headers={{
                                  Authorization: `Bearer ${localStorage.getItem(
                                    "token"
                                  )}`,
                                }}
                                listType="picture-card"
                                fileList={upload.fileList as any}
                                onPreview={handlePreview}
                                onChange={handleChange}
                              >
                                {upload.fileList.length >= 8
                                  ? null
                                  : uploadButton}
                              </Upload>
                              <Modal
                                visible={upload.previewVisible}
                                title={upload.previewTitle}
                                footer={null}
                                onCancel={handleCancel}
                              >
                                <img
                                  alt="example"
                                  style={{ width: "100%" }}
                                  src={upload.previewImage}
                                />
                              </Modal>
                            </div>
                          </Form.Item>
                        </div>
                      </Col>
                      <Col span={10}>
                        <div className="flw-comment pl-3 pr-3">
                          <h2>Follow-up</h2>
                          {values?.inquiry_followups?.map(
                            (
                              {
                                id,
                                followup_round,
                                comment,
                                followup_date,
                                postponed_date,
                              }: any,
                              index: number
                            ) => {
                              if (comment) {
                                return (
                                  <>
                                    <Comment
                                      key={id}
                                      className="mb-4"
                                      author={
                                        <Tag color="magenta">{status}</Tag>
                                      }
                                      avatar={
                                        <Avatar
                                          alt="Han Solo"
                                          style={{
                                            color: "#fff",
                                            backgroundColor: "#fa8c16",
                                          }}
                                        >
                                          {index + 1}
                                        </Avatar>
                                      }
                                      content={
                                        <div>
                                          <p>{comment}</p>
                                        </div>
                                      }
                                      datetime={<span>{followup_date}</span>}
                                    />
                                    {postponed_date && (
                                      <Alert
                                        message={
                                          <>
                                            Lead has been postponed to{" "}
                                            <$DateLabel
                                              value={postponed_date}
                                            />{" "}
                                          </>
                                        }
                                        type="info"
                                      />
                                    )}
                                  </>
                                );
                              }
                              return (
                                <Comment
                                  key={id}
                                  avatar={
                                    <Avatar
                                      style={{
                                        color: "#fff",
                                        backgroundColor: "#fa8c16",
                                      }}
                                    >
                                      {values?.inquiry_followups?.length}
                                    </Avatar>
                                  }
                                  content={
                                    <Editor
                                      onChange={(e: any) => {
                                        setFieldValue(
                                          "comment",
                                          e.target.value
                                        );
                                      }}
                                      onSubmit={() => {
                                        setFieldValue("comment", "");
                                        setFieldValue("postponed_date", "");

                                        setFieldValue("inquiry_followups", [
                                          {
                                            comment: values.comment,
                                            isNew: true,
                                            postponed_date:
                                              values.postponed_date,
                                            followup_date:
                                              moment().format("YYYY-MM-DD"),
                                          },
                                        ]);
                                      }}
                                      value={values.comment}
                                      values={values}
                                      setFieldValue={setFieldValue}
                                      name={"inquiry_followups"}
                                    />
                                  }
                                />
                              );
                            }
                          )}
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </Skeleton>
            </div>

            <div className="form-bottom d-flex justify-content-end">
              <Button
                type="primary"
                className="mr-2"
                loading={isSubmitting && isValidating}
                onClick={(e) => {
                  handleSubmit(e);
                  if (!isValid) {
                    message.error("Plese fix errors to proceed!");
                  }
                }}
                disabled={isSubmitting || isValidating ||  !dirty || !isValid}
                >
                Save
              </Button>
              <Button
                type="default"
                onClick={() => {
                  resetForm();
                }}
              >
                Reset
              </Button>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user } = state;
  return {
    inquiry,
    course,
    user,
  };
};

const mapDispatchToProps = {
  getInqueries: Actions.inquery.self.get,
  getInqueryTypes: Actions.inquery.type.get,
  getCourses: Actions.course.self.get,
  createInquery: Actions.inquery.create.post,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewInquery);
