import * as Yup from "yup";
import moment from "moment";

export default () => {
  return Yup.object().shape({
    inquiry_type_id: Yup.string()
      .required("Inquiry type is required")
      .typeError("Invalid type id"),
    contact_no: Yup.number()
      .required("Contact Number is required")
      .typeError("Contact Number is invalid"),
    inquirer_type: Yup.string()
      .required("Inquirer Type is required")
      .typeError("Inquirer Type is invalid"),
    inquirer_name: Yup.string()
      .required("Inquirer Name is required")
      .typeError("Inquirer Name is invalid"),
    nic: Yup.string().required("NIC is required").typeError("NIC is invalid"),
    email: Yup.string().email().nullable(),
    course_id: Yup.string()
      .required("Programe is required")
      .typeError("Programe is invalid"),
  });
};
