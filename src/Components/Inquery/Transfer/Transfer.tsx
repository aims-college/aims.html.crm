import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Popconfirm,
  Menu,
  PageHeader,
  Table,
  Tag,
  Input,
} from "antd";
import { $Select, $Input, $DatePicker, $RangePicker } from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
const { TextArea } = Input;
const Home = ({
  getTransfers,
  inquiry,
  getInqueryTypes,
  course,
  getCourses,
  getUsers,
  approveTransfer,
  user,
}: any) => {
  const [filter, setFilter] = useState({
    nic: "",
    status: "pending",
    name: "",
    inquiry_type_id: "",
    course_id: "",
    contact_no: "",
    start_date: moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });

  const [confirmComment, setConfirmComment] = useState<string>("");

  const history = useHistory();

  useEffect(() => {
    getTransfers(filter);
    getInqueryTypes();
    getCourses();
    getUsers();
  }, []);

  const statusMenu = (
    <Menu
      onClick={(e) => {
        setFilter({ ...filter, status: e.key.toString() });
        getTransfers({ ...filter, status: e.key.toString() });
      }}
    >
      <Menu.Item key="pending">
        <Tag className="tag-status-pending">Pending</Tag>
      </Menu.Item>
      <Menu.Item key="registered">
        <Tag className="tag-status-registered">Registered</Tag>
      </Menu.Item>
      <Menu.Item key="non_registered">
        <Tag className="tag-status-non_registered">Non Registered</Tag>
      </Menu.Item>
      <Menu.Item key="not_answered">
        <Tag className="tag-status-not_answered">Not Answered</Tag>
      </Menu.Item>
    </Menu>
  );

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "",
        dataIndex: "id",
        key: "id",
        render: (text: string) => {
          return <Tag>{`INQ-${String(text).padStart(5, "0")}`}</Tag>;
        },
        width: 80,
      },
      {
        title: "Status",
        key: "status",
        dataIndex: "status",
        render: (text: any, record: any, index: number) => {
          return <Tag className={`tag-status-${text}`}>{text}</Tag>;
        },
        width: 110,
        ellipsis: true,
      },

      {
        title: "Lead Date",
        dataIndex: "inquiry_date",
        key: "inquiry_date",
        width: 110,
      },
      // {
      //   title: "Title",
      //   dataIndex: "inquirer_type",
      //   key: "inquirer_type",
      // },
      {
        title: "Lead Name",
        dataIndex: "inquirer_name",
        key: "inquirer_name",
        render: (text: any, record: any, index: number) => {
          return (
            <div className="d-flex flex-column">
              <div>
                <strong className="mr-1">{record.inquirer_type}</strong>
                <strong>{text}</strong>
              </div>
              <div className="d-flex align-items-center">
                <a className="text-nowrap" href={`mailto:${record.email}`}>
                  <span className="mr-1">
                    <MailOutlined />
                  </span>
                  {record.email}
                </a>
                <Divider type="vertical"></Divider>{" "}
                <div className="text-nowrap">
                  <PhoneOutlined className="mr-1 text-muted" />
                  {record.contact_no}
                </div>
              </div>
            </div>
          );
        },
      },
      {
        title: "NIC",
        dataIndex: "nic",
        key: "nic",
        width: 150,
      },
      {
        title: "Lead Type",
        dataIndex: "inquiry_type_name",
        key: "inquiry_type_name",
        width: 100,
      },
      {
        title: "Take an Action",
        key: "Take an Action",
        dataIndex: "isConfirm",
        width: 100,
        render: (text: any, record: any, index: number) => (
          <>
            <Popconfirm
              title={
                <>
                  <TextArea
                    rows={2}
                    onChange={(e) => {
                      setConfirmComment(e.target.value);
                    }}
                  ></TextArea>
                </>
              }
              okButtonProps={{
                disabled: confirmComment.length === 0,
                onClick: () => {
                  setConfirmComment("");
                  approveTransfer({
                    inquiry_handler_id: record?.inquiry_handlers?.filter(
                      (data: any) => data.status == "pending"
                    )[0].id,
                    status: 1,
                    status_reason: confirmComment,
                  });
                },
              }}
              cancelButtonProps={{
                disabled: confirmComment.length === 0,
                onClick: () => {
                  setConfirmComment("");
                  approveTransfer({
                    inquiry_handler_id: record?.inquiry_handlers?.filter(
                      (data: any) => data.status == "pending"
                    )[0].id,
                    status: 0,
                    status_reason: confirmComment,
                  });
                },
                danger: true,
              }}
              okText="Approve"
              cancelText="Reject"
            >
              <Button type="primary" size="small">
                Confirm
              </Button>
            </Popconfirm>
          </>
        ),
      },
    ];
  };

  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Transfer"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={48}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        onChange={(dateRange: any) => {
                          if (dateRange) {
                            setFilter({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                            getTransfers({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                          }
                        }}
                      />
                      {/* <$Input
                        size="small"
                        placeholder="Name"
                        name="name"
                        value={filter.name}
                        onBlur={(e: any) => {
                          setFilter({ ...filter, name: e.target.value });
                          getTransfers({ ...filter, name: e.target.value });
                        }}
                      />
                      <$Input
                        size="small"
                        placeholder="NIC"
                        name="nic"
                        value={filter.nic}
                        onBlur={(e: any) => {
                          setFilter({ ...filter, nic: e.target.value });
                          getTransfers({ ...filter, nic: e.target.value });
                        }}
                      />
                      <$Input
                        size="small"
                        placeholder="Contact Number"
                        value={filter.contact_no}
                        name="contact_no"
                        onBlur={(e: any) => {
                          setFilter({ ...filter, contact_no: e.target.value });
                          getTransfers({
                            ...filter,
                            contact_no: e.target.value,
                          });
                        }}
                      /> */}
                      <$Select
                        name="inquiry_type_id"
                        size="small"
                        formitem={{
                          label: "Lead Type",
                        }}
                        options={inquiry.type.data}
                        value={filter.inquiry_type_id}
                        optionValue="id"
                        optionText="description"
                        defaultValue="all"
                        allOption={true}
                        onChange={(inquiry_type_id: any) => {
                          if (inquiry_type_id == "all") {
                            setFilter({ ...filter, inquiry_type_id: "" });
                            getTransfers({ ...filter, inquiry_type_id: "" });
                          } else {
                            setFilter({ ...filter, inquiry_type_id });
                            getTransfers({ ...filter, inquiry_type_id });
                          }
                        }}
                      />
                      <$Select
                        name="course_id"
                        size="small"
                        formitem={{
                          label: "Course",
                        }}
                        options={course.courses.data.filter(({status}:{status:string}) => status === "1")}
                        optionValue="id"
                        optionText="name"
                        defaultValue="all"
                        value={filter.course_id}
                        allOption={true}
                        onChange={(course_id: any) => {
                          if (course_id == "all") {
                            getTransfers({ ...filter, course_id: "" });
                            setFilter({ ...filter, course_id: "" });
                          } else {
                            getTransfers({ ...filter, course_id });
                            setFilter({ ...filter, course_id });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={inquiry.transfer.data}
                      loading={inquiry.transfer.isLoading}
                      rowKey={(record) => record.id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, transfer } = state;

  return {
    inquiry,
    transfer,
    course,
    user,
  };
};

const mapDispatchToProps = {
  getTransfers: Actions.inquery.transfer.get,
  getInqueryTypes: Actions.inquery.type.get,
  getCourses: Actions.course.self.get,
  getUsers: Actions.user.users.get,
  approveTransfer: Actions.inquery.approve.post,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
