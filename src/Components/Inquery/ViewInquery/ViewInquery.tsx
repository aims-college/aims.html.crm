import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import { $Select, $Input, $DatePicker, $DateLabel } from "Components/antd";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Upload,
  Form,
  Input,
  Row,
  Col,
  Select,
  Modal,
  Skeleton,
  Spin,
  Button,
  Tag,
  Timeline,
  Drawer,
  Space,
  Divider,
} from "antd";
import "./NewInquery.scss";
import { connect } from "react-redux";
import {
  DownOutlined,
  PlusOutlined,
  ClockCircleOutlined,
} from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";

const ViewInquery: React.FC<any> = ({
  inquiry,
  getInqueryTypes,
  editInquery,
  course,
  updateInquery,
  getUsers,
  user,
}: any) => {
  const { Option } = Select;
  const [fetching, setFetching] = React.useState(false);
  const { Dragger } = Upload;
  const history = useHistory();
  const [status, setStatus] = useState("pending");
  const [visibleHandlerDrawer, setVisibleHandlerDrawer] =
    useState<boolean>(false);

  const [upload, setUpload] = useState({
    previewVisible: false,
    previewImage: "",
    previewTitle: "",
    fileList: [],
  });

  const handleCancel = () => setUpload({ ...upload, previewVisible: false });

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setUpload({
      ...upload,
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf("/") + 1),
    });
  };

  const handleChange = ({ fileList }: any) =>
    setUpload({ ...upload, fileList });

  useEffect(() => {
    if (history.location.state) {
      editInquery(history.location.state as any);
      const { status, inquiry_followups } = history.location.state as any;
      setStatus(status);
      getUsers();
    }
  }, [history.location.key]);

  function getBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  const { TextArea } = Input;
  const statusMenu = (
    <Menu
      onClick={(e) => {
        setStatus(e.key.toString());
      }}
    >
      <Menu.Item key="pending">
        <Tag className="tag-status-pending">Pending</Tag>
      </Menu.Item>
      <Menu.Item key="registered">
        <Tag className="tag-status-registered">Registered</Tag>
      </Menu.Item>
      <Menu.Item key="non_registered">
        <Tag className="tag-status-non_registered">Non Registered</Tag>
      </Menu.Item>
      <Menu.Item key="not_answered">
        <Tag className="tag-status-not_answered">Not Answered</Tag>
      </Menu.Item>
    </Menu>
  );

  const Editor = ({ onChange, onSubmit, submitting, value }: any) => (
    <>
      <Form.Item>
        <TextArea rows={4} onChange={onChange} value={value} />
      </Form.Item>
      <Form.Item>
        <Button
          htmlType="submit"
          type="primary"
          onClick={onSubmit}
          disabled={!value}
        >
          Add Follow-up
        </Button>
      </Form.Item>
    </>
  );

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <>
      <Formik
        initialValues={{
          ...inquiry.edit.data,
          comment: "",
          inquiry_references: inquiry.edit.data?.inquiry_references?.map(
            ({ inquiry_references }: any) => ({
              ...inquiry_references,
              document_name: "test",
              url: "test",
              is_new: 0,
              is_deleted: 0,
              ref_id: null,
            })
          ),
          inquiry_handler: [],
          assignTo: Number(
            inquiry.edit.data?.inquiry_handlers?.filter(
              (data: any) => data.status == "handling"
            )[0]?.front_officer_id
          ),
        }}
        enableReinitialize
        validateOnBlur
        validateOnMount
        validateOnChange
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            updateInquery({
              inquiry_id: values.id,
              ...values,
              status,
              inquiry_references: values.inquiry_references,
              inquiry_followups: values.inquiry_followups.filter(
                ({ isNew }: { isNew: boolean }) => isNew === true
              ),
              inquiry_handler: [
                {
                  ...values.inquiry_handlers?.filter(
                    (data: any) => data.status == "handling"
                  )[0],
                  is_new: 0,
                  is_updated: 1,
                  inquiry_handler_id: values.inquiry_handlers?.filter(
                    (data: any) => data.status == "handling"
                  )[0].id,
                },
                {
                  front_officer_id: values.assignTo,
                  is_new: 1,
                  is_updated: 0,
                  inquiry_handler_id: null,
                },
              ],
            });
          } catch (error) {
            console.error(error);
          }
        }}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="space-content pb-5">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title={`View Lead - INQ-${
                      (values?.id && String(values?.id).padStart(5, "0")) || 0
                    }`}
                    subTitle={
                      values.inquiry_date && (
                        <$DateLabel value={values.inquiry_date} />
                      )
                    }
                    extra={[
                      <Dropdown overlay={statusMenu} disabled>
                        <div
                          className="status-dropdown-link tag-status-pending d-flex align-items-center"
                          onClick={(e) => e.preventDefault()}
                        >
                          <div className="flex-fill">{status}</div>{" "}
                          <DownOutlined />
                        </div>
                      </Dropdown>,
                    ]}
                  />
                </div>
              </Affix>
              <Skeleton loading={inquiry.edit.isLoading}>
                <div className="mt-4">
                  <Form layout="vertical">
                    <div className="d-flex">
                      <div className="flex-fill edit-left-inq">
                        <Affix offsetTop={56} style={{ zIndex: 0 }}>
                          <Row gutter={24} className="mb-5">
                            <Col span={12}>
                              <div>
                                <$Select
                                  name="inquiry_type_id"
                                  size="medium"
                                  formitem={{
                                    label: "Lead Source",
                                  }}
                                  options={inquiry.type.data}
                                  optionValue="id"
                                  optionText="description"
                                  defaultValue="all"
                                  allOption={true}
                                  disabled
                                />
                              </div>
                              <div className="d-flex">
                                <Form.Item
                                  label="Contact Number"
                                  required
                                  style={{ width: 180 }}
                                >
                                  <$Input name="contact_no" disabled />
                                </Form.Item>
                              </div>
                              <div>
                                <Form.Item label="Lead Name">
                                  <div className="d-flex">
                                    <div>
                                      <$Select
                                        style={{ width: 80 }}
                                        className="mr-1"
                                        name="inquirer_type"
                                        size="medium"
                                        options={[
                                          {
                                            label: "Mr",
                                            value: "Mr",
                                          },
                                          {
                                            label: "Mrs",
                                            value: "Mrs",
                                          },
                                          {
                                            label: "Miss",
                                            value: "Miss",
                                          },
                                          {
                                            label: "Ven",
                                            value: "Ven",
                                          },
                                        ]}
                                        optionValue="value"
                                        optionText="label"
                                        allOption={false}
                                        disabled
                                      />
                                    </div>
                                    <div className="flex-fill">
                                      {" "}
                                      <$Input name="inquirer_name" disabled />
                                    </div>
                                  </div>
                                </Form.Item>
                              </div>
                              <div className="d-flex">
                                <Form.Item
                                  label="NIC/Passport"
                                  required
                                  style={{ width: 180 }}
                                >
                                  <$Input name="nic" disabled />
                                </Form.Item>
                              </div>
                              <div>
                                <Form.Item label="Email">
                                  <$Input name="email" disabled />
                                </Form.Item>
                              </div>
                              <div>
                                <$Select
                                  name="course_id"
                                  size="medium"
                                  formitem={{
                                    label: "Programe",
                                  }}
                                  options={course.courses.data.filter(({status}:{status:string}) => status === "1")}
                                  optionValue="id"
                                  optionText="name"
                                  allOption={false}
                                  required
                                  disabled
                                />
                              </div>
                            </Col>
                            <Col span={12}>
                              <div className="d-flex justify-content-between align-items-center">
                                <h3>Transfer History</h3>
                                <Button
                                  size="small"
                                  type="link"
                                  onClick={(e: any) =>
                                    setVisibleHandlerDrawer(true)
                                  }
                                >
                                  Check
                                </Button>
                              </div>
                              <Avatar.Group>
                                {values?.inquiry_handlers?.map(
                                  ({
                                    assigned_date,
                                    handler_name,
                                    status_reason,
                                    ...restData
                                  }: any) => {
                                    return (
                                      <Avatar
                                        style={{
                                          backgroundColor: `#ccc`,
                                        }}
                                      >
                                        {handler_name &&
                                          handler_name.charAt(0).toUpperCase()}
                                        {handler_name &&
                                          handler_name.split(" ")[1] &&
                                          handler_name
                                            .split(" ")[1]
                                            .charAt(0)
                                            .toUpperCase()}
                                      </Avatar>
                                    );
                                  }
                                )}
                              </Avatar.Group>

                            
                              <Drawer
                                title="Transfer Timeline"
                                placement={"right"}
                                width={500}
                                onClose={(e: any) =>
                                  setVisibleHandlerDrawer(false)
                                }
                                visible={visibleHandlerDrawer}
                              >
                                <Timeline mode="left">
                                  {values?.inquiry_handlers?.map(
                                    ({
                                      assigned_date,
                                      handler_name,
                                      status_reason,
                                      id,
                                      ...restData
                                    }: any) => {
                                      return (
                                        <Timeline.Item
                                          key={id}
                                          color="green"
                                          dot={
                                            restData.status == "pending" && (
                                              <ClockCircleOutlined
                                                style={{ fontSize: "16px" }}
                                              />
                                            )
                                          }
                                        >
                                          <div className="d-flex flex-column pb-2">
                                            <div className="d-flex justify-content-between mb-2">
                                              <div className="font-weight-bold">
                                                <$DateLabel
                                                  value={assigned_date}
                                                />
                                              </div>
                                              <div>
                                                {" "}
                                                <Tag className="m-0">
                                                  {restData.status} on{" "}
                                                </Tag>
                                              </div>
                                            </div>
                                            <div className="d-flex align-items-center">
                                              <Avatar
                                                className="mr-2"
                                                style={{
                                                  backgroundColor: `#ccc`,
                                                }}
                                              >
                                                {handler_name &&
                                                  handler_name
                                                    .charAt(0)
                                                    .toUpperCase()}
                                                {handler_name &&
                                                  handler_name.split(" ")[1] &&
                                                  handler_name
                                                    .split(" ")[1]
                                                    .charAt(0)
                                                    .toUpperCase()}
                                              </Avatar>{" "}
                                              {handler_name && (
                                                <span>{handler_name}</span>
                                              )}
                                            </div>
                                            <Divider
                                              className="m-0 mt-1 mb-1"
                                              dashed
                                            />
                                            <div
                                              className="text-muted"
                                              style={{ paddingLeft: "2.5rem" }}
                                            >
                                              {status_reason && status_reason}
                                            </div>
                                          </div>
                                        </Timeline.Item>
                                      );
                                    }
                                  )}
                                </Timeline>
                              </Drawer>

                              <div>
                                <Form.Item label="Reference"></Form.Item>
                              </div>
                            </Col>
                          </Row>
                        </Affix>
                      </div>
                      <div>
                        <div className="flw-comment ml-5 pl-5">
                          <h2>Follow-up</h2>
                          {values?.inquiry_followups?.map(
                            (
                              {
                                id,
                                followup_round,
                                comment,
                                followup_date,
                                ...restData
                              }: any,
                              index: number
                            ) => (
                              <Comment
                                className="mb-4"
                                author={<Tag color="magenta">{restData.status}</Tag>}
                                avatar={
                                  <Avatar
                                    alt="Han Solo"
                                    style={{
                                      color: "#fff",
                                      backgroundColor: "#fa8c16",
                                    }}
                                  >
                                    {index + 1}
                                  </Avatar>
                                }
                                content={
                                  <div>
                                    <p>{comment}</p>
                                  </div>
                                }
                                datetime={<span>{followup_date}</span>}
                              />
                            )
                          )}
                        </div>
                      </div>
                    </div>
                  </Form>
                </div>
              </Skeleton>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user } = state;
  return {
    inquiry,
    course,
    user,
  };
};

const mapDispatchToProps = {
  getInqueries: Actions.inquery.self.get,
  getInqueryTypes: Actions.inquery.type.get,
  getCourses: Actions.course.self.get,
  editInquery: Actions.inquery.edit.get,
  updateInquery: Actions.inquery.update.post,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewInquery);
