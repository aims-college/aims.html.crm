import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Popconfirm,
} from "antd";
import ReactExport from "react-export-excel";

import {
  $Select,
  $Input,
  $DatePicker,
  $RangePicker,
  $AmountLabel,
} from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const BusyExport = ({ getReports, payment, getPayments }: any) => {
  const [filter, setFilter] = useState({
    start_date: moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });

  const history = useHistory();
  const [excel, setExcel] = useState<any>({
    LKR: [{ data: [], columns: [] }],
    USD: [{ data: [], columns: [] }],
    EUR: [{ data: [], columns: [] }],
    GBP: [{ data: [], columns: [] }],
  });
  useEffect(() => {
    getReports(filter);
    getPayments(filter);
  }, []);

  useEffect(() => {
    let columns: any = [
      "VCH_SERIES",
      "VCH_BILL_DATE",
      "VCH_BILL_NO",
      "ACC_NAME_DR",
      "ACC_NAME_CR",
      "AMOUNT_DR",
      "AMOUNT_CR",
      "SHORT_NARRATION",
    ];
    const dataSource: {
      LKR: Array<any>;
      USD: Array<any>;
      EUR: Array<any>;
      GBP: Array<any>;
    } = {
      LKR: [],
      USD: [],
      EUR: [],
      GBP: [],
    };

    //LKR

    if (
      payment.busy.data.hasOwnProperty("LKR") &&
      Array.isArray(payment.busy.data.LKR)
    ) {
      payment.busy.data.LKR.map((value: any) => {
        dataSource.LKR.push([
          { value: value["VCH_SERIES"]?.toString() },
          { value: value["VCH_BILL_DATE"]?.toString() },
          { value: value["VCH_BILL_NO"]?.toString() },
          { value: value["ACC_NAME_DR"]?.toString() },
          { value: value["ACC_NAME_CR"]?.toString() },
          { value: value["AMOUNT_DR"]?.toString() },
          { value: value["AMOUNT_CR"]?.toString() },
          { value: value["SHORT_NARRATION"]?.toString() },
        ]);
      });
    }

    //USD

    if (
      payment.busy.data.hasOwnProperty("USD") &&
      Array.isArray(payment.busy.data.USD)
    ) {
      payment.busy.data.USD.map((value: any) => {
        dataSource.USD.push([
          { value: value["VCH_SERIES"]?.toString() },
          { value: value["VCH_BILL_DATE"]?.toString() },
          { value: value["VCH_BILL_NO"]?.toString() },
          { value: value["ACC_NAME_DR"]?.toString() },
          { value: value["ACC_NAME_CR"]?.toString() },
          { value: value["AMOUNT_DR"]?.toString() },
          { value: value["AMOUNT_CR"]?.toString() },
          { value: value["SHORT_NARRATION"]?.toString() },
        ]);
      });
    }

    //EUR

    if (
      payment.busy.data.hasOwnProperty("EUR") &&
      Array.isArray(payment.busy.data.EUR)
    ) {
      payment.busy.data.EUR.map((value: any) => {
        dataSource.EUR.push([
          { value: value["VCH_SERIES"]?.toString() },
          { value: value["VCH_BILL_DATE"]?.toString() },
          { value: value["VCH_BILL_NO"]?.toString() },
          { value: value["ACC_NAME_DR"]?.toString() },
          { value: value["ACC_NAME_CR"]?.toString() },
          { value: value["AMOUNT_DR"]?.toString() },
          { value: value["AMOUNT_CR"]?.toString() },
          { value: value["SHORT_NARRATION"]?.toString() },
        ]);
      });
    }

    //GBP

    if (
      payment.busy.data.hasOwnProperty("GBP") &&
      Array.isArray(payment.busy.data.GBP)
    ) {
      payment.busy.data.GBP.map((value: any) => {
        dataSource.GBP.push([
          { value: value["VCH_SERIES"]?.toString() },
          { value: value["VCH_BILL_DATE"]?.toString() },
          { value: value["VCH_BILL_NO"]?.toString() },
          { value: value["ACC_NAME_DR"]?.toString() },
          { value: value["ACC_NAME_CR"]?.toString() },
          { value: value["AMOUNT_DR"]?.toString(), numFmt: "0.00%" },
          { value: value["AMOUNT_CR"]?.toString(), numFmt: "0.00%" },
          { value: value["SHORT_NARRATION"]?.toString() },
        ]);
      });
    }

    setExcel({
      LKR: [{ data: dataSource.LKR, columns }],
      USD: [{ data: dataSource.USD, columns }],
      EUR: [{ data: dataSource.EUR, columns }],
      GBP: [{ data: dataSource.GBP, columns }],
    });

  }, [payment.payments.data]);

  const columns = [
    {
      title: "Receipt No",
      dataIndex: "receipt_no",
      key: "receipt_no",
      render: (text: string) => {
        return <Tag color="red">{text}</Tag>;
      },
      width: 80,
    },
    {
      title: "Date",
      dataIndex: "payment_date",
      key: "payment_date",
    },
    {
      title: "Currency",
      dataIndex: "payment_currency",
      key: "payment_currency",
      render: (text: string) => {
        return <Tag color="success">{text}</Tag>;
      },
      width: 80,
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      render: (text: string, record: any) => (
        <$AmountLabel value={Number(record.amount)} />
      ),
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Payment Method",
      dataIndex: "payment_method",
      key: "payment_method",
    },
    {
      title: "Local|Uni",
      dataIndex: "payment_type",
      key: "payment_type",
      render: (text: string) => {
        if (text == "1") {
          return <Tag color="blue">Local</Tag>;
        } else if (text == "2") {
          return <Tag color="geekblue">Uni</Tag>;
        } else {
          return <Tag color="green">Other</Tag>;
        }
      },
    },
    {
      title: "Registration No",
      dataIndex: "reg_no",
      key: "reg_no",
      render: (text: string) => {
        return <Tag color="volcano">{text}</Tag>;
      },
    },
    {
      title: "Branch",
      dataIndex: "branch_name",
      key: "branch_name",
    },
    {
      title: "Course",
      dataIndex: "course_name",
      key: "course_name",
    },
    {
      title: "Student Name",
      dataIndex: "student_name",
      key: "student_name",
    },
    {
      title: "Student Code",
      dataIndex: "student_code",
      key: "student_code",
    },
  ];

  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          authUserId: localStorage.getItem("user_id"),
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Payments Export ( Busy )"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={0}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        onChange={(dateRange: any) => {
                          if (dateRange) {
                            setFilter({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                            getReports({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                            getPayments({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <ExcelFile
                      filename={`${filter.start_date}-${filter.end_date}-payments`}
                      element={
                        <Button type="dashed" style={{ float: "right" }}>
                          Export Data
                        </Button>
                      }
                    >
                      {excel.LKR.length > 0 && (
                        <ExcelSheet dataSet={excel.LKR} name="LKR"></ExcelSheet>
                      )}
                      {excel.USD.length > 0 && (
                        <ExcelSheet dataSet={excel.USD} name="USD"></ExcelSheet>
                      )}
                      {excel.EUR.length > 0 && (
                        <ExcelSheet dataSet={excel.EUR} name="EUR"></ExcelSheet>
                      )}
                      {excel.GBP.length > 0 && (
                        <ExcelSheet dataSet={excel.GBP} name="GBP"></ExcelSheet>
                      )}
                    </ExcelFile>

                    <Table
                      columns={columns}
                      size="small"
                      dataSource={payment.payments.data}
                      loading={payment.payments.isLoading}
                      rowKey={(record) => record.id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { payment } = state;

  return {
    payment,
  };
};

const mapDispatchToProps = {
  getReports: Actions.payments.busy.get,
  getPayments: Actions.payments.all.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(BusyExport);
