import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Statistic,
  Drawer,
} from "antd";
import {
  $Select,
  $Input,
  $DatePicker,
  $RangePicker,
  $Radio,
  $AmountLabel,
} from "Components/antd";
import { Receipt } from "./New/Receipt";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
const { RangePicker } = DatePicker;
const Home = ({
  getPayments,
  payment,
  course,
  getCourses,
  getAllCurrencies,
}: any) => {
  const [filter, setFilter] = useState({
    reg_no: "",
    payment_type: "",
    payment_method: "",
    receipt_no: "",
    branch: "",
    course_id: "",
    payment_currency: "",
    company_type: "",
    start_date: moment().format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });

  const history = useHistory();
  const [isVisible, setIsVisible] = useState(false);
  const [printerData, setPrinterData] = useState<any>({});
  const [excel, setExcel] = useState<any>([{ data: [], columns: [] }]);

  useEffect(() => {
    getPayments(filter);
    getCourses();
    getAllCurrencies();
  }, []);

  useEffect(() => {
    let columns: any = [];
    const dataSource: any = [];

    payment.payments.data.map((data: any, index: number) => {
      if (index === 0) {
        columns = Object.keys(data).map((column: any) =>
          column.replace(/_/g, " ").toUpperCase()
        );
      }
    });

    payment.payments.data.map((data: any, index: number) => {
      dataSource.push(
        Object.values(data)
          .filter((value) => !Array.isArray(value))
          .map((value: any) => ({ value: value?.toString() }))
      );
    });

    setExcel([{ data: dataSource, columns }]);
  }, [payment.payments.data]);

  const drawerClose = () => setIsVisible(false);

  const getCurrencyWord = (key: any): string => {
    try {
      return payment.currencies?.data?.filter(
        ({ code }: any) => code == printerData.payment_currency
      )[0].currency_name;
    } catch (error) {
      return "";
    }
  };

  const getReceiptTitle = (payment_type: string) => {
    switch (payment_type) {
      case "1":
        return "COURSE FEE";
      case "2":
        return "UNI FEE";
      case "3":
        return "OTHER FEE";
      default:
        return "OTHER FEE";
    }
  };

  const columns = [
    {
      title: "Receipt No",
      dataIndex: "receipt_no",
      key: "receipt_no",
      render: (text: string) => {
        return <Tag color="red">{text}</Tag>;
      },
      width: 80,
    },
    {
      title: "Date",
      dataIndex: "payment_date",
      key: "payment_date",
    },
    {
      title: "Currency",
      dataIndex: "payment_currency",
      key: "payment_currency",
      render: (text: string) => {
        return <Tag color="success">{text}</Tag>;
      },
      width: 80,
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      render: (text: string, record: any) => (
        <$AmountLabel value={Number(record.amount)} />
      ),
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Payment Method",
      dataIndex: "payment_method",
      key: "payment_method",
    },
    {
      title: "Local|Uni",
      dataIndex: "payment_type",
      key: "payment_type",
      render: (text: string) => {
        if (text == "1") {
          return <Tag color="blue">Local</Tag>;
        } else if (text == "2") {
          return <Tag color="geekblue">Uni</Tag>;
        } else {
          return <Tag color="green">Other</Tag>;
        }
      },
    },
    {
      title: "Registration No",
      dataIndex: "reg_no",
      key: "reg_no",
      render: (text: string) => {
        return <Tag color="volcano">{text}</Tag>;
      },
    },
    {
      title: "Branch",
      dataIndex: "branch_name",
      key: "branch_name",
    },
    {
      title: "Course",
      dataIndex: "course_name",
      key: "course_name",
    },
    {
      title: "Student Name",
      dataIndex: "student_name",
      key: "student_name",
    },
    {
      title: "Student Code",
      dataIndex: "student_code",
      key: "student_code",
    },
    {
      title: "Received By",
      dataIndex: "received_user_name",
      key: "received_user_name",
    },
    {
      title: "Reference",
      dataIndex: "reference_url",
      key: "reference_url",
      render: (text: string) => {
        return <a href={text}>{text && text.split("/")[5]}</a>;
      },
    },
  ];
  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          dateRange: [moment(), moment()],
        }}
        onSubmit={(values) => {}}
      >
        <>
          <div className="">
            <Affix offsetTop={0}>
              <div className="page-header header-border">
                <PageHeader
                  className="px-0"
                  onBack={() => {}}
                  title="Payments"
                />
              </div>
            </Affix>

            <div className="regh-layout">
              <Affix offsetTop={0}>
                <aside className="regh-side">
                  <h3>Filter</h3>
                  <Form layout="vertical">
                    <$RangePicker
                      name="dateRange"
                      onChange={(dateRange: any) => {
                        if (dateRange) {
                          setFilter({
                            ...filter,
                            start_date: dateRange[0].format("YYYY-MM-DD"),
                            end_date: dateRange[1].format("YYYY-MM-DD"),
                          });
                          getPayments({
                            ...filter,
                            start_date: dateRange[0].format("YYYY-MM-DD"),
                            end_date: dateRange[1].format("YYYY-MM-DD"),
                          });
                        }
                      }}
                    />
                    <$Input
                      size="small"
                      placeholder="Registration No"
                      name="reg_no"
                      value={filter.reg_no}
                      onBlur={(e: any) => {
                        setFilter({ ...filter, reg_no: e.target.value });
                        getPayments({ ...filter, reg_no: e.target.value });
                      }}
                    />

                    <$Radio
                      formitem={{
                        label: "Payment Type",
                      }}
                      options={[
                        { value: "", label: "ALL" },
                        { value: "1", label: "Local Fee" },
                        { value: "2", label: "Uni Fee" },
                        { value: "3", label: "Other Fee" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      buttonStyle="solid"
                      size="small"
                      radioButton={true}
                      name="payment_type"
                      onChange={(e: any) => {
                        setFilter({ ...filter, payment_type: e.target.value });
                        getPayments({
                          ...filter,
                          payment_type: e.target.value,
                        });
                      }}
                    />

                    <$Radio
                      formitem={{
                        label: "Payment Method",
                      }}
                      options={[
                        { value: "", label: "ALL" },
                        { value: "cash", label: "Cash" },
                        { value: "card", label: "Card" },
                        { value: "bank", label: "Bank" },
                        { value: "cheque", label: "Cheque" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      size="small"
                      buttonStyle="solid"
                      radioButton={true}
                      name="payment_method"
                      onChange={(e: any) => {
                        setFilter({
                          ...filter,
                          payment_method: e.target.value,
                        });
                        getPayments({
                          ...filter,
                          payment_method: e.target.value,
                        });
                      }}
                    />

                    <$Radio
                      formitem={{
                        label: "Currency",
                      }}
                      options={[
                        { code: "", currency_name: "ALL" },
                        ...payment.currencies.data,
                      ]}
                      optionType="button"
                      optionText="currency_name"
                      optionValue="code"
                      size="small"
                      buttonStyle="solid"
                      radioButton={true}
                      name="payment_currency"
                      onChange={(e: any) => {
                        setFilter({
                          ...filter,
                          payment_currency: e.target.value,
                        });
                        getPayments({
                          ...filter,
                          payment_currency: e.target.value,
                        });
                      }}
                    />

                    <$Radio
                      formitem={{
                        label: "Company Type",
                      }}
                      options={[
                        { value: "", label: "ALL" },
                        { value: "1", label: "AIMS College" },
                        { value: "2", label: "AIMS Education" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      size="small"
                      buttonStyle="solid"
                      radioButton={true}
                      name="company_type"
                      onChange={(e: any) => {
                        setFilter({
                          ...filter,
                          company_type: e.target.value,
                        });
                        getPayments({
                          ...filter,
                          company_type: e.target.value,
                        });
                      }}
                    />

                    <$Input
                      size="small"
                      placeholder="Receipt No"
                      name="receipt_no"
                      value={filter.receipt_no}
                      onBlur={(e: any) => {
                        setFilter({ ...filter, receipt_no: e.target.value });
                        getPayments({ ...filter, receipt_no: e.target.value });
                      }}
                    />

                    <$Select
                      name="course_id"
                      size="small"
                      formitem={{
                        label: "Course",
                      }}
                      options={course.courses.data.filter(
                        ({ status }: { status: string }) => status === "1"
                      )}
                      optionValue="id"
                      optionText="name"
                      defaultValue="all"
                      value={filter.course_id}
                      allOption={true}
                      onChange={(course_id: any) => {
                        if (course_id == "all") {
                          getPayments({ ...filter, course_id: "" });
                          setFilter({ ...filter, course_id: "" });
                        } else {
                          getPayments({ ...filter, course_id });
                          setFilter({ ...filter, course_id });
                        }
                      }}
                    />
                  </Form>
                </aside>
              </Affix>
              <div className="flex-fill ">
                <div>
                  <ExcelFile
                    element={
                      <Button type="dashed" style={{ float: "right" }}>
                        Export Data
                      </Button>
                    }
                  >
                    <ExcelSheet dataSet={excel} name="Inquiries"></ExcelSheet>
                  </ExcelFile>

                  <Table
                    columns={columns}
                    size="small"
                    dataSource={payment.payments.data}
                    loading={payment.payments.isLoading}
                    rowKey={(record) => record.id}
                    onRow={(record, rowIndex) => {
                      return {
                        onDoubleClick: (event) => {
                          setIsVisible(true);
                          setPrinterData(record);
                        },
                      };
                    }}
                    summary={(pageData) => {
                      return (
                        <>
                          <Table.Summary.Row>
                            <Table.Summary.Cell index={11}>
                              <Statistic
                                valueStyle={{ fontSize: 18, fontWeight: 600 }}
                                title="SUM"
                                precision={2}
                                value={pageData.reduce((p, c) => {
                                  return p + Number(c.amount);
                                }, 0)}
                              />
                            </Table.Summary.Cell>
                          </Table.Summary.Row>
                        </>
                      );
                    }}
                  />
                </div>
              </div>
            </div>
          </div>

          <Drawer
            title={`AIMS RECEIPT`}
            placement="right"
            width={1200}
            onClose={() => {
              drawerClose();
            }}
            visible={isVisible}
          >
            {console.log(printerData)}
            <div id="aims-receipt-local" className="pdfobject-container">
              <Receipt
                printId={"aims-receipt-local"}
                receiptNo={printerData.receipt_no}
                paymentMethod={printerData.payment_method}
                amount={printerData.amount}
                date={printerData.payment_date}
                registrationNo={printerData.reg_no}
                studentNo={printerData.student_code}
                studentName={printerData.student_name}
                description={printerData.description}
                receivedBy={printerData.received_user_name}
                title={getReceiptTitle(printerData.payment_type)}
                currency={printerData.payment_currency}
                currencyWord={getCurrencyWord("")}
              />
            </div>
          </Drawer>
        </>
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { course, payment } = state;

  return {
    course,
    payment,
  };
};

const mapDispatchToProps = {
  getPayments: Actions.payments.all.get,
  getCourses: Actions.course.self.get,
  getAllCurrencies: Actions.payments.currencies.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
