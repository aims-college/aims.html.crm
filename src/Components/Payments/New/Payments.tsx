import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType, useField } from "formik";
import {
  BankOutlined,
  DollarCircleOutlined,
  InboxOutlined,
  PlusOutlined,
  AccountBookFilled,
} from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Upload,
  Divider,
  Tabs,
  Drawer,
  Table,
  Tag,
} from "antd";
import {
  $AmountLabel,
  $DatePicker,
  $Input,
  $Radio,
  $Select,
  $TextArea,
} from "Components";
import { Receipt } from "./Receipt";
import moment from "moment";
import "./receipt.css";
import { connect } from "react-redux";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import Validations from "./Validations";

const Payments: React.FC<any> = ({
  savePayment,
  studentPayment,
  getAllCurrencies,
  printDrawer,
  drawerClose,
  getPaymentsByRegId,
  currencies,
  bankAccounts,
  exising,
  getBankAccounts,
}) => {
  const { Option } = Select;
  const { Dragger } = Upload;
  const history = useHistory();

  useEffect(() => {
    getAllCurrencies();
    getBankAccounts();
  }, []);

  return (
    <>
      <Formik
        initialValues={studentPayment}
        validateOnBlur={true}
        validateOnChange={true}
        validateOnMount={true}
        isInitialValid={false}
        validationSchema={Validations}
        enableReinitialize
        onSubmit={(values, actions) => {
          const student_payments = [];

          if (values.localAmount > 0) {
            student_payments.push({
              payment_type: 1,
              payment_method: values.localPaymentMethod,
              amount: values.localAmount,
              payment_date: values.localPaymentDate,
              payment_currency: "LKR",
              currency_conversion_rate: "1",
              description: values.localDescription,
              account_id: values.account_id_local,
              reference_url:
                values.localRef && values.localRef.url
                  ? values.localRef.url
                  : null,
            });
          }

          if (values.uniAmount > 0) {
            student_payments.push({
              payment_type: 2,
              payment_method: values.uniPaymentMethod,
              amount: values.uniAmount,
              payment_date: values.uniPaymentDate,
              payment_currency: values.uniPaidCurrency,
              currency_conversion_rate: values.conversionRate,
              description: values.uniDescription,
              account_id: values.account_id_uni,
              reference_url:
                values.uniRef && values.uniRef.url ? values.uniRef.url : null,
            });
          }

          if (values.otherAmount > 0) {
            student_payments.push({
              payment_type: 3,
              payment_method: values.otherPaymentMethod,
              amount: values.otherAmount,
              payment_date: values.otherPaymentDate,
              payment_currency: "LKR",
              currency_conversion_rate: "1",
              description: values.otherDescription,
              account_id: values.account_id_other,
              reference_url:
                values.otherRef && values.otherRef.url
                  ? values.otherRef.url
                  : null,
            });
          }

          savePayment({
            registration_no: values.registration_no,
            student_payments,
          });

          actions.resetForm();
        }}
      >
        {({
          values,
          handleSubmit,
          errors,
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          setFieldValue,
          validateForm,
        }) => (
          <>
            <Form layout="vertical" style={{ margin: "50px" }}>
              <h2 className="mb-3">New Payment</h2>
              <Row gutter={24}>
                <Col>
                  <div className="d-flex justify-content-bitween align-items-start">
                    <div className="d-flex flex-column justify-content-end text-right">
                      <$Input
                        name="registration_no"
                        label="Registration No"
                        required
                        placeholder={"21-00020"}
                        onBlur={(e: any) => {
                          getPaymentsByRegId({
                            registration_no: e.target.value,
                          });
                        }}
                      />
                    </div>
                  </div>
                </Col>
                <Col>
                  <div className="d-flex justify-content-bitween align-items-start">
                    {exising.course_fee > 0 && (
                      <div className="d-flex flex-column justify-content-end text-right">
                        <h1 className="text-mute mb-0 price-lb">
                          <$AmountLabel value={exising.course_fee} />
                        </h1>
                        <div className="text-muted">Course Fee</div>
                      </div>
                    )}
                  </div>
                </Col>
                <Col>
                  {exising.student_name !== "" && (
                    <div className="d-flex justify-content-bitween align-items-start">
                      <div className="d-flex flex-column justify-content-end text-right">
                        <h1>
                          {exising.student_name} ({exising.branch_name})
                        </h1>
                        <div className="text-muted">{exising.course_name}</div>
                      </div>
                    </div>
                  )}
                </Col>
              </Row>
              <Divider></Divider>
              <Row gutter={24}>
                <Col span={10}>
                  <Tabs tabPosition={"left"}>
                    <Tabs.TabPane
                      key="1"
                      tab={
                        <span>
                          <BankOutlined />
                          Local
                        </span>
                      }
                    >
                      <Row gutter={24}>
                        <Col span={24}>
                          <div>
                            <div className="mb-4">
                              <$Radio
                                options={[
                                  { value: "cash", label: "Cash" },
                                  { value: "card", label: "Card" },
                                  { value: "bank", label: "Bank Transfer" },
                                  { value: "cheque", label: "Cheque" },
                                ]}
                                optionType="button"
                                optionText="label"
                                optionValue="value"
                                buttonStyle="solid"
                                radioButton={true}
                                size="small"
                                defaultValue={"cash"}
                                name="localPaymentMethod"
                              />
                            </div>

                            <Form.Item
                              label="Local Amount"
                              style={{ width: 300 }}
                              required
                            >
                              <$Input name="localAmount" />
                            </Form.Item>

                            <Form.Item
                              label="Payment Date"
                              style={{ width: 300 }}
                              required
                            >
                              <$DatePicker
                                name="localPaymentDate"
                                value={values.localPaymentDate}
                              />
                            </Form.Item>

                            {values.localPaymentMethod !== "cash" && (
                              <$Select
                                required
                                name="account_id_local"
                                size="medium"
                                formitem={{
                                  label: "Debit Bank",
                                }}
                                options={bankAccounts.data.filter(
                                  ({
                                    account_type,
                                  }: {
                                    account_type: string;
                                  }) =>
                                    (values.localPaymentMethod === "bank" &&
                                      account_type === "1") ||
                                    (values.localPaymentMethod === "card" &&
                                      account_type === "2")
                                )}
                                optionValue="id"
                                optionText="name"
                                allOption={false}
                              />
                            )}

                            <Form.Item label="Description">
                              <$TextArea
                                name="localDescription"
                                rows={6}
                                cols={6}
                              />
                            </Form.Item>

                            <Form.Item label="Reference">
                              <Upload
                                maxCount={1}
                                action={`${process.env.REACT_APP_API_URL}/file_upload`}
                                headers={{
                                  Authorization: `Bearer ${localStorage.getItem(
                                    "token"
                                  )}`,
                                }}
                                defaultFileList={
                                  values.localRef.url && [
                                    values.localRef as any,
                                  ]
                                }
                                onChange={({ fileList }) => {
                                  fileList &&
                                    Array.isArray(fileList) &&
                                    fileList.filter(
                                      ({ status }) => status === "done"
                                    ).length > 0 &&
                                    fileList.map(
                                      ({
                                        name,
                                        status,
                                        response: { file_path },
                                      }: any) => {
                                        if (status === "done") {
                                          setFieldValue("localRef", {
                                            name,
                                            url: file_path,
                                          });
                                        }
                                      }
                                    );
                                }}
                              >
                                <InboxOutlined
                                  style={{ fontSize: "64px !important" }}
                                />
                                <p className="ant-upload-text">
                                  Click or drag file to this area to upload
                                </p>
                                <p className="ant-upload-hint">
                                  Support for a single upload
                                </p>
                              </Upload>
                            </Form.Item>
                          </div>
                        </Col>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane
                      key="2"
                      tab={
                        <span>
                          <DollarCircleOutlined />
                          University
                        </span>
                      }
                    >
                      <Row gutter={24}>
                        <Col span={24}>
                          <div>
                            <div className="mb-4">
                              <$Radio
                                options={[
                                  { value: "cash", label: "Cash" },
                                  { value: "card", label: "Card" },
                                  { value: "bank", label: "Bank Transfer" },
                                  { value: "cheque", label: "Cheque" },
                                ]}
                                optionType="button"
                                optionText="label"
                                optionValue="value"
                                size="small"
                                buttonStyle="solid"
                                radioButton={true}
                                defaultValue={"bank"}
                                name="uniPaymentMethod"
                              />
                            </div>

                            <Form.Item
                              label="University Amount"
                              style={{ width: 300 }}
                            >
                              <$Input
                                name="uniAmount"
                                addonBefore={
                                  <$Select
                                    name="uniPaidCurrency"
                                    size="medium"
                                    defaultValue="USD"
                                    optionValue="code"
                                    allOption={false}
                                    optionText="currency_name"
                                    options={currencies.data}
                                  />
                                }
                              />
                            </Form.Item>

                            <Form.Item
                              label="Payment Date"
                              style={{ width: 300 }}
                              required
                            >
                              <$DatePicker
                                name="uniPaymentDate"
                                value={values.uniPaymentDate}
                              />
                            </Form.Item>

                            {values.uniPaymentMethod.value !== "cash" && (
                              <$Select
                                required
                                name="account_id_uni"
                                size="medium"
                                formitem={{
                                  label: "Debit Bank",
                                }}
                                options={bankAccounts.data.filter(
                                  ({
                                    account_type,
                                  }: {
                                    account_type: string;
                                  }) =>
                                    (values.uniPaymentMethod === "bank" &&
                                      account_type === "1") ||
                                    (values.uniPaymentMethod === "card" &&
                                      account_type === "2")
                                )}
                                optionValue="id"
                                optionText="name"
                                allOption={false}
                              />
                            )}
                            <Form.Item
                              label="Conversion Rate"
                              style={{ width: 100 }}
                            >
                              <$Input name="conversionRate" placeholder="220" />
                            </Form.Item>

                            <Form.Item label="Description">
                              <$TextArea
                                name="uniDescription"
                                rows={6}
                                cols={6}
                              />
                            </Form.Item>

                            <Form.Item label="Reference">
                              <Upload
                                maxCount={1}
                                action={`${process.env.REACT_APP_API_URL}/file_upload`}
                                headers={{
                                  Authorization: `Bearer ${localStorage.getItem(
                                    "token"
                                  )}`,
                                }}
                                defaultFileList={
                                  values.uniRef.url && [values.uniRef as any]
                                }
                                onChange={({ fileList }) => {
                                  fileList &&
                                    Array.isArray(fileList) &&
                                    fileList.filter(
                                      ({ status }) => status === "done"
                                    ).length > 0 &&
                                    fileList.map(
                                      ({
                                        name,
                                        status,
                                        response: { file_path },
                                      }: any) => {
                                        if (status === "done") {
                                          setFieldValue("uniRef", {
                                            name,
                                            url: file_path,
                                          });
                                        }
                                      }
                                    );
                                }}
                              >
                                <InboxOutlined
                                  style={{ fontSize: "64px !important" }}
                                />
                                <p className="ant-upload-text">
                                  Click or drag file to this area to upload
                                </p>
                                <p className="ant-upload-hint">
                                  Support for a single upload
                                </p>
                              </Upload>
                            </Form.Item>
                          </div>
                        </Col>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane
                      key="3"
                      tab={
                        <span>
                          <AccountBookFilled />
                          Other
                        </span>
                      }
                    >
                      <Row gutter={24}>
                        <Col span={24}>
                          <div>
                            <div className="mb-4">
                              <$Radio
                                options={[
                                  { value: "cash", label: "Cash" },
                                  { value: "card", label: "Card" },
                                  { value: "bank", label: "Bank Transfer" },
                                  { value: "cheque", label: "Cheque" },
                                ]}
                                optionType="button"
                                optionText="label"
                                optionValue="value"
                                buttonStyle="solid"
                                radioButton={true}
                                size="small"
                                defaultValue={"bank"}
                                name="otherPaymentMethod"
                              />
                            </div>

                            <Form.Item
                              label="Other Amount"
                              style={{ width: 300 }}
                            >
                              <$Input name="otherAmount" />
                            </Form.Item>

                            <Form.Item
                              label="Payment Date"
                              style={{ width: 300 }}
                              required
                            >
                              <$DatePicker
                                name="otherPaymentDate"
                                value={values.otherPaymentDate}
                              />
                            </Form.Item>

                            {values.otherPaymentMethod !== "cash" && (
                              <$Select
                                required
                                name="account_id_other"
                                size="medium"
                                formitem={{
                                  label: "Debit Bank",
                                }}
                                options={bankAccounts.data.filter(
                                  ({
                                    account_type,
                                  }: {
                                    account_type: string;
                                  }) =>
                                    (values.otherPaymentMethod === "bank" &&
                                      account_type === "1") ||
                                    (values.otherPaymentMethod === "card" &&
                                      account_type === "2")
                                )}
                                optionValue="id"
                                optionText="name"
                                allOption={false}
                              />
                            )}

                            <Form.Item label="Description">
                              <$TextArea
                                name="otherDescription"
                                rows={6}
                                cols={6}
                              />
                            </Form.Item>
                            <Form.Item label="Reference">
                              <Upload
                                maxCount={1}
                                action={`${process.env.REACT_APP_API_URL}/file_upload`}
                                headers={{
                                  Authorization: `Bearer ${localStorage.getItem(
                                    "token"
                                  )}`,
                                }}
                                defaultFileList={
                                  values.otherRef.url && [
                                    values.otherRef as any,
                                  ]
                                }
                                onChange={({ fileList }) => {
                                  fileList &&
                                    Array.isArray(fileList) &&
                                    fileList.filter(
                                      ({ status }) => status === "done"
                                    ).length > 0 &&
                                    fileList.map(
                                      ({
                                        name,
                                        status,
                                        response: { file_path },
                                      }: any) => {
                                        if (status === "done") {
                                          setFieldValue("otherRef", {
                                            name,
                                            url: file_path,
                                          });
                                        }
                                      }
                                    );
                                }}
                              >
                                <InboxOutlined
                                  style={{ fontSize: "64px !important" }}
                                />
                                <p className="ant-upload-text">
                                  Click or drag file to this area to upload
                                </p>
                                <p className="ant-upload-hint">
                                  Support for a single upload
                                </p>
                              </Upload>
                            </Form.Item>
                          </div>
                        </Col>
                      </Row>
                    </Tabs.TabPane>
                  </Tabs>
                </Col>
                <Col span={14}>
                  <Tabs>
                    <Tabs.TabPane key="11" tab={<span>Installments</span>}>
                      <Table
                        columns={[
                          {
                            title: "#",
                            dataIndex: "installment_no",
                            key: "installment_no",
                          },
                          {
                            title: "Due Date",
                            dataIndex: "due_date",
                            key: "due_date",
                          },
                          {
                            title: "Local Amount",
                            dataIndex: "local_amount",
                            key: "local_amount",
                          },

                          {
                            title: "Local Paid Amount",
                            dataIndex: "local_paid_amount",
                            key: "local_paid_amount",
                          },
                          {
                            title: "Uni Amount",
                            dataIndex: "uni_amount",
                            key: "uni_amount",
                          },
                          {
                            title: "Uni Paid Amount",
                            dataIndex: "uni_paid_amount",
                            key: "uni_paid_amount",
                          },
                        ]}
                        size="small"
                        dataSource={exising.installments}
                        loading={exising.isLoading}
                        rowKey={(record) => record.id}
                      />
                    </Tabs.TabPane>
                    <Tabs.TabPane key="12" tab={<span>Payments</span>}>
                      <Table
                        columns={[
                          {
                            title: "Receipt No",
                            dataIndex: "receipt_no",
                            key: "receipt_no",
                          },
                          {
                            title: "Date",
                            dataIndex: "payment_date",
                            key: "payment_date",
                          },
                          {
                            title: "Type",
                            dataIndex: "payment_type",
                            key: "payment_type",
                            render: (text: string) => {
                              if (text == "1") {
                                return <Tag color="blue">Local</Tag>;
                              } else if (text == "2") {
                                return <Tag color="geekblue">Uni</Tag>;
                              } else {
                                return <Tag color="green">Other</Tag>;
                              }
                            },
                          },
                          {
                            title: "Method",
                            dataIndex: "payment_method",
                            key: "payment_method",
                          },
                          {
                            title: "Amount",
                            dataIndex: "amount",
                            key: "amount",
                          },
                          {
                            title: "Received By",
                            dataIndex: "received_user_name",
                            key: "received_user_name",
                          },
                          {
                            title: "Description",
                            dataIndex: "description",
                            key: "description",
                          },
                          {
                            title: "Reference",
                            dataIndex: "reference_url",
                            key: "reference_url",
                            render: (text: string) => {
                              return (
                                <a href={text}>{text && text.split("/")[5]}</a>
                              );
                            },
                          },
                        ]}
                        size="small"
                        dataSource={exising.payments}
                        loading={exising.isLoading}
                        rowKey={(record) => record.id}
                      />
                    </Tabs.TabPane>
                  </Tabs>
                </Col>
              </Row>
            </Form>
            <Drawer
              title={`AIMS RECEIPT`}
              placement="right"
              width={1200}
              onClose={() => {
                drawerClose();
                history.goBack();
              }}
              visible={printDrawer.isVisible}
            >
              <Tabs>
                {values.localAmount !== 0 && (
                  <Tabs.TabPane
                    disabled={values.localAmount == 0}
                    key="11"
                    tab={
                      <span>
                        <BankOutlined />
                        Local
                      </span>
                    }
                  >
                    <div
                      id="aims-receipt-local"
                      className="pdfobject-container"
                    >
                      <Receipt
                        printId={"aims-receipt-local"}
                        receiptNo={studentPayment.localReceiptNo}
                        paymentMethod={studentPayment.localPaymentMethod}
                        amount={studentPayment.localAmount}
                        date={moment(studentPayment.localPaymentDate).format(
                          "YYYY-MMM-DD"
                        )}
                        registrationNo={studentPayment.registrationNo}
                        studentNo={studentPayment.studentNo}
                        studentName={studentPayment.studentName}
                        description={studentPayment.localDescription}
                        receivedBy={studentPayment.received_user_name}
                        title={"COURSE FEE"}
                        currency={studentPayment.localPaidCurrency}
                        currencyWord={
                          currencies?.data?.filter(
                            ({ code }: any) =>
                              code == studentPayment.localPaidCurrency
                          )[0].currency_name
                        }
                      />
                    </div>
                  </Tabs.TabPane>
                )}

                {values.uniAmount !== 0 && (
                  <Tabs.TabPane
                    disabled={values.uniAmount == 0}
                    key="22"
                    tab={
                      <span>
                        <DollarCircleOutlined />
                        University
                      </span>
                    }
                  >
                    <div id="aims-receipt-uni" className="pdfobject-container">
                      <Receipt
                        printId={"aims-receipt-uni"}
                        receiptNo={studentPayment.uniReceiptNo}
                        paymentMethod={studentPayment.uniPaymentMethod}
                        amount={studentPayment.uniAmount}
                        date={moment(studentPayment.uniPaymentDate).format(
                          "YYYY-MMM-DD"
                        )}
                        registrationNo={studentPayment.registrationNo}
                        studentNo={studentPayment.studentNo}
                        studentName={studentPayment.studentName}
                        description={studentPayment.uniDescription}
                        receivedBy={studentPayment.received_user_name}
                        title={"UNI FEE"}
                        currency={studentPayment.uniPaidCurrency}
                        currencyWord={
                          currencies?.data?.filter(
                            ({ code }: any) =>
                              code == studentPayment.uniPaidCurrency
                          )[0].currency_name
                        }
                      />
                    </div>
                  </Tabs.TabPane>
                )}

                {values.otherAmount !== 0 && (
                  <Tabs.TabPane
                    disabled={values.otherAmount == 0}
                    key="33"
                    tab={
                      <span>
                        <AccountBookFilled />
                        Other
                      </span>
                    }
                  >
                    <div
                      id="aims-receipt-other"
                      className="pdfobject-container"
                    >
                      <Receipt
                        printId={"aims-receipt-other"}
                        receiptNo={studentPayment.otherReceiptNo}
                        paymentMethod={studentPayment.otherPaymentMethod}
                        amount={studentPayment.otherAmount}
                        date={moment(studentPayment.otherPaymentDate).format(
                          "YYYY-MMM-DD"
                        )}
                        registrationNo={studentPayment.registrationNo}
                        studentNo={studentPayment.studentNo}
                        studentName={studentPayment.studentName}
                        description={studentPayment.otherDescription}
                        receivedBy={studentPayment.received_user_name}
                        title={"OTHER FEE"}
                        currency={studentPayment.otherPaidCurrency}
                        currencyWord={
                          currencies?.data?.filter(
                            ({ code }: any) =>
                              code == studentPayment.otherPaidCurrency
                          )[0].currency_name
                        }
                      />
                    </div>
                  </Tabs.TabPane>
                )}
              </Tabs>
            </Drawer>
            <div className="form-bottom d-flex justify-content-end">
              <Button
                type="default"
                onClick={(e: any) => handleSubmit(e)}
                disabled={isSubmitting || isValidating || !dirty || !isValid}
              >
                Save
              </Button>

              <Button type="default" onClick={() => history.goBack()}>
                Cancel
              </Button>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};
const mapStateToProps = (state: any) => {
  const { inquiry, course, user, payment, branch } = state;

  const { studentPayment, printDrawer, exising, currencies, bankAccounts } =
    payment;

  return {
    inquiry,
    course,
    user,
    studentPayment,
    exising,
    branch,
    printDrawer,
    currencies,
    bankAccounts,
  };
};

const mapDispatchToProps = {
  savePayment: Actions.payments.save.post,
  drawerClose: Actions.payments.drawer.close,
  getPaymentsByRegId: Actions.payments.paymentsByRegId.get,
  getAllCurrencies: Actions.payments.currencies.get,
  getBankAccounts: Actions.payments.bankAccounts.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Payments);
