import { message } from "antd";
import * as Yup from "yup";

export default () => {
  return Yup.object().shape({
    registration_no: Yup.string()
      .required("Registration NO is required")
      .typeError("Registration NO is invalid"),
    localAmount: Yup.number().typeError("Local Fee is invalid"),
    uniAmount: Yup.number().typeError("University Fee is invalid"),
  });
};
