import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Popconfirm,
} from "antd";

import {
  $Select,
  $Input,
  $DatePicker,
  $RangePicker,
  $AmountLabel,
} from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";

const ReportsOutstanding = ({ getReports, payment }: any) => {
  const [filter, setFilter] = useState({
    report_type: 1,
    start_date: moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });

  const history = useHistory();

  useEffect(() => {
    getReports(filter);
  }, []);

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "By Type",
        key: "name",
        dataIndex: "name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Local Revenue",
        dataIndex: "local_revenue",
        key: "local_revenue",
        width: 110,
        ellipsis: true,
        render: (text: any, record: any) => {
          return (
            <$AmountLabel value={isNaN(Number(text)) ? 0 : Number(text)} />
          );
        },
      },
      {
        title: "Local Outstanding",
        dataIndex: "local_outstanding",
        key: "local_outstanding",
        width: 110,
        ellipsis: true,
        render: (text: any, record: any) => {
          return (
            <$AmountLabel value={isNaN(Number(text)) ? 0 : Number(text)} />
          );
        },
      },
      {
        title: "Uni Revenue",
        dataIndex: "uni_revenue",
        key: "uni_revenue",
        width: 110,
        ellipsis: true,
        children: [
          {
            title: "LKR",
            dataIndex: "LKR",
            key: "LKR",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_revenue.LKR))
                      ? 0
                      : Number(record.uni_revenue.LKR)
                  }
                />
              );
            },
          },
          {
            title: "USD",
            dataIndex: "USD",
            key: "USD",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_revenue.USD))
                      ? 0
                      : Number(record.uni_revenue.USD)
                  }
                />
              );
            },
          },
          {
            title: "EUR",
            dataIndex: "EUR",
            key: "EUR",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_revenue.EUR))
                      ? 0
                      : Number(record.uni_revenue.EUR)
                  }
                />
              );
            },
          },
          {
            title: "GBP",
            dataIndex: "GBP",
            key: "GBP",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_revenue.GBP))
                      ? 0
                      : Number(record.uni_revenue.GBP)
                  }
                />
              );
            },
          },
        ],
      },
      {
        title: "Uni Outstanding",
        dataIndex: "uni_outstanding",
        key: "uni_outstanding",
        width: 110,
        ellipsis: true,
        children: [
          {
            title: "LKR",
            dataIndex: "LKR",
            key: "LKR",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_outstanding.LKR))
                      ? 0
                      : Number(record.uni_outstanding.LKR)
                  }
                />
              );
            },
          },
          {
            title: "USD",
            dataIndex: "USD",
            key: "USD",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_outstanding.USD))
                      ? 0
                      : Number(record.uni_outstanding.USD)
                  }
                />
              );
            },
          },
          {
            title: "EUR",
            dataIndex: "EUR",
            key: "EUR",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_outstanding.EUR))
                      ? 0
                      : Number(record.uni_outstanding.EUR)
                  }
                />
              );
            },
          },
          {
            title: "GBP",
            dataIndex: "GBP",
            key: "GBP",
            width: 100,
            render: (text: any, record: any) => {
              return (
                <$AmountLabel
                  value={
                    isNaN(Number(record.uni_outstanding.GBP))
                      ? 0
                      : Number(record.uni_outstanding.GBP)
                  }
                />
              );
            },
          },
        ],
      },
    ];
  };
  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          authUserId: localStorage.getItem("user_id"),
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Outstanding Reports"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={0}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        onChange={(dateRange: any) => {
                          if (dateRange) {
                            setFilter(() => {
                              return {
                                ...filter,
                                start_date: dateRange[0].format("YYYY-MM-DD"),
                                end_date: dateRange[1].format("YYYY-MM-DD"),
                              };
                            });
                            getReports({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                          }
                        }}
                      />

                      <$Select
                        name="report_type"
                        size="small"
                        formitem={{
                          label: "Type",
                        }}
                        options={[
                          {
                            id: 1,
                            name: "By Branch",
                          },
                          {
                            id: 2,
                            name: "By Course",
                          },
                          {
                            id: 3,
                            name: "By Faculty",
                          },
                          {
                            id: 4,
                            name: "By Student",
                          },
                        ]}
                        value={filter.report_type}
                        optionValue="id"
                        optionText="name"
                        defaultValue="all"
                        allOption={false}
                        onChange={(report_type: any) => {
                          if (report_type == "all") {
                            setFilter({ ...filter, report_type: -1 });
                            getReports({ ...filter, report_type: -1 });
                          } else {
                            setFilter({ ...filter, report_type });
                            getReports({ ...filter, report_type });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={payment.outstanding.data}
                      loading={payment.outstanding.isLoading}
                      rowKey={(record) => record.id}
                      onRow={(record, rowIndex) => {
                        return {
                          onDoubleClick: (event) => {
                            history.push(
                              `/payments/outstanding/students?report_type=${filter.report_type}&type_id=${record.id}&start_date=${filter.start_date}&end_date=${filter.end_date}`,
                              record
                            );
                          },
                        };
                      }}
                     
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { payment } = state;

  return {
    payment,
  };
};

const mapDispatchToProps = {
  getReports: Actions.payments.outstanding.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportsOutstanding);
