import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Popconfirm,
} from "antd";

import {
  $Select,
  $Input,
  $DatePicker,
  $RangePicker,
  $AmountLabel,
} from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import queryString from "query-string";

const ReportsOutstanding = ({ getReports, payment }: any) => {
  const history = useHistory();
  const params = queryString.parse(history.location.search);

  const [filter, setFilter] = useState({
    report_type: Number(params.report_type),
    type_id: params.type_id,
    start_date: params.start_date,
    end_date: params.end_date,
  });

  useEffect(() => {
    getReports(filter);
  }, []);

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "Student Name",
        key: "student_name",
        dataIndex: "student_name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Reg No",
        key: "reg_no",
        dataIndex: "reg_no",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Student Code",
        key: "student_code",
        dataIndex: "student_code",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Register Date",
        key: "register_date",
        dataIndex: "register_date",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Contact No",
        key: "contact_no_1",
        dataIndex: "contact_no_1",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Branch",
        key: "branch_name",
        dataIndex: "branch_name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Course",
        key: "course_name",
        dataIndex: "course_name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Faculty",
        key: "faculty_name",
        dataIndex: "faculty_name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Local Revenue",
        dataIndex: "local_revenue",
        key: "local_revenue",
        width: 100,
        render: (text: any, record: any) => {
          return (
            <$AmountLabel
              value={
                isNaN(Number(record.local_revenue))
                  ? 0
                  : Number(record.local_revenue)
              }
            />
          );
        },
      },
      {
        title: "Local outstanding",
        dataIndex: "local_outstanding",
        key: "local_outstanding",
        width: 100,
        render: (text: any, record: any) => {
          return (
            <$AmountLabel
              value={
                isNaN(Number(record.local_outstanding))
                  ? 0
                  : Number(record.local_outstanding)
              }
            />
          );
        },
      },
      {
        title: "Uni Revenue",
        dataIndex: "uni_revenue",
        key: "uni_revenue",
        width: 100,
        render: (text: any, record: any) => {
          return (
            <$AmountLabel
              value={
                isNaN(Number(record.uni_revenue))
                  ? 0
                  : Number(record.uni_revenue)
              }
            />
          );
        },
      },
      {
        title: "Uni Outstanding",
        dataIndex: "uni_outstanding",
        key: "uni_outstanding",
        width: 100,
        render: (text: any, record: any) => {
          return (
            <$AmountLabel
              value={
                isNaN(Number(record.uni_outstanding))
                  ? 0
                  : Number(record.uni_outstanding)
              }
            />
          );
        },
      }
    ]
  };
  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          authUserId: localStorage.getItem("user_id"),
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Outstanding Reports"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={0}>
                  {/* <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        onChange={(dateRange: any) => {
                          if (dateRange) {
                            setFilter(() => {
                              return {
                                ...filter,
                                start_date: dateRange[0].format("YYYY-MM-DD"),
                                end_date: dateRange[1].format("YYYY-MM-DD"),
                              };
                            });
                            getReports({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                          }
                        }}
                      />

                      <$Select
                        name="report_type"
                        size="small"
                        formitem={{
                          label: "Type",
                        }}
                        options={[
                          {
                            id: 1,
                            name: "By Branch",
                          },
                          {
                            id: 2,
                            name: "By Course",
                          },
                          {
                            id: 3,
                            name: "By Faculty",
                          },
                          {
                            id: 4,
                            name: "By Student",
                          },
                        ]}
                        value={filter.report_type}
                        optionValue="id"
                        optionText="name"
                        defaultValue="all"
                        allOption={false}
                        onChange={(report_type: any) => {
                          if (report_type == "all") {
                            setFilter({ ...filter, report_type: -1 });
                            getReports({ ...filter, report_type: -1 });
                          } else {
                            setFilter({ ...filter, report_type });
                            getReports({ ...filter, report_type });
                          }
                        }}
                      />
                    </Form>
                  </aside> */}
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={payment.outstandingStudent.data}
                      loading={payment.outstandingStudent.isLoading}
                      rowKey={(record) => record.id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { payment } = state;

  return {
    payment,
  };
};

const mapDispatchToProps = {
  getReports: Actions.payments.outstandingStudent.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportsOutstanding);
