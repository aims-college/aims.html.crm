import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Popconfirm,
} from "antd";

import { $Select, $Input, $DatePicker, $RangePicker } from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const BusyExport = ({ getReports, registration, getRegistrations }: any) => {
  const [filter, setFilter] = useState({
    report_type: 1,
    start_date: moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });
  const [excel, setExcel] = useState<any>([{ data: [], columns: [] }]);

  useEffect(() => {
    const dataSource: any = [];
    let columns: any = [];

    registration.busyInstallments.data.map((data: any, index: number) => {
      if (index === 0) {
        columns = Object.keys(data);
      }
    });

    registration.busyInstallments.data.map((data: any, index: number) => {
      dataSource.push(
        Object.values(data)
          .filter((value) => !Array.isArray(value))
          .map((value: any) => ({ value: value?.toString() }))
      );
    });

    setExcel([{ data: dataSource, columns }]);
  }, [registration.busyInstallments.data]);

  const history = useHistory();

  useEffect(() => {
    getRegistrations(filter);
    getReports(filter)
  }, [filter]);

  const columns = [
    {
      title: "Registration No",
      dataIndex: "id",
      key: "id",
      render: (text: string, record: any) => {
        return <Tag color="red">{record.student_registration.reg_no}</Tag>;
      },
    },
    {
      title: "Registration Date",
      dataIndex: "registrationDate",
      key: "id",
      render: (text: string, record: any) => {
        return <>{record.student_registration.register_date}</>;
      },
    },
    {
      title: "Student Code",
      dataIndex: "student_code",
      render: (text: string, record: any) => {
        return (
          <Tag color="blue">{record.student_registration.student_code}</Tag>
        );
      },
    },
    {
      title: "Name",
      dataIndex: "firstName",
      render: (text: string, record: any) => {
        return (
          <>
            {" "}
            {record.first_name} {record.middle_name} {record.last_name}{" "}
          </>
        );
      },
    },
    {
      title: "NIC/Passport",
      dataIndex: "nic",
    },
    {
      title: "Name In Certificate",
      dataIndex: "name_in_certificate",
    },
    {
      title: "DOB",
      dataIndex: "dob",
    },
    {
      title: "Gender",
      dataIndex: "gender",
    },
    {
      title: "Course",
      dataIndex: "course_name",
      render: (text: string, record: any) => {
        return <> {record.student_registration.course_name} </>;
      },
    },
  ];

  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          authUserId: localStorage.getItem("user_id"),
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Installments Export ( Busy )"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={0}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        onChange={(dateRange: any) => {
                          console.log(dateRange);
                          if (dateRange) {
                            setFilter({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                            // getReports({
                            //   ...filter,
                            //   start_date: dateRange[0].format("YYYY-MM-DD"),
                            //   end_date: dateRange[1].format("YYYY-MM-DD"),
                            // });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <ExcelFile
                      filename={`${filter.start_date}-${filter.end_date}-Installments`}
                      element={
                        <Button type="dashed" style={{ float: "right" }}>
                          Export Data
                        </Button>
                      }
                    >
                      <ExcelSheet
                        dataSet={excel}
                        name="Installments"
                      ></ExcelSheet>
                    </ExcelFile>

                    <Table
                      columns={columns}
                      size="small"
                      dataSource={registration.all.data}
                      loading={registration.all.isLoading}
                      rowKey={(row) => row.id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { registration } = state;

  return {
    registration,
  };
};

const mapDispatchToProps = {
  getRegistrations: Actions.registration.all.get,
  getReports: Actions.registration.busyInstallments.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(BusyExport);
