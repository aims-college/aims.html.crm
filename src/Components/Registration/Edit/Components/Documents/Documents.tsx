import React, { useState, useRef, useEffect } from "react";
import { Formik, FieldArray, useField } from "formik";
import {
  MinusCircleOutlined,
  PlusOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Tag,
  Divider,
  Upload,
} from "antd";
import {
  $Select,
  $Input,
  $DatePicker,
  $DateLabel,
  $RangePicker,
  $Switch,
} from "Components/antd";

// 1) CV
// 2) OL/AL Result sheet
// 3) NIC
// 4) Passport Front Page/ Driving license, English Translated BC Copy
// 5) passport size 1 photo graph
// 6) Diploma Certificate
// 7) Working Experience letter
// 8) Degree certificate and transcript

const Documents: React.FC<any> = (props) => {
  const { Option } = Select;
  const { RangePicker } = DatePicker;
  const [documents, , documentsHelper] = useField("documents");
  const [nameInCertificate] = useField("nameInCertificate");

  return (
    <>
      <h2 className="mb-0">Documents</h2>
      <Form autoComplete="off">
        <FieldArray
          name="documents"
          render={({ push, remove, insert, replace }) => (
            <>
              {documents.value.map((data: any, index: any) => (
                <div key={index}>
                  <Row gutter={16}>
                    <Col span={6}>{data.document}</Col>
                    <Col span={2}>
                      <Form.Item>
                        <$Switch
                          label="Check"
                          placeholder="Checked"
                          name={`documents[${index}].checked`}
                          checked={data.checked}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={3}>
                      <Upload
                        maxCount={1}
                        action={`${process.env.REACT_APP_API_URL}/file_upload`}
                        headers={{
                          Authorization: `Bearer ${localStorage.getItem(
                            "token"
                          )}`,
                        }}
                        defaultFileList={data.url && [{
                          url: data?.url,
                          name: data?.name
                        } as any]}
                        onChange={({ fileList }) => {
                          fileList &&
                            Array.isArray(fileList) &&
                            fileList.filter(
                              ({ status }) =>
                                status === "done"
                            ).length > 0 &&
                            fileList.map(
                              ({
                                name,
                                status,
                                response: { file_path },
                              }: any) => {
                                if (status === "done") {
                                  replace(index, {
                                    ...data,
                                    name,
                                    url: file_path,
                                  });
                                }
                              }
                            );
                        }}
                      >
                        <Button icon={<UploadOutlined />}>Upload</Button>
                      </Upload>
                    </Col>
                  </Row>
                </div>
              ))}
            </>
          )}
        />
      </Form>

    </>
  );
};

export default Documents;
