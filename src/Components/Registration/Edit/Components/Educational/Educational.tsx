import React, { useState, useRef, useEffect } from "react";
import { Formik, FieldArray, useField } from "formik";
import {
  MinusCircleOutlined,
  PlusOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Tag,
  Divider,
  Upload,
} from "antd";
import { $Select, $Input, $DatePicker, $DateLabel } from "Components/antd";

const Educational: React.FC<any> = (props) => {
  const { Option } = Select;
  const [educational] = useField("educational");
  const [coursesTaken] = useField("coursesTaken");

  return (
    <>
      <h2>Educational Attainment</h2>
      <Form autoComplete="off">
        <FieldArray
          name="educational"
          render={({ push, remove, insert, replace }) => (
            <>
              {educational.value.map(
                (data: any, index: any) =>
                  (data.is_updated == 1 || data.is_new == 1) && (
                    <div key={index}>
                      <Row gutter={16}>
                        <Col span={4}>
                          <$Select
                            name={`educational[${index}].edu_level`}
                            size="medium"
                            formitem={{
                              label: "",
                            }}
                            placeholder="Level"
                            options={[
                              { value: "High School", label: "High School" },
                              {
                                value: "Undergraduate",
                                label: "Undergraduate",
                              },
                              {
                                value: "Graduate School 1",
                                label: "Graduate School 1",
                              },
                              {
                                value: "Graduate School 2",
                                label: "Graduate School 2",
                              },
                            ]}
                            allOption={false}
                            required
                          />
                        </Col>
                        <Col span={4}>
                          <Form.Item>
                            <$DatePicker
                              className="w-100"
                              name={`educational[${index}].inclusive_date`}
                              placeholder="Inclusive Dates"
                              value={educational.value[index].inclusive_date}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={4}>
                          <Form.Item>
                            <$Input
                              placeholder="School Name and Address"
                              name={`educational[${index}].school_name`}
                              required
                            />
                          </Form.Item>
                        </Col>
                        <Col span={4}>
                          <Form.Item>
                            <$Input
                              placeholder="Degree"
                              name={`educational[${index}].degree`}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={3}>
                          <Form.Item>
                            <$Input
                              placeholder="Major Field"
                              name={`educational[${index}].major_field`}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={3}>
                          <Upload
                            maxCount={1}
                            action={`${process.env.REACT_APP_API_URL}/file_upload`}
                            headers={{
                              Authorization: `Bearer ${localStorage.getItem(
                                "token"
                              )}`,
                            }}
                            defaultFileList={
                              data.url && [
                                {
                                  url: data?.url,
                                  name: data?.name,
                                } as any,
                              ]
                            }
                            onChange={({ fileList }) => {
                              fileList &&
                                Array.isArray(fileList) &&
                                fileList.filter(
                                  ({ status }) => status === "done"
                                ).length > 0 &&
                                fileList.map(
                                  ({
                                    name,
                                    status,
                                    response: { file_path },
                                  }: any) => {
                                    if (status === "done") {
                                      replace(index, {
                                        ...data,
                                        name,
                                        url: file_path,
                                      });
                                    }
                                  }
                                );
                            }}
                          >
                            <Button icon={<UploadOutlined />}>Upload</Button>
                          </Upload>
                        </Col>
                        <Col span={1}>
                          <MinusCircleOutlined
                            onClick={() =>
                              replace(index, {
                                ...data,
                                is_new: 0,
                                is_deleted: 1,
                                is_updated: 0,
                              })
                            }
                          />
                        </Col>
                      </Row>
                    </div>
                  )
              )}
              <div className="d-flex mb-5">
                <Form.Item className="mt-4">
                  <Button
                    type="dashed"
                    onClick={() =>
                      push({
                        edu_level: "",
                        inclusive_date: "",
                        school_name: "",
                        degree: "",
                        major_field: "",
                        name: "",
                        url: "",
                        is_new: 1,
                        is_deleted: 0,
                        is_updated: 0,
                      })
                    }
                    block
                    icon={<PlusOutlined />}
                  >
                    Add field
                  </Button>
                </Form.Item>
              </div>
            </>
          )}
        />

        <h2>Pre-Requisite Courses Taken</h2>
        <FieldArray
          name="coursesTaken"
          render={({ push, remove, insert, replace }) => (
            <>
              {coursesTaken.value.map(
                (data: any, index: any) =>
                  (data.is_updated == 1 || data.is_new == 1) && (
                    <div key={index}>
                      <Row gutter={16}>
                        <Col span={2}>
                          <Form.Item required>
                            <$Input
                              placeholder="Level"
                              name={`coursesTaken[${index}].level`}
                              required
                            />
                          </Form.Item>
                        </Col>
                        <Col span={4}>
                          <Form.Item>
                            <$Input
                              placeholder="Course Description"
                              name={`coursesTaken[${index}].course_description`}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={4}>
                          <Form.Item>
                            <$Input
                              placeholder="School Nameand Address"
                              name={`coursesTaken[${index}].school_name`}
                              required
                            />
                          </Form.Item>
                        </Col>
                        <Col span={2}>
                          <Form.Item>
                            <$Input
                              placeholder="Credit Units"
                              name={`coursesTaken[${index}].credit_units`}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={2}>
                          <Form.Item>
                            <$Input
                              placeholder="Grade"
                              name={`coursesTaken[${index}].grade`}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={3}>
                          <Form.Item>
                            <$Input
                              placeholder="Year Taken"
                              name={`coursesTaken[${index}].year_taken`}
                            />
                          </Form.Item>
                        </Col>
                        <Col span={3}>
                          <Upload
                            maxCount={1}
                            action={`${process.env.REACT_APP_API_URL}/file_upload`}
                            headers={{
                              Authorization: `Bearer ${localStorage.getItem(
                                "token"
                              )}`,
                            }}
                            defaultFileList={
                              data.url && [
                                {
                                  url: data?.url,
                                  name: data?.name,
                                } as any,
                              ]
                            }
                            onChange={({ fileList }) => {
                              fileList &&
                                Array.isArray(fileList) &&
                                fileList.filter(
                                  ({ status }) => status === "done"
                                ).length > 0 &&
                                fileList.map(
                                  ({
                                    name,
                                    status,
                                    response: { file_path },
                                  }: any) => {
                                    if (status === "done") {
                                      replace(index, {
                                        ...data,
                                        name,
                                        url: file_path,
                                      });
                                    }
                                  }
                                );
                            }}
                          >
                            <Button icon={<UploadOutlined />}>Upload</Button>
                          </Upload>
                        </Col>
                        <Col span={1}>
                          <MinusCircleOutlined
                            onClick={() =>
                              replace(index, {
                                ...data,
                                is_new: 0,
                                is_deleted: 1,
                                is_updated: 0,
                              })
                            }
                          />
                        </Col>
                      </Row>
                    </div>
                  )
              )}
              <div className="d-flex mb-5">
                <Form.Item className="mt-4">
                  <Button
                    type="dashed"
                    onClick={() =>
                      push({
                        level: "",
                        course_description: "",
                        school_name: "",
                        credit_units: "",
                        grade: "",
                        year_taken: "",
                        name: "",
                        url: "",
                        is_new: 1,
                        is_deleted: 0,
                        is_updated: 0,
                      })
                    }
                    block
                    icon={<PlusOutlined />}
                  >
                    Add field
                  </Button>
                </Form.Item>
              </div>
            </>
          )}
        />
      </Form>
    </>
  );
};

export default Educational;
