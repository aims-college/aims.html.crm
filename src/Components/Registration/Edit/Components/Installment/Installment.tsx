import React, { useState, useRef, useEffect } from "react";
import {
  Formik,
  FormikContextType,
  useField,
  FieldArray,
  ErrorMessage,
} from "formik";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Spin,
  Divider,
  message,
  Alert,
} from "antd";
import { connect } from "react-redux";
import * as Actions from "Actions";
import {
  $Select,
  $Input,
  $DatePicker,
  $DateLabel,
  $AmountLabel,
} from "Components/antd";
import { getInstallments, getDiscountedInstallmemts } from "Functions";
import "./Installment.scss";
import moment from "moment";

const Installment: React.FC<any> = ({
  getUsers,
  getDiscounts,
  discounts,
  user,
}) => {
  const { Option } = Select;
  const { RangePicker } = DatePicker;

  const [fetching, setFetching] = React.useState(false);
  const [installments, installmentsMeta, installmentsHelper] =
    useField("installments");
  const [courseDuration, , courseDurationHelper] = useField("courseDuration");
  const [courseFee, , courseFeeHelper] = useField("courseFee");
  const [method, , methodFeeHelper] = useField("method");
  const [discountId, , discountIdHelper] = useField("discountId");
  const [discountAmount, , discountAmountFeeHelper] =
    useField("discountAmount");

  return (
    <>
      <Row gutter={24}>
        <Col span={6}>
          <h2 className="mb-3">Discount</h2>
          <Form layout="vertical">
            <$Input
              disabled
              name="method"
              label="No of Installments"
              onBlur={(e: any) => {
                installmentsHelper.setValue(
                  getInstallments(e.target.value, courseDuration, 'USD', 1)
                );
                message.loading("Generating Installments ...");
              }}
            />
            {/* <$Select
              name="method"
              size="medium"
              formitem={{
                label: "Method",
              }}
              options={[
                {
                  id: 1,
                  label: "Monthly",
                },
                {
                  id: 4,
                  label: "Quarterly",
                },
                {
                  id: 6,
                  label: "Half yearly",
                },
                {
                  id: 12,
                  label: "Yearly",
                },
              ]}
              
              optionValue="id"
              optionText="label"
              allOption={false}
            /> */}

            <$Select
              name="discountId"
              size="medium"
              formitem={{
                label: "Discount by category",
              }}
              disabled
              options={discounts.data}
              optionValue="discount"
              optionText="name"
              allOption={false}
              onChange={(val: any) => {
                discountAmountFeeHelper.setValue((courseFee.value * val) / 100);

                getDiscountedInstallmemts(
                  val,
                  getInstallments(method.value, courseDuration, 'USD', 1),
                  installmentsHelper.setValue,
                  courseFee.value,
                  "byPercentage"
                );
              }}
            />

            <$Input
              disabled
              name="discountAmount"
              label="Discount Amount"
              onBlur={(e: any) => {
                discountIdHelper.setValue("");
                getDiscountedInstallmemts(
                  e.target.value,
                  getInstallments(method.value, courseDuration, 'USD', 1),
                  installmentsHelper.setValue,
                  courseFee.value,
                  "byValue"
                );
              }}
            />

            <$Select
              disabled
              name="approvalId"
              size="medium"
              formitem={{
                label: "Discount approval",
              }}
              options={user.users.data}
              optionValue="id"
              optionText="fullName"
              allOption={false}
            />
          </Form>
        </Col>
        <Col span={12}>
          {installmentsMeta.error && (
            <Alert
              message="Installments sum doesn't match with course fee"
              type="warning"
              showIcon
              closable
            />
          )}
          <div className="d-flex align-items-stretch">
            <div style={{ width: 500 }} className="reg-ins-content pl-4 pb-5">
              <h2 className="mb-0">
                Installment
              </h2>

              <div className="reg-installment">
                <div className="reg-installment-row reg-installment-header">
                  <div className="reg-installment-col"> </div>
                  <div className="reg-installment-col ">Due Date</div>
                  <div className="reg-installment-col text-right">Local</div>
                  <div className="reg-installment-col text-right">UNI</div>
                </div>
                <FieldArray
                  name="installments"
                  render={({ insert, remove, replace }) => (
                    <>
                      {installments.value &&
                        installments.value
                          // .filter(
                          //   ({ hide }: { hide: boolean }) => hide !== true
                          // )
                          .map((installment: any, index: number) => (
                            <div
                              className="reg-installment-row"
                              style={{ width: "500px" }}
                            >
                              <div className="reg-installment-col text-primary">
                                <strong>{index + 1}</strong>
                              </div>
                              <div
                                className="reg-installment-col"
                                style={{ width: "150px" }}
                              >
                                <$DatePicker
                                  name={`installments[${index}].due_date`}
                                  value={installment.due_date}
                                  disabled
                                />
                              </div>
                              <div className="reg-installment-col text-right">
                                <$Input
                                  name={`installments[${index}].local_amount`}
                                  required
                                  disabled
                                />
                              </div>
                              <div className="reg-installment-col text-right">
                                <$Input
                                  name={`installments[${index}].uni_amount`}
                                  disabled
                                />
                              </div>
                              <div className="reg-installment-col text-right">
                                <MinusCircleOutlined
                                  onClick={() => remove(index)}
                                />
                              </div>
                            </div>
                          ))}
                    </>
                  )}
                />
              </div>
            </div>
          </div>
        </Col>
        <Col span={6}>
          <div className="d-flex justify-content-bitween align-items-start">
            <div className="d-flex flex-column justify-content-end text-right">
              <h1 className="text-mute mb-0 price-lb">
                <$AmountLabel value={courseFee.value} />
              </h1>
              <div className="text-muted">Course Fee</div>
            </div>
          </div>
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration } = state;

  const { student, discounts } = registration;

  return {
    user,
    student,
    discounts,
  };
};

const mapDispatchToProps = {
  getDiscounts: Actions.registration.discounts.get,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Installment);
