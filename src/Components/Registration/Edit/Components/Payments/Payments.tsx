import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType, useField } from "formik";
import {
  BankOutlined,
  DollarCircleOutlined,
  InboxOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Upload,
  Divider,
  Tabs,
  Drawer,
} from "antd";
import { $AmountLabel, $Input, $Radio } from "Components";
import { Receipt } from "./Receipt";
import moment from "moment";
import "./receipt.css";
import { connect } from "react-redux";
import * as Actions from "Actions";

const Payments: React.FC<any> = ({ student, printDrawer, drawerClose }) => {
  const { Option } = Select;
  const { Dragger } = Upload;

  const [courseFee, , courseFeeHelper] = useField("courseFee");
  const [courseRegistrationFee, , courseRegistrationFeeHelper] = useField(
    "courseRegistrationFee"
  );
  const [installments, , installmentsHelper] = useField("installments");
  const [uniRef, , uniRefHelper] = useField("uniRef");
  const [localRef, , localRefHelper] = useField("localRef");

  const [uniAmount, , uniAmountHelper] = useField("uniAmount");
  const [localAmount, , localAmountHelper] = useField("localAmount");

  const [discountAmount, , discountAmountFeeHelper] =
    useField("discountAmount");

  return (
    <>
      <Form layout="vertical">
        <h2 className="mb-3">Payments</h2>
        <div className="d-flex justify-content-bitween align-items-start">
          <div className="d-flex flex-column justify-content-end text-right">
            <h1 className="text-mute mb-0 price-lb">
              <$AmountLabel value={courseFee.value - discountAmount.value} />
            </h1>
            <div className="text-muted">Course Fee</div>
          </div>
        </div>
        <Divider></Divider>

        <Tabs tabPosition={"left"}>
          <Tabs.TabPane
            key="1"
            tab={
              <span>
                <BankOutlined />
                Local
              </span>
            }
          >
            <Row gutter={24}>
              <Col span={10}>
                <div>
                  <div className="mb-4">
                    <$Radio
                      options={[
                        { value: "cash", label: "Cash" },
                        { value: "card", label: "Card" },
                        { value: "bank", label: "Bank Transfer" },
                        { value: "cheque", label: "Cheque" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      optionStyle="flex-fill text-center px-4"
                      buttonStyle="solid"
                      radioButton={true}
                      defaultValue={"cash"}
                      name="localPaymentMethod"
                    />
                  </div>

                  <Form.Item
                    label="Local Amount"
                    required
                    style={{ width: 180 }}
                  >
                    <$Input name="localAmount" />
                  </Form.Item>
                </div>
              </Col>
              <Col span={14}>
                <div className="mb-1">Reference</div>
                <Upload
                  maxCount={1}
                  action={`${process.env.REACT_APP_API_URL}/file_upload`}
                  headers={{
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  }}
                  defaultFileList={localRef.value.url && [localRef.value as any]}
                  onChange={({ fileList }) => {
                    fileList &&
                      Array.isArray(fileList) &&
                      fileList.filter(({ status }) => status === "done")
                        .length > 0 &&
                      fileList.map(
                        ({ name, status, response: { file_path } }: any) => {
                          if (status === "done") {
                            localRefHelper.setValue({
                              name,
                              url: file_path,
                            });
                          }
                        }
                      );
                  }}
                >
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from
                    uploading company data or other band files
                  </p>
                </Upload>
              </Col>
            </Row>
          </Tabs.TabPane>

          <Tabs.TabPane
            key="2"
            tab={
              <span>
                <DollarCircleOutlined />
                University
              </span>
            }
          >
            <Row gutter={24}>
              <Col span={10}>
                <div>
                  <div className="mb-4">
                    <$Radio
                      options={[
                        { value: "cash", label: "Cash" },
                        { value: "card", label: "Card" },
                        { value: "bank", label: "Bank Transfer" },
                        { value: "cheque", label: "Cheque" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      optionStyle="flex-fill text-center px-4"
                      buttonStyle="solid"
                      radioButton={true}
                      defaultValue={"bank"}
                      name="uniPaymentMethod"
                    />
                  </div>

                  <Form.Item label="University Amount" style={{ width: 180 }}>
                    <$Input name="uniAmount" />
                  </Form.Item>
                </div>
              </Col>
              <Col span={14}>
                <div className="mb-1">Reference</div>
                <Upload
                  maxCount={1}
                  action={`${process.env.REACT_APP_API_URL}/file_upload`}
                  headers={{
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  }}
                  defaultFileList={uniRef.value.url && [uniRef.value as any]}
                  onChange={({ fileList }) => {
                    fileList &&
                      Array.isArray(fileList) &&
                      fileList.filter(({ status }) => status === "done")
                        .length > 0 &&
                      fileList.map(
                        ({ name, status, response: { file_path } }: any) => {
                          if (status === "done") {
                            uniRefHelper.setValue({
                              name,
                              url: file_path,
                            });
                          }
                        }
                      );
                  }}
                >
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from
                    uploading company data or other band files
                  </p>
                </Upload>
              </Col>
            </Row>
          </Tabs.TabPane>
        </Tabs>
      </Form>
      <Drawer
        title={`AIMS RECEIPT`}
        placement="right"
        width={1200}
        onClose={() => drawerClose()}
        visible={printDrawer.isVisible}
      >
        <Tabs>
          <Tabs.TabPane
            disabled={student.localAmount == 0}
            key="11"
            tab={
              <span>
                <BankOutlined />
                Local
              </span>
            }
          >
            <div id="aims-receipt-local" className="pdfobject-container">
              <Receipt
                printId={"aims-receipt-local"}
                receiptNo={student.localReceiptNo}
                paymentMethod={student.localPaymentMethod}
                amount={student.localAmount}
                date={student.localPaymentDate}
                registrationNo={student.registrationNo}
                studentNo={student.studentNo}
                studentName={student.studentName}
                title={"LOCAL FEE"}
                currency="Rs"
                currencyWord="Rupees"
              />
            </div>
          </Tabs.TabPane>

          <Tabs.TabPane
            disabled={student.uniAmount == 0}
            key="22"
            tab={
              <span>
                <DollarCircleOutlined />
                University
              </span>
            }
          >
            <div id="aims-receipt-uni" className="pdfobject-container">
              <Receipt
                printId={"aims-receipt-uni"}
                receiptNo={student.uniReceiptNo}
                paymentMethod={student.unilPaymentMethod}
                amount={student.uniAmount}
                date={student.uniPaymentDate}
                registrationNo={student.registrationNo}
                studentNo={student.studentNo}
                studentName={student.studentName}
                title={"UNI FEE"}
                currency="$"
                currencyWord="Dolar"
              />
            </div>
          </Tabs.TabPane>
        </Tabs>
      </Drawer>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration, branch } = state;

  const { student, printDrawer } = registration;

  return {
    inquiry,
    course,
    user,
    student,
    branch,
    printDrawer,
  };
};

const mapDispatchToProps = {
  drawerClose: Actions.registration.drawer.close,
};

export default connect(mapStateToProps, mapDispatchToProps)(Payments);
