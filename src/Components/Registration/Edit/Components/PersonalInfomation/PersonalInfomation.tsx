import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType, useField } from "formik";
import { DownOutlined, PhoneOutlined } from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Spin,
  Button,
  Tag,
  Divider,
  message,
} from "antd";
import { connect } from "react-redux";
import * as Actions from "Actions";
import {
  $Select,
  $Input,
  $DatePicker,
  $DateLabel,
  $Radio,
} from "Components/antd";
import Countries from "./Countries";

const PersonalInfomation: React.FC<any> = ({ branch }) => {
  const { Option } = Select;

  const [fName] = useField("fName");
  const [lName] = useField("lName");
  const [middleName] = useField("middleName");
  const [birthdate] = useField("birthdate");
  const [country] = useField("country");
  const [nameInCertificate] = useField("nameInCertificate");
  const [gender, , genderHelper] = useField("gender");
  const [civilStatus, , civilStatusHelper] = useField("civilStatus");
  const [courseCode, , courseCodeHelper] = useField("courseCode");
  const [studentCode, , studentCodeHelper] = useField("studentCode");
  const [courseMedium, , courseMediumHelper] = useField("courseMedium");
  const [branchCode, , branchCodeHelper] = useField("branchCode");
  const [batchCode, , batchCodeHelper] = useField("batchCode");

  return (
    <>
      <h2>Personal Infomation</h2>
      <Form layout="vertical">
        <Row gutter={24}>
          <Col span={6}>
            <$Input
              label="First Name"
              name="fName"
              required
              value={fName.value}
            />
          </Col>
          <Col span={6}>
            <$Input label="Middle Name" name="middleName" />
          </Col>
          <Col span={6}>
            <$Input
              label="Last Name"
              name="lName"
              required
              value={lName.value}
            />
          </Col>
          <Col span={6}>
            <Form.Item label="Birth Date" required>
              <$DatePicker
                className="w-100"
                name="birthdate"
                value={birthdate.value}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>
            <div className="cert-box">
              <$Input
                name="nameInCertificate"
                label="Name in Certificate"
                value={nameInCertificate.value}
                required
              />
            </div>
          </Col>
          <Col span={6}>
            <$Input label="NIC/Passport" name="nic" required />
          </Col>
          <Col span={6}>
            <$Select
              name="countryOfCitizenship"
              size="medium"
              formitem={{
                label: "Country of Citizenship",
              }}
              options={Countries}
              optionValue="code"
              optionText="name"
              allOption={false}
            />
          </Col>
        </Row>
        <Row gutter={24} className="mt-3 mb-4">
          <Col span={6}>
            <Form.Item label="Gender" required>
              <$Radio
                options={[
                  { value: "Male", label: "Male" },
                  { value: "Female", label: "Female" },
                  { value: "Other", label: "Other" },
                ]}
                optionType="button"
                buttonStyle="solid"
                optionText="label"
                optionValue="value"
                optionStyle="flex-fill text-center px-4"
                radioButton={true}
                name="gender"
                onChange={(e: any) => {
                  genderHelper.setValue(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Civil Status" required>
              <$Radio
                options={[
                  { value: "Single", label: "Single" },
                  { value: "Married", label: "Married" },
                  { value: "Widowed", label: "Widowed" },
                  { value: "Separated", label: "Separated" },
                ]}
                optionType="button"
                optionText="label"
                optionValue="value"
                optionStyle="flex-fill text-center px-4"
                buttonStyle="solid"
                radioButton={true}
                name="civilStatus"
                onChange={(e: any) => {
                  civilStatusHelper.setValue(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <h2>Extra Infomation</h2>
        <Row gutter={24} className="mt-3 mb-4">
          <Col span={6}>
            <div className="cert-box">
              <$Select
                name="branch"
                size="medium"
                formitem={{
                  label: "Branch",
                }}
                options={branch?.branches.data}
                optionValue="id"
                optionText="branch_name"
                allOption={false}
                required
                disabled
                onChange={(val: string) => {
                  try {
                    const _branchCode = branch?.branches.data.filter(
                      ({ id }: any) => id == val
                    )[0].code;
                    _branchCode && branchCodeHelper.setValue(_branchCode);
                  } catch (error) {
                    message.error("Something wrong");
                  }
                }}
              />
            </div>
          </Col>
          <Col span={6}>
            <Form.Item label="Medium" required>
              <Radio.Group
                disabled
                options={["Sinhala", "English", "Tamil"]}
                optionType="button"
                buttonStyle="solid"
                defaultValue="English"
                onChange={(e) => {
                  switch (e.target.value) {
                    case "English":
                      courseMediumHelper.setValue("E");
                      break;
                    case "Sinhala":
                      courseMediumHelper.setValue("S");
                      break;
                    case "Tamil":
                      courseMediumHelper.setValue("T");
                      break;
                    default:
                      courseMediumHelper.setValue("E");
                      break;
                  }
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="Student Code" required>
              <Tag style={{ fontSize: "30px", padding: "10px" }}>
                {studentCode.value}
              </Tag>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="Batch Code" required>
              <$Input name="batchCode" />
            </Form.Item>
          </Col>
        </Row>

        <Divider />
        <h3> Address</h3>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item label="Street">
              <$Input name="street" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="City">
              <$Input name="city" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <$Select
              name="country"
              size="medium"
              formitem={{
                label: "Country",
              }}
              options={Countries}
              optionValue="code"
              optionText="name"
              allOption={false}
              required
            />
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <$Input name="primaryContact" label="Primary Contact" />
          </Col>
          <Col span={8}>
            <$Input name="secondaryContact" label="Secondary Contact" />
          </Col>
          <Col span={8}>
            <$Input name="email" label="Primary Email" />
          </Col>
        </Row>
      </Form>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration, branch } = state;

  const { student } = registration;

  return {
    inquiry,
    course,
    user,
    student,
    branch,
  };
};

const mapDispatchToProps = {
  getFaculties: Actions.course.faculty.get,
  getCourses: Actions.course.self.get,
  getInqueryById: Actions.inquery.byId.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfomation);
