import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import { Affix, Steps, Button, Card, Divider, notification, Dropdown, Form, Input, Menu, PageHeader, Select, Table, Tag } from 'antd';
import { DownOutlined, PhoneOutlined } from "@ant-design/icons";

const Programs: React.FC<any> = (props) => {

 
  return (
    <>
      <Formik
        initialValues={{
          fName: "",
        }}
        onSubmit={(values) => { }}
      >
        <>
        <h2>Postgraduate Program</h2>
      <div className="d-flex flex-wrap" >

        <Card
          hoverable
          style={{ width: 240 }}
          className="mr-2 mb-2 card-height"
        > PhD/DBAProgram
        </Card>
        <Card
          hoverable
          style={{ width: 240 }}
          className="mr-2 mb-2 card-height"
        > MBA Program
        </Card>
        <Card
          hoverable
          style={{ width: 240 }}
          className="mr-2 mb-2 card-height"
        >MEd Program
        </Card>
      </div>
        </>
      </Formik>
    </>
  );
};


export default Programs