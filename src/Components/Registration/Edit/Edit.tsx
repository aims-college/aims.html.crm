import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType, useField } from "formik";
import {
  Affix,
  Steps,
  Button,
  Card,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Select,
  Table,
  Tag,
  Skeleton,
} from "antd";
import { DownOutlined, PhoneOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import * as Actions from "Actions";
import "./New.scss";
import Validations from "./Validations";
import { Programs } from "./Components/Programs";
import { PersonalInfomation } from "./Components/PersonalInfomation";
import { Documents } from "./Components/Documents";
import { Educational } from "./Components/Educational";
import { WorkExperience } from "./Components/WorkExperience";
import { Installment } from "./Components/Installment";
import { Payments } from "./Components/Payments";
import { useParams } from "react-router";
import moment from "moment";
import { useHistory } from "react-router-dom";

const { Step } = Steps;
const steps = [
  // {
  //   title: "Program",
  //   content: (
  //     <>
  //       <Programs></Programs>
  //     </>
  //   ),
  // },
  {
    title: "Personal Information",
    content: (
      <>
        <PersonalInfomation></PersonalInfomation>
      </>
    ),
  },
  {
    title: "Educational Attainment",
    content: (
      <>
        <Educational></Educational>
      </>
    ),
  },
  {
    title: "Work Experience",
    content: (
      <>
        <WorkExperience></WorkExperience>
      </>
    ),
  },
  {
    title: "Documents",
    content: (
      <>
        <Documents></Documents>
      </>
    ),
  },
  {
    title: "Installment",
    content: (
      <>
        <Installment></Installment>
      </>
    ),
  },
  // {
  //   title: "Payments",
  //   content: (
  //     <>
  //       <Payments></Payments>
  //     </>
  //   ),
  // },
];

// branch selection - done
//medium selection - done
//Nic need to add  - done
//Middle name should in the middle - done
//student no need to generate from UI - done
// Other option should be added to gender - done
// email need to display - done
//contact_no/2 add - done
//street need to add for address  - done
//upload option to educational, work
// checklist need to get. label, true/false
// course_duration need to send throuh get_inqy_by_id
// course fee, registration fee get_inqy_by_id
// amount and due date can be change

//tab need to add Monthly, quatly

//payment_method //cash/card
//payment_type = course_fee/university fee
//registration_no
//line_items

function studentRepo(params: any) {
  const student_payments = [];

  console.log(params.localRef);
  if (params.localAmount > 0) {
    student_payments.push({
      payment_type: 1,
      payment_method: params.localPaymentMethod,
      amount: params.localAmount,
      reference_url:
        params.localRef && params.localRef.url ? params.localRef.url : null,
    });
  }

  if (params.uniAmount > 0) {
    student_payments.push({
      payment_type: 2,
      payment_method: params.uniPaymentMethod,
      amount: params.uniAmount,
      reference_url:
        params.uniRef && params.uniRef.url ? params.uniRef.url : null,
    });
  }

  return {
    student_id: params.student_id,
    student_registration_id: params.student_registration_id,
    first_name: params.fName,
    middle_name: params.middleName,
    last_name: params.lName,
    nic: params.nic,
    email: params.email,
    dob: moment(params.birthdate).format('YYYY-MM-DD'),
    country_of_citizenship: params.countryOfCitizenship,
    name_in_certificate: params.nameInCertificate,
    gender: params.gender,
    civil_status: params.civilStatus,
    current_address: params.street,
    current_address_city: params.city,
    current_address_country: params.country,
    contact_no_1: params.primaryContact,
    contact_no_2: params.secondaryContact,
    inquiry_id: params.inquiry_id,
    course_id: params.course_id,
    branch_id: params.branch,
    discount_id: params.discountId,
    batch_code: params.batchCode,
    student_code: params.studentCode,
    registration_fee: 0,
    medium: params.courseMedium,
    course_fee: params.courseFee,
    discount_amount: params.discountAmount,
    discount_approval_status: 1,
    discount_approved_by: params.approvalId,
    register_date: moment(),
    student_education: params.educational.map(
      (educational: any, index: number) => ({
        ...educational,
        ref_url: educational.url,
      })
    ),
    student_prerequisite_course: params.coursesTaken.map(
      (coursesTaken: any, index: number) => ({
        ...coursesTaken,
        ref_url: coursesTaken.url,
      })
    ),
    student_work_experience: params.workExperiance.map(
      (workExperiance: any, index: number) => ({
        ...workExperiance,
        ref_url: workExperiance.url,
      })
    ),
    student_document_checklist: params.documents,
    student_course_installment: params.installments.map(
      (installments: any, index: number) => ({
        ...installments,
        installment_no: index + 1,
      })
    ),
    student_payments,
  };
}

const RegistrationEdit: React.FC<any> = ({
  getRegistrationById,
  student,
  getBranches,
  getUsers,
  getDiscounts,
  getRegistrationNumber,
  updateRegister,
  branch,
}) => {
  const [current, setCurrent] = React.useState(0);
  const routeParams = useParams<{ id: string }>();
  const history = useHistory();

  useEffect(() => {
    if (routeParams && routeParams.hasOwnProperty("id")) {
      getRegistrationById(routeParams.id);
      getBranches();
      getUsers();
      getDiscounts();
    }
  }, [routeParams.id]);

  return (
    <>
      <Formik
        initialValues={{ ...student.data, inquiry_id: routeParams.id }}
        validateOnBlur={true}
        validateOnChange={true}
        validateOnMount={true}
        validationSchema={Validations}
        enableReinitialize
        onSubmit={(values) => {
          updateRegister(studentRepo(values));
        }}
      >
        {({
          values,
          handleSubmit,
          errors,
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          validateForm,
        }) => (
          <>
            <div className="mb-5">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <Skeleton loading={student.isLoading} paragraph={{ rows: 1 }}>
                    <PageHeader
                      className="px-0"
                      onBack={() => history.goBack()}
                      title={`Edit Registration : ${student.data.registrationNo}`}
                    />
                  </Skeleton>
                </div>
              </Affix>
              <div>
                <div className="px-5 pt-4">
                  <Steps current={current} direction="horizontal" size="small">
                    {steps.map((item) => (
                      <Step key={item.title} title={item.title} />
                    ))}
                  </Steps>

                  <div className="d-flex align-items-center mt-2">
                    <h3 className="font-weight-bold mb-0">
                      {values.fName} {values.lName}
                    </h3>
                    <Divider type="vertical" className="mx-4"></Divider>
                    <h3 className="font-weight-bold mb-0">
                      {student.data.course_name}
                    </h3>
                  </div>
                  <Divider></Divider>
                </div>
                <div className="flex-fill px-5">
                  <div className="steps-content">{steps[current]?.content}</div>
                </div>
              </div>
            </div>
            <div className="form-bottom d-flex justify-content-end">
              {current !== 0 && (
                <Button
                  type="primary"
                  className="mr-2"
                  onClick={() => setCurrent(current - 1)}
                >
                  Previous
                </Button>
              )}
              {current !== 4 && (
                <Button
                  type="primary"
                  className="mr-2"
                  // disabled={isSubmitting || isValidating || !dirty || !isValid}
                  onClick={() => {
                    setCurrent(current + 1);
                  }}
                >
                  Next
                </Button>
              )}
              {current == 4 && (
                <Button
                  type="default"
                  onClick={(e: any) => handleSubmit(e)}
                  // disabled={isSubmitting || isValidating || !dirty || !isValid}
                >
                  Save
                </Button>
              )}

              <Button type="default" onClick={() => history.goBack()}>
                Cancel
              </Button>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration, branch } = state;

  const { student } = registration;

  return {
    inquiry,
    course,
    user,
    student,
    branch,
  };
};

const mapDispatchToProps = {
  getCourses: Actions.course.self.get,
  getRegistrationById: Actions.registration.byId.get,
  getBranches: Actions.branch.self.get,
  updateRegister: Actions.registration.update.post,
  getDiscounts: Actions.registration.discounts.get,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationEdit);
