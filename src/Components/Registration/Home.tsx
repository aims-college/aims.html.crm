import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
} from "antd";
import { $Select, $Input, $DatePicker, $RangePicker } from "Components/antd";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";
import queryString from "query-string";

const { RangePicker } = DatePicker;
const Home = ({
  getRegistrations,
  inquiry,
  getInqueryTypes,
  course,
  registration,
  getCourses,
  getUsers,
  user,
}: any) => {
  const history = useHistory();
  const params = queryString.parse(history.location.search);

  const [filter, setFilter] = useState({
    nic: "",
    status: "pending",
    name: "",
    contact_no: "",
    reg_no: "",
    course_id: params.course_id ? Number(params.course_id) : "",
    inquiry_type_id: params.inquiry_type_id
      ? Number(params.inquiry_type_id)
      : "",
    user_id: params.user_id ? Number(params.user_id) : null,
    start_date: params.start_date
      ? moment(params.start_date).format("YYYY-MM-DD")
      : moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: params.end_date
      ? moment(params.end_date).format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD"),
  });
  const [excel, setExcel] = useState<any>([{ data: [], columns: [] }]);

  useEffect(() => {
    getRegistrations(filter);
    getInqueryTypes();
    getCourses();
    getUsers();
  }, []);

  useEffect(() => {
    let columns: any = [
      "id",
      "inquiry_id",
      "student_code",
      "reg_no",
      "course_name",
      "register_date",
      "medium",
      "registration_fee",
      "book_fee",
      "course_fee",
      "discount_amount",
      "batch_code",
      "first_name",
      "middle_name",
      "last_name",
      "nic",
      "email",
      "dob",
      "country_of_citizenship",
      "name_in_certificate",
      "gender",
      "civil_status",
      "current_address",
      "current_address_city",
      "current_address_country",
      "permanent_address",
      "permanent_address_city",
      "permanent_address_country",
      "contact_no_1",
      "contact_no_2",
    ];
    const dataSource: any = [];

    registration.all.data.map((data: any, index: number) => {
      dataSource.push([
        { value: data["id"].toString() },
        { value: data["student_registration"]["inquiry_id"]?.toString() },
        { value: data["student_registration"]["student_code"]?.toString() },
        { value: data["student_registration"]["reg_no"]?.toString() },
        { value: data["student_registration"]["course_name"]?.toString() },
        { value: data["student_registration"]["register_date"]?.toString() },
        { value: data["student_registration"]["medium"]?.toString() },
        { value: data["student_registration"]["registration_fee"] },
        { value: data["student_registration"]["book_fee"] },
        { value: data["student_registration"]["course_fee"] },
        { value: data["student_registration"]["discount_amount"] },
        { value: data["student_registration"]["batch_code"] },
        { value: data["first_name"]?.toString() },
        { value: data["middle_name"]?.toString() },
        { value: data["last_name"]?.toString() },
        { value: data["nic"]?.toString() },
        { value: data["email"]?.toString() },
        { value: data["dob"]?.toString() },
        { value: data["country_of_citizenship"]?.toString() },
        { value: data["name_in_certificate"]?.toString() },
        { value: data["gender"]?.toString() },
        { value: data["civil_status"]?.toString() },
        { value: data["current_address"]?.toString() },
        { value: data["current_address_city"]?.toString() },
        { value: data["current_address_country"]?.toString() },
        { value: data["permanent_address"]?.toString() },
        { value: data["permanent_address_city"]?.toString() },
        { value: data["permanent_address_country"]?.toString() },
        { value: data["contact_no_1"]?.toString() },
        { value: data["contact_no_2"]?.toString() },
      ]);
    });

    setExcel([
      {
        data: dataSource,
        columns: columns.map((column: any) =>
          column.replace(/_/g, " ").toUpperCase()
        ),
      },
    ]);
  }, [registration.all.data]);

  const statusMenu = (
    <Menu
      onClick={(e) => {
        setFilter({ ...filter, status: e.key.toString() });
        getRegistrations({ ...filter, status: e.key.toString() });
      }}
    >
      <Menu.Item key="pending">
        <Tag className="tag-status-pending">Pending</Tag>
      </Menu.Item>
      <Menu.Item key="registered">
        <Tag className="tag-status-registered">Registered</Tag>
      </Menu.Item>
      <Menu.Item key="non_registered">
        <Tag className="tag-status-non_registered">Non Registered</Tag>
      </Menu.Item>
      <Menu.Item key="not_answered">
        <Tag className="tag-status-not_answered">Not Answered</Tag>
      </Menu.Item>
    </Menu>
  );

  const columns = [
    {
      title: "Registration No",
      dataIndex: "id",
      key: "id",
      render: (text: string, record: any) => {
        return <Tag color="red">{record.student_registration.reg_no}</Tag>;
      },
    },
    {
      title: "Registration Date",
      dataIndex: "registrationDate",
      key: "id",
      render: (text: string, record: any) => {
        return <>{record.student_registration.register_date}</>;
      },
    },
    {
      title: "Student Code",
      dataIndex: "student_code",
      render: (text: string, record: any) => {
        return (
          <Tag color="blue">{record.student_registration.student_code}</Tag>
        );
      },
    },
    {
      title: "Name",
      dataIndex: "firstName",
      render: (text: string, record: any) => {
        return (
          <>
            {" "}
            {record.first_name} {record.middle_name} {record.last_name}{" "}
          </>
        );
      },
    },
    {
      title: "NIC/Passport",
      dataIndex: "nic",
    },
    {
      title: "Name In Certificate",
      dataIndex: "name_in_certificate",
    },
    {
      title: "DOB",
      dataIndex: "dob",
    },
    {
      title: "Gender",
      dataIndex: "gender",
    },
    {
      title: "Course",
      dataIndex: "course_name",
      render: (text: string, record: any) => {
        return <> {record.student_registration.course_name} </>;
      },
    },
  ];
  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          dateRange:
            params.start_date && params.end_date
              ? [
                  moment(params.start_date),
                  moment(params.end_date),
                ]
              : [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        <>
          <div className="">
            <Affix offsetTop={0}>
              <div className="page-header header-border">
                <PageHeader
                  className="px-0"
                  onBack={() => history.goBack()}
                  title="Registration"
                />
              </div>
            </Affix>

            <div className="regh-layout">
              <Affix offsetTop={0}>
                <aside className="regh-side">
                  <h3>Filter</h3>
                  <Form layout="vertical">
                    <$RangePicker
                      size="small"
                      className="mb-3"
                      name="dateRange"
                      onChange={(dateRange: any) => {
                        if (dateRange) {
                          setFilter({
                            ...filter,
                            start_date: dateRange[0].format("YYYY-MM-DD"),
                            end_date: dateRange[1].format("YYYY-MM-DD"),
                          });
                          getRegistrations({
                            ...filter,
                            start_date: dateRange[0].format("YYYY-MM-DD"),
                            end_date: dateRange[1].format("YYYY-MM-DD"),
                          });
                        }
                      }}
                    />
                    <$Input
                      size="small"
                      placeholder="Name"
                      name="name"
                      value={filter.name}
                      onBlur={(e: any) => {
                        setFilter({ ...filter, name: e.target.value });
                        getRegistrations({ ...filter, name: e.target.value });
                      }}
                    />
                    <$Input
                      size="small"
                      placeholder="NIC"
                      name="nic"
                      value={filter.nic}
                      onBlur={(e: any) => {
                        setFilter({ ...filter, nic: e.target.value });
                        getRegistrations({ ...filter, nic: e.target.value });
                      }}
                    />
                    <$Input
                      size="small"
                      placeholder="Contact Number"
                      value={filter.contact_no}
                      name="contact_no"
                      onBlur={(e: any) => {
                        setFilter({ ...filter, contact_no: e.target.value });
                        getRegistrations({
                          ...filter,
                          contact_no: e.target.value,
                        });
                      }}
                    />
                    <$Input
                      size="small"
                      placeholder="Registration Number"
                      name="registration_no"
                      onBlur={(e: any) => {
                        setFilter({ ...filter, reg_no: e.target.value });
                        getRegistrations({
                          ...filter,
                          reg_no: e.target.value,
                        });
                      }}
                    />

                    <$Select
                      name="course_id"
                      size="small"
                      formitem={{
                        label: "Course",
                      }}
                      options={course.courses.data.filter(
                        ({ status }: { status: string }) => status === "1"
                      )}
                      optionValue="id"
                      optionText="name"
                      defaultValue="all"
                      value={filter.course_id}
                      allOption={true}
                      onChange={(course_id: any) => {
                        if (course_id == "all") {
                          getRegistrations({ ...filter, course_id: "" });
                          setFilter({ ...filter, course_id: "" });
                        } else {
                          getRegistrations({ ...filter, course_id });
                          setFilter({ ...filter, course_id });
                        }
                      }}
                    />

                    <$Select
                      name="user_id"
                      size="small"
                      formitem={{
                        label: "",
                      }}
                      options={user.users.data}
                      placeholder="Select User"
                      optionValue="id"
                      optionText="fullName"
                      allOption={true}
                      onChange={(user_id: any) => {
                        if (user_id == "all") {
                          getRegistrations({ ...filter, user_id: null });
                          setFilter({ ...filter, user_id: null });
                        } else {
                          getRegistrations({ ...filter, user_id });
                          setFilter({ ...filter, user_id });
                        }
                      }}
                    />
                  </Form>
                </aside>
              </Affix>
              <div className="flex-fill ">
                <div>
                  <ExcelFile
                    element={
                      <Button type="dashed" style={{ float: "right" }}>
                        Export Data
                      </Button>
                    }
                  >
                    <ExcelSheet
                      dataSet={excel}
                      name="Registrations"
                    ></ExcelSheet>
                  </ExcelFile>
                  <Table
                    columns={columns}
                    size="small"
                    dataSource={registration.all.data}
                    loading={registration.all.isLoading}
                    rowKey={(record) => record.id}
                    onRow={(record, rowIndex) => {
                      return {
                        onDoubleClick: (event) => {
                          history.push(
                            `/registrations/${record.student_registration.id}/edit`,
                            record
                          );
                        },
                      };
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </>
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, registration, user } = state;

  return {
    inquiry,
    course,
    registration,
    user,
  };
};

const mapDispatchToProps = {
  getRegistrations: Actions.registration.all.get,
  getInqueryTypes: Actions.inquery.type.get,
  getCourses: Actions.course.self.get,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
