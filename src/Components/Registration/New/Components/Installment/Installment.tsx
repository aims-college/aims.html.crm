import React, { useState, useRef, useEffect } from "react";
import {
  Formik,
  FormikContextType,
  useField,
  FieldArray,
  ErrorMessage,
} from "formik";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Spin,
  Divider,
  message,
  Alert,
  Tag,
  Statistic,
} from "antd";
import { connect } from "react-redux";
import * as Actions from "Actions";
import {
  $Select,
  $Input,
  $DatePicker,
  $DateLabel,
  $AmountLabel,
} from "Components/antd";
import { getInstallments, getDiscountedInstallmemts } from "Functions";
import "./Installment.scss";
import moment from "moment";
import { registration } from "Services";
import { values } from "lodash";

const Installment: React.FC<any> = ({
  getUsers,
  getDiscounts,
  discounts,
  payment,
  user,
}) => {
  const { Option } = Select;
  const { RangePicker } = DatePicker;

  const [applied, setApplied] = useState<boolean>(false);
  const [discountApproval, discountApprovalMeta, discountApprovalHelper] =
  useField("discountApproval");

  const [installments, installmentsMeta, installmentsHelper] =
    useField("installments");
  const [courseDuration, , courseDurationHelper] = useField("courseDuration");
  const [courseFee, , courseFeeHelper] = useField("courseFee");
  const [method, , methodFeeHelper] = useField("method");
  const [discountId, , discountIdHelper] = useField("discountId");
  const [discountAmount, , discountAmountFeeHelper] =
    useField("discountAmount");
  const [inquiry_id, ,] = useField("inquiry_id");
  const [course_id, ,] = useField("course_id");
  const [branch, ,] = useField("branch");
  const [currency, , currencyHelper] = useField("currency");

  const applyForApproval = () => {
    if (typeof branch.value === "undefined") {
      message.warning("Please select branch");
      return;
    }

    if (method.value == 0) {
      message.warning("Please add no of installments");
      return;
    }

    if (discountAmount.value == 0) {
      message.warning("Please add discounts");
      return;
    }

    registration.applyForApproval
      .post({
        inquiry_id: inquiry_id.value,
        course_id: course_id.value,
        branch_id: branch.value,
        discount_id: discountId.value,
        course_fee: courseFee.value,
        course_duration: courseDuration.value,
        no_of_installments: method.value,
        discount_amount: discountAmount.value,
      })
      .then(() => {
        setApplied(true);
        message.success("Successfully applied for approval");
      })
      .catch(() => {
        message.warn("Something Wrong");
      });
  };

  const checkApproval = (id: number) => {
    registration.checkDiscountApproval.get(id).then(({ data }) => {
      if (data[data.length - 1]?.discount_approval_status == 1) {
        message.success("Successfully Applied the discount");
        discountApprovalHelper.setValue(true);
        getDiscountedInstallmemts(
          discountAmount.value,
          getInstallments(method.value, courseDuration, currency.value, 1),
          installmentsHelper.setValue,
          courseFee.value,
          "byValue"
        );
      } else if (data[data.length - 1]?.discount_approval_status == 0) {
        message.loading("Approval is Pending");
        discountApprovalHelper.setValue(false);
      } else if (data[data.length - 1]?.discount_approval_status == 2) {
        discountApprovalHelper.setValue(false);
        message.warning(
          `Approval has been rejected by ${
            data[data.length - 1]?.discount_approved_user_name
          }`
        );
      }
    });
  };

  return (
    <>
      <Row gutter={24}>
        <Col span={4}>
          <h2 className="mb-3">Discount</h2>
          <Form layout="vertical">
            <$Input
              name="method"
              label="No of Installments"
              onBlur={(e: any) => {
                installmentsHelper.setValue(
                  getInstallments(
                    e.target.value,
                    courseDuration,
                    currency.value,
                    1
                  )
                );
                message.loading("Generating Installments ...");
              }}
            />

            <$Select
              name="discountId"
              size="medium"
              formitem={{
                label: "Discount by category",
              }}
              options={discounts.data}
              optionValue="id"
              optionText="name"
              allOption={false}
              onChange={(discountId: any) => {
                const discount = discounts.data.filter(
                  ({ id }: any) => id === discountId
                )[0].discount;

                discountAmountFeeHelper.setValue(
                  (courseFee.value * discount) / 100
                );
              }}
            />

            <$Input
              name="discountAmount"
              label="Discount Amount"
              onBlur={(e: any) => {
                discountIdHelper.setValue("");
              }}
            />

            {!applied && (
              <Button onClick={() => applyForApproval()}>
                Apply For Discount Approval
              </Button>
            )}

            {applied && (
              <>
                <Button onClick={() => checkApproval(inquiry_id.value)}>
                  Check Discount Approval Status
                </Button>
              </>
            )}
          </Form>
        </Col>
        <Col span={4}>
          <h2 className="mb-3">Extra Fees</h2>
          <$Input name={`registrationFee`} label="Registration Fee" />
          <$Input name={`bookFee`} label="Book Fee" />{" "}
        </Col>
        <Col span={10}>
          {installmentsMeta.error && (
            <Alert
              message="Installments sum doesn't match with course fee"
              type="warning"
              showIcon
              closable
            />
          )}
          <div className="d-flex align-items-stretch">
            <div style={{ width: 500 }} className="reg-ins-content pl-4 pb-5">
              <h2 className="mb-3">
                Installment ( Course Duration {courseDuration.value} )
              </h2>

              <Form.Item label="Currency">
                <$Select
                  name="currency"
                  size="medium"
                  defaultValue="USD"
                  optionValue="code"
                  allOption={false}
                  optionText="currency_name"
                  options={payment.currencies.data}
                />
              </Form.Item>

              <div className="reg-installment">
                <div className="reg-installment-row reg-installment-header">
                  <div className="reg-installment-col"> </div>
                  <div className="reg-installment-col ">Due Date</div>
                  <div className="reg-installment-col text-right">
                    Course Fee
                  </div>
                  <div className="reg-installment-col text-right">
                    <span>Uni Fee</span>
                  </div>
                </div>

                <FieldArray
                  name="installments"
                  render={({ insert, remove, replace }) => (
                    <>
                      {installments.value &&
                        installments.value
                          // .filter(
                          //   ({ hide }: { hide: boolean }) => hide !== true
                          // )
                          .map((installment: any, index: number) => (
                            <div
                              className="reg-installment-row"
                              style={{ width: "600px" }}
                            >
                              <div className="reg-installment-col text-primary">
                                <strong>{index + 1}</strong>
                              </div>
                              <div
                                className="reg-installment-col"
                                style={{ width: "150px" }}
                              >
                                <$DatePicker
                                  name={`installments[${index}].due_date`}
                                  value={installment.due_date}
                                />
                              </div>
                              <div className="reg-installment-col text-right">
                                <$Input
                                  addonBefore={'LKR'}
                                  name={`installments[${index}].local_amount`}
                                  required
                                />
                              </div>
                              <div className="reg-installment-col text-right">
                                <$Input
                                  addonBefore={currency.value}
                                  name={`installments[${index}].uni_amount`}
                                />
                              </div>
                              <div className="reg-installment-col text-right">
                                <MinusCircleOutlined
                                  onClick={() => {
                                    remove(index);
                                  }}
                                />
                              </div>
                            </div>
                          ))}
                    </>
                  )}
                />
              </div>
            </div>
          </div>
        </Col>
        <Col span={6}>
          <div className="d-flex justify-content-bitween align-items-start">
            <div className="d-flex flex-column justify-content-end text-right">
              <Statistic
                title="Course Fee"
                prefix={'LKR'}
                value={courseFee.value - (discountApproval.value ? discountAmount.value : 0)}
                precision={2}
                valueStyle={{fontWeight: 600}}
              />
              <Divider/>
              <Statistic
                title="UNI Fee"
                prefix={currency.value}
                value={installments.value.reduce(
                  (p: number, c: any) => p + Number(c.uni_amount),
                  0
                )}
                valueStyle={{fontWeight: 600}}
                precision={2}
              />
            </div>
          </div>
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration, payment } = state;

  const { student, discounts } = registration;

  return {
    user,
    student,
    payment,
    discounts,
  };
};

const mapDispatchToProps = {
  getDiscounts: Actions.registration.discounts.get,
  getUsers: Actions.user.users.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Installment);
