import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType, useField } from "formik";
import {
  AccountBookFilled,
  BankOutlined,
  DollarCircleOutlined,
  InboxOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Upload,
  Divider,
  Tabs,
  Drawer,
  Statistic,
} from "antd";
import { $AmountLabel, $Input, $Radio, $Select, $TextArea } from "Components";
import { Receipt } from "./Receipt";
import moment from "moment";
import "./receipt.css";
import { connect } from "react-redux";
import * as Actions from "Actions";
import { useHistory } from "react-router";
const Payments: React.FC<any> = ({
  student,
  printDrawer,
  drawerClose,
  payment,
}) => {
  const { Option } = Select;
  const { Dragger } = Upload;
  const history = useHistory();

  const [tabIndex, setTabIndex] = useState(1);
  const [courseFee, , courseFeeHelper] = useField("courseFee");
  const [courseRegistrationFee, , courseRegistrationFeeHelper] = useField(
    "courseRegistrationFee"
  );
  const [installments, , installmentsHelper] = useField("installments");
  const [uniRef, , uniRefHelper] = useField("uniRef");
  const [localRef, , localRefHelper] = useField("localRef");
  const [otherRef, , otherRefHelper] = useField("otherRef");

  const [uniAmount, , uniAmountHelper] = useField("uniAmount");
  const [localAmount, , localAmountHelper] = useField("localAmount");
  const [otherAmount, , otherAmountHelper] = useField("otherAmount");

  const [currency, , currencyHelper] = useField("currency");
  const [uniPaidCurrency, , uniPaidCurrencyHelper] =
    useField("uniPaidCurrency");
  const [otherPaidCurrency, , otherPaidCurrencyHelper] =
    useField("otherPaidCurrency");

  const [discountAmount, , discountAmountFeeHelper] =
    useField("discountAmount");

  const [localPaymentMethod, ,] = useField("localPaymentMethod");
  const [uniPaymentMethod, ,] = useField("uniPaymentMethod");
  const [otherPaymentMethod, ,] = useField("otherPaymentMethod");

  const getCurrencyWord = (key: any): string => {
    try {
      return payment.currencies?.data?.filter(
        ({ code }: any) => code == student.data[key]
      )[0].currency_name;
    } catch (error) {
      return "";
    }
  };

  return (
    <>
      <Form layout="vertical">
        <h2 className="mb-3">Payments</h2>

        <Row gutter={16}>
          <Col span={4}>
            <Statistic
              title="Course Fee"
              prefix={"LKR"}
              value={courseFee.value - discountAmount.value}
              precision={2}
              valueStyle={{ fontWeight: 600 }}
            />
          </Col>
          <Col span={4}>
            <Statistic
              title="Uni Fee"
              prefix={currency.value}
              value={installments.value.reduce(
                (p: number, c: any) => p + Number(c.uni_amount),
                0
              )}
              precision={2}
              valueStyle={{ fontWeight: 600 }}
            />
          </Col>
        </Row>

        <Divider></Divider>

        <Tabs
          tabPosition={"left"}
          onTabClick={(key) => setTabIndex(Number(key))}
        >
          <Tabs.TabPane
            key="1"
            tab={
              <span>
                <BankOutlined />
                Local
              </span>
            }
          >
            <Row gutter={24}>
              <Col span={10}>
                <div>
                  <div className="mb-4">
                    <$Radio
                      options={[
                        { value: "cash", label: "Cash" },
                        { value: "card", label: "Card" },
                        { value: "bank", label: "Bank Transfer" },
                        { value: "cheque", label: "Cheque" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      optionStyle="flex-fill text-center px-4"
                      buttonStyle="solid"
                      radioButton={true}
                      defaultValue={"cash"}
                      name="localPaymentMethod"
                    />
                  </div>

                  <Form.Item
                    label="Local Amount"
                    required
                    style={{ width: 300 }}
                  >
                    <$Input
                      name="localAmount"
                      addonBefore={
                        <$Select
                          name="localPaidCurrency"
                          size="medium"
                          optionValue="code"
                          allOption={false}
                          disabled
                          optionText="currency_name"
                          options={payment.currencies.data}
                        />
                      }
                    />
                  </Form.Item>

                  {localPaymentMethod.value !== "cash" && (
                    <$Select
                      required
                      name="account_id_local"
                      size="medium"
                      formitem={{
                        label: "Debit Bank",
                      }}
                      options={payment.bankAccounts.data.filter(
                        ({ account_type }: { account_type: string }) =>
                          (localPaymentMethod.value === "bank" &&
                            account_type === "1") ||
                          (localPaymentMethod.value === "card" &&
                            account_type === "2")
                      )}
                      optionValue="id"
                      optionText="name"
                      allOption={false}
                    />
                  )}

                  <Form.Item label="Description">
                    <$TextArea name="localDescription" rows={6} cols={6} />
                  </Form.Item>
                </div>
              </Col>
              <Col span={14}>
                <div className="mb-1">Reference</div>
                <Upload
                  maxCount={1}
                  action={`${process.env.REACT_APP_API_URL}/file_upload`}
                  headers={{
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  }}
                  defaultFileList={
                    localRef.value.url && [localRef.value as any]
                  }
                  onChange={({ fileList }) => {
                    fileList &&
                      Array.isArray(fileList) &&
                      fileList.filter(({ status }) => status === "done")
                        .length > 0 &&
                      fileList.map(
                        ({ name, status, response: { file_path } }: any) => {
                          if (status === "done") {
                            localRefHelper.setValue({
                              name,
                              url: file_path,
                            });
                          }
                        }
                      );
                  }}
                >
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from
                    uploading company data or other band files
                  </p>
                </Upload>
              </Col>
            </Row>
          </Tabs.TabPane>

          <Tabs.TabPane
            key="2"
            tab={
              <span>
                <DollarCircleOutlined />
                University
              </span>
            }
          >
            <Row gutter={24}>
              <Col span={10}>
                <div>
                  <div className="mb-4">
                    <$Radio
                      options={[
                        { value: "cash", label: "Cash" },
                        { value: "card", label: "Card" },
                        { value: "bank", label: "Bank Transfer" },
                        { value: "cheque", label: "Cheque" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      optionStyle="flex-fill text-center px-4"
                      buttonStyle="solid"
                      radioButton={true}
                      defaultValue={"bank"}
                      name="uniPaymentMethod"
                    />
                  </div>

                  <Form.Item label="University Amount" style={{ width: 300 }}>
                    <$Input
                      name="uniAmount"
                      addonBefore={
                        <$Select
                          name="uniPaidCurrency"
                          size="medium"
                          defaultValue="USD"
                          optionValue="code"
                          allOption={false}
                          optionText="currency_name"
                          options={payment.currencies.data}
                        />
                      }
                    />
                  </Form.Item>

                  {uniPaidCurrency.value != currency.value && (
                    <Form.Item label="Conversion Rate" style={{ width: 100 }}>
                      <$Input name="conversionRate" placeholder="220" />
                    </Form.Item>
                  )}

                  {uniPaymentMethod.value !== "cash" && (
                    <$Select
                      required
                      name="account_id_uni"
                      size="medium"
                      formitem={{
                        label: "Debit Bank",
                      }}
                      options={payment.bankAccounts.data.filter(
                        ({ account_type }: { account_type: string }) =>
                          (uniPaymentMethod.value === "bank" &&
                            account_type === "1") ||
                          (uniPaymentMethod.value === "card" &&
                            account_type === "2")
                      )}
                      optionValue="id"
                      optionText="name"
                      allOption={false}
                    />
                  )}

                  <Form.Item label="Description">
                    <$TextArea name="uniDescription" rows={6} cols={6} />
                  </Form.Item>
                </div>
              </Col>
              <Col span={14}>
                <div className="mb-1">Reference</div>
                <Upload
                  maxCount={1}
                  action={`${process.env.REACT_APP_API_URL}/file_upload`}
                  headers={{
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  }}
                  defaultFileList={uniRef.value.url && [uniRef.value as any]}
                  onChange={({ fileList }) => {
                    fileList &&
                      Array.isArray(fileList) &&
                      fileList.filter(({ status }) => status === "done")
                        .length > 0 &&
                      fileList.map(
                        ({ name, status, response: { file_path } }: any) => {
                          if (status === "done") {
                            uniRefHelper.setValue({
                              name,
                              url: file_path,
                            });
                          }
                        }
                      );
                  }}
                >
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from
                    uploading company data or other band files
                  </p>
                </Upload>
              </Col>
            </Row>
          </Tabs.TabPane>

          <Tabs.TabPane
            key="3"
            tab={
              <span>
                <AccountBookFilled />
                Other
              </span>
            }
          >
            <Row gutter={24}>
              <Col span={24}>
                <div>
                  <div className="mb-4">
                    <$Radio
                      options={[
                        { value: "cash", label: "Cash" },
                        { value: "card", label: "Card" },
                        { value: "bank", label: "Bank Transfer" },
                        { value: "cheque", label: "Cheque" },
                      ]}
                      optionType="button"
                      optionText="label"
                      optionValue="value"
                      buttonStyle="solid"
                      radioButton={true}
                      defaultValue={"bank"}
                      name="otherPaymentMethod"
                    />
                  </div>

                  <Form.Item label="Other Amount" style={{ width: 300 }}>
                    <$Input
                      name="otherAmount"
                      addonBefore={
                        <$Select
                          disabled
                          name="otherPaidCurrency"
                          size="medium"
                          defaultValue="USD"
                          optionValue="code"
                          allOption={false}
                          optionText="currency_name"
                          options={payment.currencies.data}
                        />
                      }
                    />
                  </Form.Item>

                  {otherPaymentMethod.value !== "cash" && (
                    <$Select
                      required
                      name="account_id_other"
                      size="medium"
                      formitem={{
                        label: "Debit Bank",
                      }}
                      options={payment.bankAccounts.data.filter(
                        ({ account_type }: { account_type: string }) =>
                          (otherPaymentMethod.value === "bank" &&
                            account_type === "1") ||
                          (otherPaymentMethod.value === "card" &&
                            account_type === "2")
                      )}
                      optionValue="id"
                      optionText="name"
                      allOption={false}
                    />
                  )}

                  <$Select
                    required
                    name="otherDescription"
                    size="medium"
                    formitem={{
                      label: "Payment Reason",
                    }}
                    options={[
                      { id: "Registration Fee", value: "Registration Fee" },
                      { id: "Book / Tute Fee", value: "Book / Tute Fee" },
                      { id: "Exam Fee", value: "Exam Fee" },
                      { id: "Record Book", value: "Record Book" },
                    ]}
                    optionValue="id"
                    optionText="value"
                    allOption={false}
                  />
                  <Form.Item label="Reference">
                    <Upload
                      maxCount={1}
                      action={`${process.env.REACT_APP_API_URL}/file_upload`}
                      headers={{
                        Authorization: `Bearer ${localStorage.getItem(
                          "token"
                        )}`,
                      }}
                      defaultFileList={
                        otherRef.value.url && [otherRef.value as any]
                      }
                      onChange={({ fileList }) => {
                        fileList &&
                          Array.isArray(fileList) &&
                          fileList.filter(({ status }) => status === "done")
                            .length > 0 &&
                          fileList.map(
                            ({
                              name,
                              status,
                              response: { file_path },
                            }: any) => {
                              if (status === "done") {
                                uniRefHelper.setValue({
                                  name,
                                  url: file_path,
                                });
                              }
                            }
                          );
                      }}
                    >
                      <InboxOutlined style={{ fontSize: "64px !important" }} />
                      <p className="ant-upload-text">
                        Click or drag file to this area to upload
                      </p>
                      <p className="ant-upload-hint">
                        Support for a single upload
                      </p>
                    </Upload>
                  </Form.Item>
                </div>
              </Col>
            </Row>
          </Tabs.TabPane>
        </Tabs>
      </Form>
      <Drawer
        title={`AIMS RECEIPT`}
        placement="right"
        width={1200}
        onClose={() => {
          drawerClose();
          history.goBack();
        }}
        visible={printDrawer.isVisible}
      >
        <Tabs>
          {student.data.localAmount > 0 && (
            <Tabs.TabPane
              key="11"
              tab={
                <span>
                  <BankOutlined />
                  Local
                </span>
              }
            >
              <div id="aims-receipt-local" className="pdfobject-container">
                <Receipt
                  printId={"aims-receipt-local"}
                  receiptNo={student.data.localReceiptNo}
                  paymentMethod={student.data.localPaymentMethod}
                  amount={student.data.localAmount}
                  date={student.data.localPaymentDate}
                  registrationNo={student.data.registrationNo}
                  studentNo={student.data.studentNo}
                  studentName={student.data.studentName}
                  description={student.data.localDescription}
                  receivedBy={student.data.received_user_name}
                  title={"COURSE FEE"}
                  currency={student.data.localPaidCurrency}
                  currencyWord={getCurrencyWord("localPaidCurrency")}
                />
              </div>
            </Tabs.TabPane>
          )}

          {student.data.uniAmount > 0 && (
            <Tabs.TabPane
              key="22"
              tab={
                <span>
                  <DollarCircleOutlined />
                  University
                </span>
              }
            >
              <div id="aims-receipt-uni" className="pdfobject-container">
                <Receipt
                  printId={"aims-receipt-uni"}
                  receiptNo={student.data.uniReceiptNo}
                  paymentMethod={student.data.unilPaymentMethod}
                  amount={student.data.uniAmount}
                  date={student.data.uniPaymentDate}
                  registrationNo={student.data.registrationNo}
                  studentNo={student.data.studentNo}
                  studentName={student.data.studentName}
                  description={student.data.uniDescription}
                  receivedBy={student.data.received_user_name}
                  title={"UNI FEE"}
                  currency={student.data.uniPaidCurrency}
                  currencyWord={getCurrencyWord("uniPaidCurrency")}
                />
              </div>
            </Tabs.TabPane>
          )}

          {student.data.otherAmount > 0 && (
            <Tabs.TabPane
              key="33"
              tab={
                <span>
                  <DollarCircleOutlined />
                  Other
                </span>
              }
            >
              <div id="aims-receipt-uni" className="pdfobject-container">
                <Receipt
                  printId={"aims-receipt-uni"}
                  receiptNo={student.data.otherReceiptNo}
                  paymentMethod={student.data.otherPaymentMethod}
                  amount={student.data.otherAmount}
                  date={student.data.otherPaymentDate}
                  registrationNo={student.data.registrationNo}
                  studentNo={student.data.studentNo}
                  studentName={student.data.studentName}
                  description={student.data.otherDescription}
                  receivedBy={student.data.received_user_name}
                  title={"OTHER FEE"}
                  currency={student.data.otherPaidCurrency}
                  currencyWord={getCurrencyWord("otherPaidCurrency")}
                />
              </div>
            </Tabs.TabPane>
          )}
        </Tabs>
      </Drawer>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration, branch, payment } = state;

  const { student, printDrawer } = registration;

  return {
    inquiry,
    course,
    user,
    student,
    branch,
    payment,
    printDrawer,
  };
};

const mapDispatchToProps = {
  drawerClose: Actions.registration.drawer.close,
};

export default connect(mapStateToProps, mapDispatchToProps)(Payments);
