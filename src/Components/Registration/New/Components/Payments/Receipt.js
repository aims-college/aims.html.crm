import React, { useState, useRef, useEffect } from "react";
import { jsPDF } from "jspdf";
import PDFObject from "pdfobject";
const numWords = require("num-words");
import { $AmountLabel } from "Components";

export const Receipt = (props) => {
  const doc = new jsPDF("p", "pt", "tabloid", true);

  useEffect(() => {
    const source = document.getElementById(props.printId);
    doc.setFontSize(9);
    doc.setDisplayMode("fullwidth");
    source &&
      doc.html(source, {
        callback: function (doc) {
          PDFObject.embed(doc.output("bloburl"), `#${props.printId}`);
        },
        x: 15,
        y: 15,
      });
  }, [props.printId]);

  return (
    <table
      border-spacing={0}
      style={{ border: "1px solid #000", width: "400px" }}
      cellSpacing={0}
      cellPadding={0}
      border={0}
    >
      <tbody>
        <tr>
          <td
            style={{
              textAlign: "center",
              borderBottom: "1px solid #000",
              fontFamily: "Arial, Helvetica, sans-serif",
              fontSize: "14px",
              paddingBottom: "10px",
            }}
          >
            <p
              style={{
                textDecoration: "underline",
                textTransform: "uppercase",
                fontWeight: 900,
              }}
            >
              RECEIPT
            </p>
            <h2 style={{ fontWeight: 900 }}>
              AIMS COLLEGE BUSINESS IT (Pvt) Ltd
            </h2>
            <p style={{ margin: "0px", padding: "0px" }}>
              CITY CAMPUS : No. 33A, Vijeya Kumarathunga MW, Colombo 5, Srilanka
              <br />
              NEGOMBO CAMPUS : No. 349/2, Main Street, Negombo, Sri Lanka <br />
              Island wide Tele No : +94 11-7574500, <br />
              <span style={{ marginRight: "5px" }}>
                Email : info@aimscollege.lk
              </span>
              <span>Web : www.aimscollege.edu.lk</span>
            </p>
            <p
              style={{
                fontSize: "24px",
                textTransform: "uppercase",
                margin: "0px",
                padding: "0px",
              }}
            >
              {props.title}
            </p>
          </td>
        </tr>
        <tr>
          <td>
            <table
              border-spacing={0}
              style={{
                width: "760px",
                fontFamily: "Arial, Helvetica, sans-serif",
                fontSize: "14px",
              }}
              cellSpacing={0}
              cellPadding={5}
              border={0}
            >
              <tbody>
                <tr>
                  <td style={{ borderRight: "1px solid #000", width: "380px" }}>
                    Receipt No.&nbsp; &nbsp;&nbsp;&nbsp;: {props.receiptNo}
                  </td>
                  <td style={{ width: "380px" }}>
                    Dated &nbsp; &nbsp;&nbsp;&nbsp;: {props.date}
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td
            style={{
              fontFamily: "Arial, Helvetica, sans-serif",
              fontSize: "14px",
              borderTop: "1px solid #000",
              padding: "5px",
            }}
          >
            <p>Student's ID No: {props.registrationNo}</p>
            <p>
              Received with thanks from {props.studentName} {props.studentNo}{" "}
            </p>
            <p>
              Sum of {props.currencyWord} {numWords(props.amount)} Only
            </p>
            <p>As per the following details :</p>
          </td>
        </tr>
        <tr>
          <td
            style={{
              fontFamily: "Arial, Helvetica, sans-serif",
              fontSize: "14px",
              borderTop: "1px solid #000",
              padding: "5px",
            }}
          >
            <p
              style={{
                marginTop: "0px",
                marginBottom: "0px",
                padding: "0px",
              }}
            >
              <strong>
                {props.description} {props.paymentMethod}
              </strong>
            </p>
            <br />
          </td>
        </tr>
        <tr>
          <td
            style={{
              fontFamily: "Arial, Helvetica, sans-serif",
              fontSize: "14px",
              borderTop: "1px solid #000",
            }}
          >
            <table
              border-spacing={0}
              style={{
                width: "760px",
                fontFamily: "Arial, Helvetica, sans-serif",
                fontSize: "14px",
              }}
              cellSpacing={0}
              cellPadding={5}
              border={0}
            >
              <tbody>
                <tr>
                  <td style={{ paddingTop: "20px", paddingBottom: "20px" }}>
                    <p
                      style={{
                        margin: "0px",
                        padding: "0px",
                        textDecoration: "underline",
                      }}
                    >
                      <strong>
                        {props.currency} : <$AmountLabel value={props.amount} />
                      </strong>
                    </p>
                    <p
                      style={{
                        margin: "0px",
                        padding: "0px",
                        fontSize: "10px",
                      }}
                    >
                      (Cheque Subject to Realization)
                    </p>
                    <p
                      style={{
                        margin: "0px",
                        padding: "0px",
                        paddingTop: "5px",
                      }}
                    >
                      <strong>All Payments Are Non Refundable</strong>
                    </p>
                  </td>
                  <td style={{ width: "380px" }}>
                    <p
                      style={{
                        margin: "0px",
                        padding: "0px",
                        textAlign: "center",
                      }}
                    >
                      <strong>AIMS COLLEGE BUSINESS &amp; IT (Pvt) Ltd</strong>
                    </p>
                    <p
                      style={{
                        margin: "0px",
                        padding: "0px",
                        textAlign: "center",
                      }}
                    >
                      {props.receivedBy}
                    </p>
                    <p
                      style={{
                        margin: "0px",
                        padding: "0px",
                        textAlign: "center",
                      }}
                    >
                      <strong>Authorised Signatory</strong>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  );
};
