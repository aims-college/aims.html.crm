import React, { useState, useRef, useEffect } from "react";
import { Formik, FieldArray, useField } from "formik";
import { MinusCircleOutlined, PlusOutlined, UploadOutlined } from "@ant-design/icons";
import {
  Affix,
  Badge,
  Layout,
  Comment,
  Avatar,
  Menu,
  Dropdown,
  List,
  PageHeader,
  Radio,
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Space,
  Button,
  Tag,
  Divider,
  Upload,
} from "antd";
import {
  $Select,
  $Input,
  $DatePicker,
  $DateLabel,
  $RangePicker,
} from "Components/antd";

const WorkExperience: React.FC<any> = (props) => {
  const { Option } = Select;
  const { RangePicker } = DatePicker;
  const [workExperiance] = useField("workExperiance");

  return (
    <>
      <h2 className="mb-0">Work Experiance</h2>
      <p className="text-muted">Please list LATEST position first</p>
      <Form autoComplete="off">
        <FieldArray
          name="workExperiance"
          render={({ push, remove, replace }) => (
            <>
              {workExperiance.value.map((data: any, index: any) => (
                <div key={index}>
                  <Row gutter={16}>
                    <Col span={4}>
                      <Form.Item required>
                        <$Input
                          placeholder="Position/Job Title"
                          name={`workExperiance[${index}].position`}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={4}>
                      <Form.Item>
                        <$Input
                          placeholder="Major Responsibility"
                          name={`workExperiance[${index}].major_responsibility`}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={4}>
                      <Form.Item  required>
                        <$Input
                          placeholder="Company Name and Address"
                          name={`workExperiance[${index}].company_name`}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={3}>
                      <Form.Item>
                        <$DatePicker
                          placeholder={"From Date"}
                          name={`workExperiance[${index}].start_date`}
                          value={data.start_date}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={3}>
                      <Form.Item>
                        <$DatePicker
                          placeholder={"End Date"}
                          name={`workExperiance[${index}].end_date`}
                          value={data.end_date}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={2}>
                      <Form.Item>
                        <$Input
                          placeholder="Gross Annual Compensation"
                          name={`workExperiance[${index}].gross_annual_compensation`}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={2}>
                      <Upload
                        maxCount={1}
                        action={`${process.env.REACT_APP_API_URL}/file_upload`}
                        headers={{
                          Authorization: `Bearer ${localStorage.getItem(
                            "token"
                          )}`,
                        }}
                        defaultFileList={data.url && [{
                          url: data?.url,
                          name: data?.name
                        } as any]}
                        onChange={({ fileList }) => {
                          fileList &&
                            Array.isArray(fileList) &&
                            fileList.filter(
                              ({ status }) =>
                                status === "done"
                            ).length > 0 &&
                            fileList.map(
                              ({
                                name,
                                status,
                                response: { file_path },
                              }: any) => {
                                if (status === "done") {
                                  replace(index, {
                                    ...data,
                                    name,
                                    url: file_path,
                                  });
                                }
                              }
                            );
                        }}
                      >
                        <Button icon={<UploadOutlined />}>Upload</Button>
                      </Upload>
                    </Col>
                    <Col span={1}>
                      <MinusCircleOutlined onClick={() => remove(index)} />
                    </Col>
                  </Row>
                </div>
              ))}
              <div className="d-flex mb-5">
                <Form.Item className="mt-4">
                  <Button
                    type="dashed"
                    onClick={() =>
                      push({
                        position: "",
                        major_responsibility: "",
                        start_date: "",
                        end_date: "",
                        gross_annual_compensation: "",
                        name: "",
                        url: "",
                      })
                    }
                    block
                    icon={<PlusOutlined />}
                  >
                    Add field
                  </Button>
                </Form.Item>
              </div>
            </>
          )}
        />
      </Form>
    </>
  );
};

export default WorkExperience;
