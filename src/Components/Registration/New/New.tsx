import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType, useField } from "formik";
import {
  Affix,
  Steps,
  Button,
  Card,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Select,
  Table,
  Tag,
  Skeleton,
} from "antd";
import { DownOutlined, PhoneOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import * as Actions from "Actions";
import "./New.scss";
import Validations from "./Validations";
import { Programs } from "./Components/Programs";
import { PersonalInfomation } from "./Components/PersonalInfomation";
import { Documents } from "./Components/Documents";
import { Educational } from "./Components/Educational";
import { WorkExperience } from "./Components/WorkExperience";
import { Installment } from "./Components/Installment";
import { Payments } from "./Components/Payments";
import { useParams } from "react-router";
import moment from "moment";
import { useHistory } from "react-router-dom";

const { Step } = Steps;
const steps = [
  
  {
    title: "Personal Information",
    content: (
      <>
        <PersonalInfomation></PersonalInfomation>
      </>
    ),
  },
  {
    title: "Educational Attainment",
    content: (
      <>
        <Educational></Educational>
      </>
    ),
  },
  {
    title: "Work Experience",
    content: (
      <>
        <WorkExperience></WorkExperience>
      </>
    ),
  },
  {
    title: "Documents",
    content: (
      <>
        <Documents></Documents>
      </>
    ),
  },
  {
    title: "Installment",
    content: (
      <>
        <Installment></Installment>
      </>
    ),
  },
  {
    title: "Payments",
    content: (
      <>
        <Payments></Payments>
      </>
    ),
  },
];

function studentRepo(params: any) {
  const student_payments = [];

  if (params.localAmount > 0) {
    student_payments.push({
      payment_type: 1,
      payment_method: params.localPaymentMethod,
      amount: params.localAmount,
      payment_currency: 'LKR',
      currency_conversion_rate: '1',
      description: params.localDescription,
      account_id: params.account_id_local,
      reference_url:
        params.localRef &&
        params.localRef.url
          ? params.localRef.url
          : null,
    });
  }

  if (params.uniAmount > 0) {
    student_payments.push({
      payment_type: 2,
      payment_method: params.uniPaymentMethod,
      amount: params.uniAmount,
      payment_currency: params.uniPaidCurrency,
      currency_conversion_rate: params.conversionRate,
      description: params.uniDescription,
      account_id: params.account_id_uni,
      reference_url:
        params.uniRef &&
        params.uniRef.url
          ? params.uniRef.url
          : null,
    });
  }

  if (params.otherAmount > 0) {
    student_payments.push({
      payment_type: 3,
      payment_method: params.otherPaymentMethod,
      amount: params.otherAmount,
      payment_currency: 'LKR',
      currency_conversion_rate: 1,
      description: params.otherDescription,
      account_id: params.account_id_other,
      reference_url:
        params.otherRef &&
        params.otherRef.url
          ? params.otherRef.url
          : null,
    });
  }

  return {
    first_name: params.fName,
    middle_name: params.middleName,
    last_name: params.lName,
    nic: params.nic,
    email: params.email,
    dob: moment(params.birthdate).format('YYYY-MM-DD'),
    country_of_citizenship: params.countryOfCitizenship,
    name_in_certificate: params.nameInCertificate,
    gender: params.gender,
    civil_status: params.civilStatus,
    current_address: params.street,
    current_address_city: params.city,
    current_address_country: params.country,
    contact_no_1: params.primaryContact,
    contact_no_2: params.secondaryContact,
    inquiry_id: params.inquiry_id,
    course_id: params.course_id,
    branch_id: params.branch,
    discount_id: params.discountId,
    batch_code: params.batchCode,
    student_code:
      params.courseCode + params.courseMedium + "-" + params.branchCode,
    medium: params.courseMedium,
    course_fee: params.courseFee - params.discountAmount,
    registration_fee: params.registrationFee,
    book_fee: params.bookFee,
    company_type: params.company_type,
    discount_amount: params.discountAmount,
    discount_approval_status: 1,
    discount_approved_by: params.approvalId,
    register_date: moment(),
    student_education: params.educational.map(
      (educational: any, index: number) => ({
        ...educational,
        ref_url: educational.url,
      })
    ),
    student_prerequisite_course: params.coursesTaken.map(
      (coursesTaken: any, index: number) => ({
        ...coursesTaken,
        ref_url: coursesTaken.url,
      })
    ),
    student_work_experience: params.workExperiance.map(
      (workExperiance: any, index: number) => ({
        ...workExperiance,
        ref_url: workExperiance.url,
      })
    ),
    student_document_checklist: params.documents,
    student_course_installment: params.installments.map(
      (installments: any, index: number) => ({
        ...installments,
        installment_no: index + 1,
      })
    ),
    student_payments,
  };
}

const Registration: React.FC<any> = ({
  getInqueryById,
  student,
  getBranches,
  getUsers,
  getDiscounts,
  getAllCurrencies,
  getRegistrationNumber,
  saveRegister,
  branch,
  getBankAccounts
}) => {
  const [current, setCurrent] = React.useState(0);
  const routeParams = useParams<{ id: string }>();
  const history = useHistory();

  useEffect(() => {
    if (routeParams && routeParams.hasOwnProperty("id")) {
      getInqueryById(routeParams.id);
      getBranches();
      getUsers();
      getDiscounts();
      getAllCurrencies();
      getBankAccounts()
    }
  }, [routeParams.id]);
  

  return (
    <>
      <Formik
        initialValues={{ ...student.data, inquiry_id: routeParams.id, discountApproval: false }}
        validateOnBlur={true}
        validateOnChange={true}
        validateOnMount={true}
        isInitialValid={false}
        validationSchema={Validations}
        enableReinitialize
        onSubmit={(values) => {
          console.log(studentRepo(values));
          saveRegister(studentRepo(values));
        }}
      >
        {({
          values,
          handleSubmit,
          errors,
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          validateForm,
        }) => (
          <>
            <div className="mb-5">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <Skeleton loading={student.isLoading} paragraph={{ rows: 1 }}>
                    <PageHeader
                      className="px-0"
                      onBack={() => history.goBack()}
                      title={`New Registration : ${student.registrationNo}`}
                    />
                  </Skeleton>
                </div>
              </Affix>

              <div>
                <div className="px-5 pt-4">
                  <Steps current={current} direction="horizontal" size="small">
                    {steps.map((item) => (
                      <Step key={item.title} title={item.title} />
                    ))}
                  </Steps>

                  <div className="d-flex align-items-center mt-2">
                    <h3 className="font-weight-bold mb-0">
                      {values.fName} {values.lName}
                    </h3>
                    <Divider type="vertical" className="mx-4"></Divider>
                    <h3 className="font-weight-bold mb-0">
                      {student.data.course_name}
                    </h3>
                  </div>
                  <Divider></Divider>
                </div>
                <div className="flex-fill px-5">
                  <div className="steps-content">{steps[current]?.content}</div>
                </div>
              </div>
            </div>
            <div className="form-bottom d-flex justify-content-end">
              {current !== 0 && (
                <Button
                  type="primary"
                  className="mr-2"
                  onClick={() => setCurrent(current - 1)}
                >
                  Previous
                </Button>
              )}
              {current !== 5 && (
                <Button
                  type="primary"
                  className="mr-2"
                  // disabled={isSubmitting || isValidating || !dirty || !isValid}
                  onClick={() => {
                    setCurrent(current + 1);
                  }}
                >
                  Next
                </Button>
              )}
              {current == 5 && (
                <Button
                  type="default"
                  onClick={(e: any) => handleSubmit(e)}
                  // disabled={isSubmitting || isValidating || !dirty || !isValid}
                >
                  Save
                </Button>
              )}

              <Button type="default" onClick={() => history.goBack()}>
                Cancel
              </Button>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { inquiry, course, user, registration, branch, payment } = state;

  const { student } = registration;

  return {
    inquiry,
    course,
    user,
    student,
    branch,
    payment
  };
};

const mapDispatchToProps = {
  getRegistrationNumber: Actions.registration.registrationNumber.get,
  getCourses: Actions.course.self.get,
  getInqueryById: Actions.inquery.byId.get,
  getBranches: Actions.branch.self.get,
  saveRegister: Actions.registration.save.post,
  getDiscounts: Actions.registration.discounts.get,
  getUsers: Actions.user.users.get,
  getAllCurrencies: Actions.payments.currencies.get,
  getBankAccounts: Actions.payments.bankAccounts.get,
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
