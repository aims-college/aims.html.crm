import { message } from "antd";
import * as Yup from "yup";

export default Yup.object().shape({
  fName: Yup.string()
    .required("First Name is required")
    .typeError("First Name is invalid"),
  nic: Yup.string().required("NIC is required").typeError("NIC is invalid"),
  email: Yup.string().email("Email is invalid").nullable(),
  primaryContact: Yup.number().typeError("Primary Contact is invalid"),
  lName: Yup.string()
    .required("Last Name is required")
    .typeError("Last Name is invalid"),
  nameInCertificate: Yup.string()
    .required("Name in certificate is required")
    .typeError("Name in certificate is invalid"),
  branch: Yup.string()
    .required("Branch is required")
    .typeError("Branch is invalid"),
  batchCode: Yup.string()
    .required("Batch is required")
    .typeError("Batch is invalid"),
  birthdate: Yup.string()
    .required("Birthday is required")
    .typeError("Birthday is invalid"),
  documents: Yup.array().of(
    Yup.object().shape({
      checked: Yup.boolean().required(),
    })
  ),
  educational: Yup.array().of(
    Yup.object().shape({
      edu_level: Yup.string().required("Level is required"),
      school_name: Yup.string().required("School Name and Address is required"),
    })
  ),
  coursesTaken: Yup.array().of(
    Yup.object().shape({
      level: Yup.string().required("Level is required"),
      school_name: Yup.string().required("School Name and Address is required"),
      year_taken: Yup.number()
        .required("Year is required")
        .typeError("Year is invalid"),
    })
  ),
  workExperiance: Yup.array().of(
    Yup.object().shape({
      position: Yup.string().required("Position is required"),
      company_name: Yup.string().required("Company Name is required"),
    })
  ),
  localAmount: Yup.number()
    // .moreThan(0, "Local Fee must be greater than 0")
    .typeError("Local Fee is invalid"),
  uniAmount: Yup.number()
    // .moreThan(0, "Local Fee must be greater than 0")
    .typeError("University Fee is invalid"),
  otherAmount: Yup.number()
    // .moreThan(0, "Other Fee must be greater than 0")
    .typeError("Other Fee is invalid"),
  method: Yup.number()
    .required("No of installments is required")
    .typeError("No of installments is invalid"),
  installments: Yup.array()
    .of(
      Yup.object().shape({
        local_amount: Yup.number()
          .required("Local Fee is required")
          .typeError("Local Fee is invalid"),

        uni_amount: Yup.number().typeError("Uni Fee is invalid"),
      })
    )
    .test(
      "sum",
      "Installments sum doesn't match with course fee",
      function (rows: any = []) {
        const total = rows.reduce((total: any, row: any) => {
          return total + (row.local_amount || 0);
        }, 0);

        return (
          total <=
          this.parent.courseFee -
            (this.parent.discountApproval ? this.parent.discountAmount : 0)
        );
      }
    ),
});
