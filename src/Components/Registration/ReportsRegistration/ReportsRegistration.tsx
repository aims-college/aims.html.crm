import React, { useState, useRef, useEffect } from "react";
import { Formik, FormikContextType } from "formik";
import {
  Affix,
  Alert,
  Button,
  DatePicker,
  Divider,
  notification,
  Dropdown,
  Form,
  Input,
  Menu,
  PageHeader,
  Table,
  Tag,
  Popconfirm,
} from "antd";

import { $Select, $Input, $DatePicker, $RangePicker } from "Components/antd";

import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import * as Actions from "Actions";
import { useHistory } from "react-router-dom";
import moment from "moment";

const ReportsRegistration = ({ getReports, registration }: any) => {
  const [filter, setFilter] = useState({
    report_type: 1,
    start_date: moment().subtract(1, "months").format("YYYY-MM-DD"),
    end_date: moment().format("YYYY-MM-DD"),
  });

  const history = useHistory();
  const [registrationsLink, setRegistrationsLink] = useState<string>(
    `/registrations?start_date=${filter.start_date}&end_date=${filter.end_date}`
  );

  useEffect(() => {
    getReports(filter);
  }, []);

  const columns: any = (setFieldValue: any) => {
    return [
      {
        title: "By Type",
        key: "report_type",
        dataIndex: "name",
        width: 110,
        ellipsis: true,
      },
      {
        title: "Total",
        dataIndex: "total",
        key: "total",
        width: 110,
        ellipsis: true,
      },
    ];
  };
  return (
    <>
      <Formik
        initialValues={{
          ...filter,
          authUserId: localStorage.getItem("user_id"),
          dateRange: [moment().subtract(1, "months"), moment()],
        }}
        onSubmit={(values) => {}}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          isSubmitting,
          isValidating,
          resetForm,
        }: any) => (
          <>
            <div className="">
              <Affix offsetTop={0}>
                <div className="page-header header-border">
                  <PageHeader
                    className="px-0"
                    onBack={() => history.goBack()}
                    title="Registration Reports"
                  />
                </div>
              </Affix>

              <div className="inq-layout">
                <Affix offsetTop={0}>
                  <aside className="inq-side">
                    <h3>Filter</h3>
                    <Form layout="vertical">
                      <$RangePicker
                        size="small"
                        className="mb-3"
                        name="dateRange"
                        onChange={(dateRange: any) => {
                          if (dateRange) {
                            setRegistrationsLink(
                              `/registrations?start_date=${dateRange[0].format(
                                "YYYY-MM-DD"
                              )}&end_date=${dateRange[1].format("YYYY-MM-DD")}`
                            );
                            setFilter({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                            getReports({
                              ...filter,
                              start_date: dateRange[0].format("YYYY-MM-DD"),
                              end_date: dateRange[1].format("YYYY-MM-DD"),
                            });
                          }
                        }}
                      />

                      <$Select
                        name="report_type"
                        size="small"
                        formitem={{
                          label: "Type",
                        }}
                        options={[
                          {
                            id: 1,
                            name: "By Course",
                          },
                          {
                            id: 2,
                            name: "By Faculty",
                          },
                          {
                            id: 3,
                            name: "By Handler",
                          },
                          {
                            id: 4,
                            name: "By Type",
                          },
                        ]}
                        value={filter.report_type}
                        optionValue="id"
                        optionText="name"
                        defaultValue="all"
                        allOption={false}
                        onChange={(report_type: any) => {
                          if (report_type == "all") {
                            setFilter({ ...filter, report_type: -1 });
                            getReports({ ...filter, report_type: -1 });
                          } else {
                            setFilter({ ...filter, report_type });
                            getReports({ ...filter, report_type });
                          }
                        }}
                      />
                    </Form>
                  </aside>
                </Affix>
                <div className="flex-fill ">
                  <div>
                    <Table
                      columns={columns(setFieldValue)}
                      size="small"
                      dataSource={registration.reports.data}
                      loading={registration.reports.isLoading}
                      rowKey={(record) => record.id}
                      onRow={(record, rowIndex) => {
                        return {
                          onDoubleClick: (event) => {
                            switch (values.report_type) {
                              case 1:
                                registrationsLink &&
                                  history.push(
                                    `${registrationsLink}&course_id=${record.id}`
                                  );
                                break;
                              case 2:
                                registrationsLink &&
                                  history.push(
                                    `${registrationsLink}&faculty_id=${record.id}`
                                  );
                                break;
                              case 3:
                                registrationsLink &&
                                  history.push(
                                    `${registrationsLink}&user_id=${record.front_officer_id}`
                                  );
                                break;
                              case 4:
                                registrationsLink &&
                                  history.push(
                                    `${registrationsLink}&inquiry_type_id=${record.id}`
                                  );
                                break;
                              default:
                                break;
                            }
                          },
                        };
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { registration } = state;

  return {
    registration,
  };
};

const mapDispatchToProps = {
  getReports: Actions.registration.reports.get,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportsRegistration);
