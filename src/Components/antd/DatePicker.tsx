import React, { useEffect, useState } from "react";
import { DatePicker, Form } from "antd";
import { useField, Field } from "formik";
import moment from "moment";
import * as Utility from "../utility";

export const $DatePicker = ({
  name,
  label,
  formProps,
  required,
  format,
  onChange,
  defaultPickerValue,
  defaultValue,
  dateFormat,
  disabledDate,
  value,
  style,
  size,
  placeholder,
  touched,
  allowClear = false,
  onBlur,
  ...rest
}: any) => {
  const [field, meta, helpers] = useField(name);

  const error = meta?.error;
  const isTouched = meta?.touched;

  return (
    <Form.Item
      label={label}
      help={error && error as string}
      {...formProps}
      required={typeof required == "undefined" ? false : true}
      validateStatus={Utility.validateStatus(error, true)}
    >
      <Field
        as={DatePicker}
        {...field}
        {...rest}
        value={value && moment(value).isValid() ? moment(value) : null}
        size={size}
        name={name}
        allowClear={allowClear}
        style={style}
        format={format}
        id={name}
        defaultValue={defaultValue}
        placeholder={placeholder}
        disabledDate={disabledDate}
        defaultPickerValue={defaultPickerValue}
        onChange={(date: any) => {
          onChange && onChange;
          helpers.setValue(moment(date, format));
        }}
        onBlur={onBlur}
      />
    </Form.Item>
  );
};
