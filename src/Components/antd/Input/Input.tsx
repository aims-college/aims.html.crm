import React from 'react';
import { Input, Form } from 'antd';
import { useField, Field } from 'formik';
import * as Utility from '../../utility'
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

const $I = React.forwardRef(({
  name,
  label,
  placeholder,
  type,
  size,
  className,
  disabled,
  allowClear,
  required,
  suffix,
  prefix,
  maxLength,
  onKeyDown,
  onBlur,
  onChange,
  addonBefore,
  addonAfter,
  onPressEnter,
  max,
  min,
  style,
  tabIndex,
  dataTestid,
  autoFocus
}: any, ref: any) => {
  const [field, meta, helpers] = useField(name);
  const error = meta?.error;
  const isTouched = meta?.touched;
  return (
    <Form.Item
      label={label}
      help={error && error}
      validateStatus={Utility.validateStatus(error, true)}
      required={typeof required == 'undefined' ? false : true}
    >
      <Input
        {...field}
        placeholder={placeholder}
        type={type}
        style={style}
        max={max}
        min={min}
        ref={ref}
        size={size}
        tabIndex={tabIndex}
        disabled={disabled}
        className={className}
        addonBefore={addonBefore} 
        addonAfter={addonAfter}
        suffix={suffix}
        prefix={prefix}
        data-testid={dataTestid}
        maxLength={maxLength}
        allowClear={allowClear}
        autoFocus={autoFocus}
        onKeyDown={onKeyDown}
        onPressEnter={onPressEnter}
        onBlur={(e: any) => {
          onBlur && onBlur(e);
          field.onBlur(e);
        }}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          typeof onChange == 'function'
            ? onChange(e)
            : helpers?.setValue(e.target.value);
        }}
      />
    </Form.Item>
  );
});

$I.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.string,
  allowClear: PropTypes.bool,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  suffix: PropTypes.element,
  prefix: PropTypes.element,
  maxLength: PropTypes.number,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  addonBefore: PropTypes.element,
  addonAfter: PropTypes.element,
  onPressEnter: PropTypes.func,
  max: PropTypes.number,
  min: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
}

export const $Input = $I