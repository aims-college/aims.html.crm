import React from "react";
import { Input, Form } from "antd";
import { useField } from "formik";
import * as Utility from "../../utility";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";

/** 
 * @function create returns control digit using modulus 11 algorithm 
 * @param input value for create control digit
*/
const create = (input: string) => {
  let sum = 0;
  input
    ?.split("")
    ?.reverse()
    ?.forEach(function (value, index) {
      sum += parseInt(value, 10) * ((index % 6) + 2);
    });
  let sumMod11 = sum % 11;
  if (sumMod11 === 0) {
    return "0";
  } else if (sumMod11 === 1) {
    return "-";
  } else {
    return 11 - sumMod11 + "";
  }
};

/** 
 * @function isValid check provided value is valid 
 * @param input value for create control digit
 * 
*/
const isValid = (input: string) => {
  let formatted = input?.replace(/\D/g, "");
  let checkDigitIndex = formatted?.length - 1;
  return (
    /^[0-9-( )]+$/.test(input) &&
    formatted?.length === 11 &&
    formatted?.substr(checkDigitIndex) ===
      create(formatted?.substr(0, checkDigitIndex))
  );
};

const $I = React.forwardRef(
  (
    {
      name,
      label,
      placeholder,
      type,
      size,
      className,
      disabled,
      allowClear,
      validationMessage,
      required = false,
      suffix,
      prefix,
      maxLength,
      onKeyDown,
      onBlur,
      onChange,
      addonBefore,
      addonAfter,
      onPressEnter,
      max,
      min,
      style,
      tabIndex,
      dataTestid,
    }: any,
    ref: any
  ) => {
    const [field, meta, helpers] = useField({
      name,
      validate: (value) => {
        let message: any = undefined;
        if (!isValid(value) && value) {
          message = validationMessage
            ? validationMessage
            : "COMMON.INVALID_ACCOUNT_NO";
        }
        return message;
      },
    });

    const error = meta?.error;
    const isTouched = meta?.touched;
    const { t } = useTranslation();

    return (
      <Form.Item
        label={label}
        help={isTouched && error && t(error as string)}
        validateStatus={Utility.validateStatus(error, isTouched)}
        required={required}
      >
        <Input
          {...field}
          placeholder={placeholder}
          type={type}
          style={style}
          max={max}
          min={min}
          ref={ref}
          size={size}
          tabIndex={tabIndex}
          disabled={disabled}
          className={className}
          addonBefore={addonBefore}
          addonAfter={addonAfter}
          suffix={suffix}
          prefix={prefix}
          data-testid={dataTestid}
          maxLength={maxLength}
          allowClear={allowClear}
          onKeyDown={onKeyDown}
          onPressEnter={onPressEnter}
          onBlur={(e: any) => {
            onBlur && onBlur(e);
            field.onBlur(e);
          }}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            typeof onChange == "function"
              ? onChange(e)
              : helpers?.setValue(e.target.value);
          }}
        />
      </Form.Item>
    );
  }
);

$I.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  validationMessage: PropTypes.string,
  type: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.string,
  allowClear: PropTypes.bool,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  suffix: PropTypes.element,
  prefix: PropTypes.element,
  maxLength: PropTypes.number,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  addonBefore: PropTypes.element,
  addonAfter: PropTypes.element,
  onPressEnter: PropTypes.func,
  max: PropTypes.number,
  min: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export const $InputAccountNo = $I;
