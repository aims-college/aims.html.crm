import React from 'react';
import { InputNumber, Form } from 'antd';
import { useField, Field } from 'formik';
import * as Utility from '../../utility';
import { useTranslation } from 'react-i18next';
import ReactNumeric from 'react-numeric';
import PropTypes from 'prop-types';

const $I = ({
  name,
  value,
  label,
  currentLanguage,
  currentCurrency,
  placeholder,
  size,
  disabled = false,
  className,
  allowClear,
  required,
  suffix,
  prefix,
  maxLength,
  onBlur,
  onChange,
  addonBefore,
  onPressEnter,
  onKeyPress,
  max = Number.MAX_SAFE_INTEGER,
  min = Number.MIN_SAFE_INTEGER,
  style,
  defaultValue,
  tabIndex,
  formitem,
  dataTestid
}: any) => {
  const [field, meta, helpers] = useField(name);
  const error = meta?.error;
  const isTouched = meta?.touched;
  
  return (
    <Form.Item
      label={label}
      help={isTouched && error && error as string}
      validateStatus={Utility.validateStatus(error, isTouched)}
      required={typeof required == 'undefined' ? false : true}
      extra={formitem?.extra}
    >
      <div className="ant-form-item-control-input-content">
        <div className={`ant-input-number ant-input-number-sm ${className} ${disabled && 'ant-input-number-disabled'}`}>
          <div className="ant-input-number-input-wrap">
            <ReactNumeric
              currencySymbol=""
              {...field}
              onBlur={(e: any) => {
                onBlur && onBlur(e);
                field.onBlur(e);
              }}
              data-testid={dataTestid}
              style={style}
              tabIndex={tabIndex}
              disabled={disabled}
              placeholder={placeholder}
              className="ant-input-number-input"
              decimalCharacter={currentLanguage === 'en-GB' ? '.' : ','}
              digitGroupSeparator={currentLanguage === 'en-GB' ? ',' : ' '}
              onChange={(e: any, v: number) =>
                typeof onChange == 'function'
                  ? onChange(v)
                  : helpers?.setValue(v)
              }
              onKeyPress={onKeyPress}
              value={value}
              maximumValue={max != undefined ? max.toString() : Number.MAX_SAFE_INTEGER}
              minimumValue={min != undefined ? min.toString() : Number.MIN_SAFE_INTEGER}
            />
          </div>
        </div>
      </div>
      {isTouched && error && (
        <div className="ant-form-item-explain ant-form-item-explain-error"></div>
      )}
    </Form.Item>
  );
};

$I.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.string,
  allowClear: PropTypes.bool,
  required: PropTypes.bool,
  suffix: PropTypes.element,
  prefix: PropTypes.element,
  maxLength: PropTypes.number,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  addonBefore: PropTypes.element,
  onPressEnter: PropTypes.func,
  max: PropTypes.number,
  min: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export const $InputAmount = $I;
