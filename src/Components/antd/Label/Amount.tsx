import React, { useEffect, useState } from "react";
import { InputNumber, Form } from "antd";
import { useSelector } from "react-redux";

export default ({ value }: { value: number }) => {
  return (
    <React.Fragment>
      {new Intl.NumberFormat("en-GB", { style: "currency", currency: 'LKR' }).format(value).replace('LKR', '')}
    </React.Fragment>
  );
};
