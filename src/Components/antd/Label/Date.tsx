import React, { useEffect, useState } from 'react'
import { useSelector } from "react-redux"
import moment from 'moment';

export default ({ value, ...rest }: any) => {

    const { format } = useSelector((state: any) => state.common.date)

    return (
        <React.Fragment {...rest}>
            {
                moment(value)?.format(format)
            }
        </React.Fragment>
    )
}