import React from "react";
import { DatePicker, Form } from "antd";
import { useField, Field } from "formik";
import * as Utility from "../../utility";
import moment from "moment";

const { RangePicker } = DatePicker;

export const $RangePicker = ({
  errors,
  name,
  picker,
  label,
  hasFeedback,
  currentTimeFormat,
  disabledTime,
  disabledDate,
  value,
  disabled,
  size,
  onOk,
  placeholder,
  touched,
  noStyle,
  onChange,
  onOpenChange,
  minuteStep,
  formItem,
  style,
  order = true,
  format,
  allowClear,
  defaultValue,
  defaultPickerValue,
  onBlur,
}: any) => {
  const [field, meta, helpers] = useField(name);
  const error = meta?.error;
  const isTouched = meta?.touched;

  return (
    <>
        <Form.Item label='From'>
          <Field
            as={DatePicker}
            {...field}
            label='From'
            value={field.value[0] && field.value[0]}
            name={name}
            allowClear={allowClear}
            style={{width: '100%'}}
            format={format}
            id={name}
            defaultValue={defaultValue}
            placeholder={'Start Date'}
            disabledDate={disabledDate}
            defaultPickerValue={defaultPickerValue}
            onChange={(date: any) => {
              onChange && onChange([moment(date, format), field.value[1]]);
              helpers.setValue([moment(date, format), field.value[1]]);
            }}
            onBlur={onBlur}
          />
        </Form.Item>

        <Form.Item label='To'>
          <Field
            as={DatePicker}
            {...field}
            value={field.value[1] && field.value[1]}
            name={name}
            allowClear={allowClear}
            style={{width: '100%'}}
            format={format}
            id={name}
            defaultValue={defaultValue}
            placeholder={'End Date'}
            disabledDate={disabledDate}
            defaultPickerValue={defaultPickerValue}
            onChange={(date: any) => {
              onChange && onChange([field.value[0], moment(date, format)]);
              helpers.setValue([field.value[0], moment(date, format)]);

            }}
            onBlur={onBlur}
          />
        </Form.Item>
      </>
  );
};
