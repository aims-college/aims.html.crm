import React, { useEffect } from "react";
import * as Components from "antd";
import * as Utility from "../utility";
import {
  useField,
  Field,
  FormikValues,
  FieldInputProps,
  FieldMetaProps,
  FieldHelperProps,
} from "formik";

const Select = React.forwardRef(
  (
    {
      value,
      name,
      options,
      optionValue = "value",
      optionText = "label",
      optionExtra = undefined,
      placeholder,
      defaultValue,
      defaultLabel,
      hideSearch,
      allowClear,
      size,
      mode,
      onSearch,
      onChange,
      onSearchBy,
      notFoundContent,
      dropdownRender,
      tabIndex,
      dropdownMatchSelectWidth = true,
      dataTestid,
      ...rest
    }: any,
    ref: any
  ) => {
    const [data, setData] = React.useState<any>([]);
    const [field, meta, helpers] = useField<FormikValues>(name);

    React.useEffect(() => {
      if (typeof onSearchBy !== undefined) {
        setData(options);
      }
    }, [options]);

    return (
      <Components.Select
        {...field}
        tabIndex={tabIndex}
        defaultActiveFirstOption
        value={typeof value == "undefined" ? field?.value : value}
        placeholder={placeholder}
        showSearch={typeof hideSearch == "undefined" ? true : false}
        size={typeof size == "undefined" ? "small" : size}
        mode={mode}
        notFoundContent={notFoundContent}
        dropdownRender={dropdownRender}
        filterOption={false}
        defaultValue={defaultValue}
        ref={ref}
        data-testid={dataTestid}
        dropdownMatchSelectWidth={true}
        getPopupContainer={(trigger: any) => trigger.parentNode}
        onChange={(val: any) => {
          helpers?.setValue(val);
          onChange && onChange(val);
        }}
        onSearch={(value: any) => {
          if (typeof onSearch !== undefined && typeof onSearchBy !== undefined)
            setData(
              options?.filter((o: any) => {
                return (
                  (onSearchBy &&
                    o[onSearchBy[0]]
                      .toLowerCase()
                      ?.includes(value?.toLowerCase())) ||
                  (onSearchBy && o[onSearchBy[1]]?.toString().includes(value))
                );
              })
            );
        }}
        allowClear={typeof allowClear == "undefined" ? false : true}
        {...rest}
      >
        {data?.map((item: any, index: number) => (
          <Components.Select.Option key={index} value={item?.[optionValue]} title={item?.[optionText]}>
            <div className="d-flex flex-fill justify-content-between">
              <span>
                {optionExtra &&
                  optionExtra.hasOwnProperty("extraNumber") &&
                  item?.[optionExtra?.extraNumber] + " - "}
                <span>{item?.[optionText]}</span>
              </span>
              {optionExtra &&
              typeof optionExtra?.extraName !== "undefined" &&
              optionExtra.hasOwnProperty("isAmount") ? (
                <span className="text-muted font-sm">
                  Fee: {item?.[optionExtra?.extraName]}
                </span>
              ) : (
                <span className="text-muted font-sm">
                  {optionExtra &&
                    typeof optionExtra?.extraName !== "undefined" &&
                    item?.[optionExtra?.extraName]}
                </span>
              )}
            </div>
          </Components.Select.Option>
        ))}
      </Components.Select>
    );
  }
);

export default React.forwardRef(
  (
    {
      value,
      name,
      options,
      optionValue,
      optionText,
      defaultValue,
      allOption = true,
      dropdownRender,
      autoFocus,
      selectRef,
      ...restProps
    }: any,
    ref: any
  ) => {
    if (typeof name == "undefined") {
      return (
        <Select
          value={value}
          options={options}
          optionValue={optionValue}
          optionText={optionText}
          name={"default"}
          {...restProps}
        />
      );
    }

    const [field, meta, helpers] = useField<FormikValues>(name);

    const error = meta?.error;
    const isTouched = meta?.touched;

    const allOptions = allOption
      ? [
          {
            [optionValue as any]: defaultValue,
            [optionText as any]: "All",
          },
        ]
      : [];

    return (
      <>
        {restProps.hasOwnProperty("formitem") && (
          <Components.Form.Item
            label={restProps?.formitem?.label}
            className={restProps?.formitem?.className}
            style={restProps?.formitem?.style}
            extra={restProps?.formitem?.extra}
            {...restProps?.formitem?.props}
            help={error && (error as string)}
            validateStatus={Utility.validateStatus(error, true)}
            required={typeof restProps.required == "undefined" ? false : true}
          >
            <Select
              value={value}
              options={allOptions.concat(options)}
              defaultValue={defaultValue}
              optionValue={optionValue}
              optionText={optionText}
              dropdownRender={dropdownRender}
              name={name}
              autoFocus={autoFocus}
              ref={selectRef}
              {...restProps}
            />
          </Components.Form.Item>
        )}

        {!restProps.hasOwnProperty("formitem") && (
          <Select
            value={value}
            options={allOptions.concat(options)}
            defaultValue={defaultValue}
            optionValue={optionValue}
            optionText={optionText}
            dropdownRender={dropdownRender}
            name={name}
            {...restProps}
          />
        )}
      </>
    );
  }
);
