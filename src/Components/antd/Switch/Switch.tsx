import React from "react";
import * as Components from "antd";
import * as Utility from "../../utility";
import { useField, Field } from "formik";

export default ({
  value,
  setFieldValue,
  name,
  errors,
  formitem,
  formitemlabel,
  onChange,
  disabled,
  checked,
  touched,
  className,
  size,
  ...restProps
}: any) => {
  const [field, meta, helpers] = useField(name);
  const error = meta?.error;
  const isTouched = meta?.touched;

  return (
    <>
      {restProps.hasOwnProperty("formitem") && (
        <Components.Form.Item
          label={formitemlabel}
          help={error && error as string}
          validateStatus={Utility.validateStatus(error, true)}
        >
          <Field
            as={Components.Switch}
            disabled={disabled}
            checked={checked}
            className={className}
            size={restProps.hasOwnProperty("size") ? size : "small"}
            {...field}
            onChange={
              typeof onChange == "function"
                ? onChange
                : (v: any) => helpers.setValue(v)
            }
          />
        </Components.Form.Item>
      )}

      {
        <Field
          as={Components.Switch}
          disabled={disabled}
          checked={checked}
          className={className}
          size={restProps.hasOwnProperty("size") ? size : "small"}
          {...field}
          onChange={
            typeof onChange == "function"
              ? onChange
              : (v: any) => helpers.setValue(v)
          }
        />
      }
    </>
  );
};
