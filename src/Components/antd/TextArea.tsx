import React, { useEffect, useState } from "react";
import { Form, Input } from "antd";
import * as Utility from "../utility";
import { useField, Field } from "formik";

const { TextArea } = Input;

export const $TextArea = React.forwardRef(
  ({ name, label, formItem, required, ...rest }: any, ref: any) => {
    const [field, meta, helpers] = useField(name);
    const error = meta?.error;
    const isTouched = meta?.touched;

    return (
      <Form.Item
        label={label}
        {...formItem}
        help={error && (error as string)}
        validateStatus={Utility.validateStatus(error, true)}
        required={typeof required == "undefined" ? false : true}
      >
        <TextArea ref={ref} {...rest} {...field} />
      </Form.Item>
    );
  }
);
