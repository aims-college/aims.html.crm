import _ from "lodash"

export const error = ({ errors, name }: any): string => _.get(errors, name)
export const isTouched = ({ touched, name }: any): boolean => _.get(touched, name)