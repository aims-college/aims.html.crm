export default (error: any, isTouched: boolean): any => {
    if (typeof error == "undefined" && isTouched)
        return "success"
    else if (typeof error !== "undefined" && isTouched)
        return "error"
    else
        return ""
}
