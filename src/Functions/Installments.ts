import moment from "moment";
import { message } from "antd";

export const getInstallments = (val: number, courseDuration: any, accepted_uni_currency: string, uni_currency_conversion_rate: number) => {
  try {
    return Array.apply(null, Array(Number(val)))
      .map((data, index: number) => {
        if (index === 0) {
          return {
            installment_no: 1,
            due_date: moment(),
            local_amount: 0,
            uni_amount: 0,
            local_paid_amount: 0,
            uni_paid_amount: 0,
            accepted_local_currency: "LKR",
            local_currency_conversion_rate: "1",
            accepted_uni_currency,
            uni_currency_conversion_rate,
            hide: false,
          };
        } else {
          return {
            installment_no: index + 1,
            due_date: moment().add(index, "months").set("date", 10),
            local_amount: 0,
            uni_amount: 0,
            local_paid_amount: 0,
            uni_paid_amount: 0,
            accepted_local_currency: "LKR",
            local_currency_conversion_rate: "1",
            accepted_uni_currency,
            uni_currency_conversion_rate,
            hide: false,
          };
        }
      })
      .filter(({ hide }: { hide: boolean }) => hide !== true);
  } catch (error) {
    return [];
  }

  // if (method == 1) {
  //   return Array.apply(null, Array(Number(courseDuration.value)))
  //     .map((data, index: number) => {
  //       if (index === 0) {
  //         return {
  //           installment_no: 1,
  //           due_date: moment(),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else {
  //         return {
  //           installment_no: index + 1,
  //           due_date: moment().add(index, "months").set("date", 10),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       }
  //     })
  //     .filter(({ hide }: { hide: boolean }) => hide !== true);
  // } else if (method == 4) {
  //   return Array.apply(null, Array(Number(courseDuration.value)))
  //     .map((data, index: number) => {
  //       if (index === 0) {
  //         return {
  //           installment_no: 1,
  //           due_date: moment(),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else if (index % 4 == 0) {
  //         return {
  //           installment_no: index + 1,
  //           due_date: moment().add(index, "months").set("date", 10),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else {
  //         return {
  //           installment_no: index + 1,
  //           due_date: "",
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: true,
  //         };
  //       }
  //     })
  //     .filter(({ hide }: { hide: boolean }) => hide !== true);
  // } else if (method == 6) {
  //   return Array.apply(null, Array(Number(courseDuration.value)))
  //     .map((data, index: number) => {
  //       if (index === 0) {
  //         return {
  //           installment_no: 1,
  //           due_date: moment(),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else if (index % 6 == 0) {
  //         return {
  //           installment_no: index + 1,
  //           due_date: moment().add(index, "months").set("date", 10),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else {
  //         return {
  //           installment_no: index + 1,
  //           due_date: "",
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: true,
  //         };
  //       }
  //     })
  //     .filter(({ hide }: { hide: boolean }) => hide !== true);
  // } else if (method == 12) {
  //   return Array.apply(null, Array(Number(courseDuration.value)))
  //     .map((data, index: number) => {
  //       if (index === 0) {
  //         return {
  //           installment_no: 1,
  //           due_date: moment(),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else if (index % 12 == 0) {
  //         return {
  //           installment_no: index + 1,
  //           due_date: moment().add(index, "months").set("date", 10),
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: false,
  //         };
  //       } else {
  //         return {
  //           installment_no: index + 1,
  //           due_date: "",
  //           local_amount: 0,
  //           uni_amount: 0,
  //           local_paid_amount: 0,
  //           uni_paid_amount: 0,
  //           hide: true,
  //         };
  //       }
  //     })
  //     .filter(({ hide }: { hide: boolean }) => hide !== true);
  // }
};

export const getDiscountedInstallmemts = (
  discount: any,
  installments: any,
  setValue: any,
  courseFee: number,
  type: "byPercentage" | "byValue"
) => {
  try {
    let remaning =
      type == "byPercentage" ? (courseFee * discount) / 100 : discount;

    const ins = [...installments]
      .reverse()
      .map(({ local_amount, ...rest }: any, index: number) => {
        if (local_amount >= remaning) {
          local_amount -= remaning;
          remaning = 0;
        } else {
          remaning -= local_amount;
          local_amount = 0;
        }

        return { ...rest, local_amount };
      });

    setValue(ins.reverse());

    message.loading("Generating Installments ...");
  } catch (error) {
    if (error) {
      message.warning("Please select the method first");
    }
  }
};
