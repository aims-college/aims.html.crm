export const checkURL = (url: string) => {
  return url.match(/\.(jpeg|jpg|gif|PNG|png)$/) != null;
};
