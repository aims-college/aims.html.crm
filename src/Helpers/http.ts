import axios, {
  AxiosInstance,
  AxiosResponse,
  AxiosError,
  AxiosPromise,
  AxiosInterceptorManager,
} from "axios";

const errorInterceptor = (error: AxiosError) => {
  switch (error?.response?.status) {
    case 400:
      break;

    case 401:
      break;

    case 404:
      break;

    default:
  }
  return Promise.reject({ httpCode: error?.response?.status, error });
};

const responseInterceptor = (response: AxiosResponse) => {
  switch (response.status) {
    case 200:
      break;
    default:
  }

  return response;
};

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*"
  },
  transformResponse: [
    function (data) {
      let response;
      try {
        response = JSON.parse(data);
      } catch (error) {
        // throw error;
      }
      return response;
    },
  ],
});

axiosInstance.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  config.headers.Authorization = token ? `Bearer ${token}` : "";

  try {
  } catch (error) {
    // throw error
  }

  return config;
});

axiosInstance.interceptors.response.use(responseInterceptor, errorInterceptor);

const AzureHttp = {
  post: function <D extends {} | Array<{}>>(
    endpoint: string,
    data: D
  ): AxiosPromise {
    return axiosInstance.post(`/${endpoint}`, data);
  },
  put: function <D extends {} | Array<{}>>(
    service: string,
    endpoint: string,
    data: D
  ): AxiosPromise {
    return axiosInstance.put(`/${endpoint}`, data);
  },
  putById: function (
    service: string,
    endpoint: string,
    id: number | string
  ): AxiosPromise {
    return axiosInstance.put(`/${endpoint}/${id}`);
  },
  get: function <D extends {} | Array<{}>>(
    endpoint: string,
    data: D
  ): AxiosPromise {
    return axiosInstance.get(`/${endpoint}`, {
      params: data,
    });
  },
  getById: function (endpoint: string, id: number | string): AxiosPromise {
    return axiosInstance.get(`/${endpoint}/${id}`);
  },
  getByType: function (
    service: string,
    endpoint1: string,
    id: number | string,
    endpoint2: string,
    type: number | string,
    subType: number | string
  ): AxiosPromise {
    return axiosInstance.get(`/${endpoint2}/${type}/${subType}`);
  },
  delete: function (
    service: string,
    endpoint: string,
    id: number | string
  ): AxiosPromise {
    return axiosInstance.delete(`/${endpoint}/${id}`);
  },
  patch: function <D extends {} | Array<{}>>(
    service: string,
    endpoint: string,
    data: D
  ): AxiosPromise {
    return axiosInstance.patch(`/${endpoint}`, data);
  },
};

export default AzureHttp;
