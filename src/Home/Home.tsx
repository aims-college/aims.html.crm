import { Button, Col, Input, Row } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import React, { useEffect } from "react";
import "./Home.scss";
import { connect } from "react-redux";
import { DownOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import { $Select, $Input, $DatePicker } from "Components/antd";
import { Form, Formik, FormikContextType } from "formik";
var logo = require("./logo.png");
var threeDots = require("./3Dots.png");

const Home = ({ postlogin, history }: any) => {
  return (
    <>
      <div className="parent clearfix home-bg">
        <div className="welcome-bg">
          <img src={logo} className="welcome-logo" />
          <div className="welcome-title">
            <span>Customer Relationship</span>
            <span>Management</span>
          </div>
          <img src={threeDots} className="welcome-dots" />
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
