import React, { useState } from "react";
import { Affix, Avatar, Badge, Layout, Menu, Tag } from "antd";
import { NewInquery } from "../Components/Inquery/NewInquery";
import "./Layout.scss";
import {
  ContactsOutlined,
  ContainerOutlined,
  DollarCircleOutlined,
  FileAddOutlined,
  OrderedListOutlined,
  PieChartOutlined,
  PlusOutlined,
  SubnodeOutlined,
  UserAddOutlined,
  UserOutlined,
  LogoutOutlined,
  EnterOutlined,
  AimOutlined,
  BookOutlined,
  AccountBookFilled
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import packageJson from "../../package.json";

const { Header, Content, Footer, Sider } = Layout;
const LayoutMain: React.FC<any> = (props) => {
  const { SubMenu } = Menu;
  return (
    <>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider defaultCollapsed={true}>
          <Affix offsetTop={0}>
            <div
              className="d-flex align-items-start flex-column"
              style={{ minHeight: "100vh" }}
            >
              <div className="mb-auto">
                <div className="logo">
                  <svg
                    style={{ maxHeight: "16px" }}
                    viewBox="0 0 100 48"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill="#FFFFFF"
                      d="M22.6,38.4c-0.1-0.1-0.1-0.2-0.2-0.3l-0.6-3.7c-0.1-0.1-0.1-0.2-0.2-0.2h-9.3c-0.1,0-0.2,0.1-0.2,0.2l-1.5,3.7
    									c-0.1,0.3-0.4,0.4-0.6,0.4H3.7c-0.2,0-0.3,0-0.4-0.1c-0.1-0.1-0.1-0.2,0-0.4L15.6,9.6c0.1-0.3,0.3-0.4,0.6-0.4h7.7
    									c0.3,0,0.4,0.1,0.5,0.4L29.6,38v0.2c0,0.3-0.2,0.4-0.5,0.4h-6.3C22.8,38.6,22.7,38.5,22.6,38.4z M14.6,28.9h6.1
    									c0.1,0,0.2-0.1,0.2-0.3l-1.8-10.8c0-0.1-0.1-0.2-0.1-0.1c-0.1,0-0.1,0.1-0.1,0.1l-4.4,10.8C14.4,28.8,14.4,28.9,14.6,28.9z
    									 M32.7,38.4c-0.1-0.1-0.1-0.2-0.1-0.4l3.5-28.4c0-0.1,0.1-0.3,0.2-0.4c0.1-0.1,0.2-0.1,0.4-0.1h5.8c0.3,0,0.5,0.2,0.5,0.5l-3.5,28.4
    									c0,0.1-0.1,0.3-0.2,0.4c-0.1,0.1-0.2,0.1-0.4,0.1h-5.8C32.9,38.6,32.8,38.5,32.7,38.4z M65.9,9.1h5.8c0.1,0,0.3,0,0.3,0.1
    									C72,9.4,72.1,9.5,72,9.6l-3.5,28.4c0,0.1-0.1,0.3-0.2,0.4c-0.1,0.1-0.2,0.1-0.4,0.1h-5.8c-0.1,0-0.3,0-0.3-0.1
    									c-0.1-0.1-0.1-0.2-0.1-0.4l2.1-16.9c0-0.1,0-0.2,0-0.2c-0.1,0-0.1,0-0.2,0.1l-4.2,5.8c-0.1,0.2-0.3,0.3-0.6,0.3h-2.9
    									c-0.3,0-0.4-0.1-0.5-0.3l-2.8-5.8c0-0.1-0.1-0.1-0.1-0.1c-0.1,0-0.1,0.1-0.1,0.2l-2.1,16.9c0,0.1-0.1,0.3-0.2,0.4
    									c-0.1,0.1-0.2,0.1-0.4,0.1h-5.8c-0.1,0-0.3,0-0.3-0.1c-0.1-0.1-0.1-0.2-0.1-0.4l3.5-28.4c0-0.1,0.1-0.3,0.2-0.4
    									c0.1-0.1,0.2-0.1,0.4-0.1h5.7c0.3,0,0.5,0.1,0.5,0.3l4.5,9.2c0.1,0.2,0.2,0.2,0.3,0l6.7-9.2C65.4,9.3,65.6,9.1,65.9,9.1z M75.4,36.8
    									c-1.8-1.4-2.7-3.3-2.7-5.8c0-0.3,0-0.7,0.1-1.2l0.1-0.8c0-0.1,0.1-0.3,0.2-0.4c0.1-0.1,0.2-0.1,0.4-0.1h5.7c0.1,0,0.3,0,0.3,0.1
    									c0.1,0.1,0.1,0.2,0.1,0.4l-0.1,0.5c-0.1,0.9,0.2,1.7,1.1,2.4c0.8,0.7,2,1,3.5,1c1.3,0,2.2-0.3,2.9-0.9c0.6-0.6,0.9-1.3,0.9-2
    									c0-0.7-0.3-1.2-0.9-1.6c-0.6-0.4-1.7-0.8-3.2-1.4l-1-0.3c-1.6-0.6-2.9-1.1-4.1-1.8c-1.1-0.6-2.1-1.5-3-2.5c-0.8-1.1-1.2-2.4-1.2-3.9
    									c0-0.2,0-0.6,0.1-1.1c0.2-1.7,0.9-3.3,1.9-4.6c1.1-1.3,2.4-2.3,4.1-3c1.7-0.7,3.5-1.1,5.5-1.1c1.9,0,3.6,0.3,5.1,1
    									c1.5,0.7,2.7,1.6,3.5,2.9c0.8,1.2,1.3,2.7,1.3,4.3c0,0.3,0,0.7-0.1,1.2l-0.1,0.6c0,0.1-0.1,0.3-0.2,0.4c-0.1,0.1-0.2,0.1-0.4,0.1
    									h-5.6c-0.1,0-0.3,0-0.4-0.1c-0.1-0.1-0.1-0.2-0.1-0.4l0-0.3c0.1-1-0.2-1.8-0.9-2.6c-0.7-0.8-1.8-1.1-3.2-1.1c-1.1,0-2.1,0.3-2.7,0.8
    									c-0.7,0.5-1,1.2-1,2.2c0,0.5,0.2,0.9,0.5,1.3c0.4,0.4,0.9,0.7,1.6,1c0.7,0.3,1.8,0.8,3.4,1.3c1.7,0.6,3,1.1,4,1.6
    									c0.9,0.4,1.8,1.2,2.6,2.2c0.8,1,1.2,2.3,1.2,3.8c0,0.2,0,0.6-0.1,1.1c-0.3,2.7-1.6,4.9-3.7,6.5c-2.1,1.6-4.8,2.4-8.1,2.4
    									C79.7,38.9,77.2,38.2,75.4,36.8z"
                    />
                  </svg>
                  <div>
                    <div className="p-1 text-muted font-sm">
                      {packageJson.version}
                    </div>
                  </div>
                </div>

                <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
                  <SubMenu title="Inquiry" icon={<ContainerOutlined />}>
                    <Menu.Item icon={<ContainerOutlined />}>
                      <Link to="/inqueries">Leads</Link>
                    </Menu.Item>
                    <Menu.Item icon={<FileAddOutlined />}>
                      <Link to="/inqueries/create">New Lead</Link>
                    </Menu.Item>
                    <Menu.Item icon={<EnterOutlined />}>
                      <Link to="/inqueries/transfer">Transfer</Link>
                    </Menu.Item>
                  </SubMenu>
                  <SubMenu title="Registrations" icon={<ContactsOutlined />}>
                    <Menu.Item icon={<ContactsOutlined />}>
                      <Link to="/registrations">Registrations</Link>
                    </Menu.Item>
                    <Menu.Item icon={<PieChartOutlined />}>
                      <Link to="/registrations-export">
                        Registrations Export
                      </Link>
                    </Menu.Item>
                    <Menu.Item icon={<PieChartOutlined />}>
                      <Link to="/installments-export">
                        Installments Export
                      </Link>
                    </Menu.Item>
                    
                    <Menu.Item icon={<BookOutlined />}>
                      <Link to="/discount-approval">Discount Approval</Link>
                    </Menu.Item>
                  </SubMenu>
                  <SubMenu title="Payments" icon={<DollarCircleOutlined />}>
                    <Menu.Item icon={<DollarCircleOutlined />}>
                      <Link to="/payments">Payments</Link>
                    </Menu.Item>
                    <Menu.Item icon={<SubnodeOutlined />}>
                      <Link to="/payments/new">New Payment</Link>
                    </Menu.Item>
                    <Menu.Item icon={<PieChartOutlined />}>
                      <Link to="/payments-export">Payments Export</Link>
                    </Menu.Item>
                  </SubMenu>
                  <SubMenu title="Courses" icon={<OrderedListOutlined />}>
                    <Menu.Item icon={<OrderedListOutlined />}>
                      <Link to="/courses">Add Courses</Link>
                    </Menu.Item>
                  </SubMenu>
                  <SubMenu title="Discounts" icon={<AimOutlined />}>
                    <Menu.Item icon={<AimOutlined />}>
                      <Link to="/discounts">Add Discounts</Link>
                    </Menu.Item>
                  </SubMenu>
                  <SubMenu title="Reports" icon={<AccountBookFilled />}>
                    <Menu.Item icon={<PieChartOutlined />}>
                      <Link to="/registrations/reports">
                        Registrations Reports
                      </Link>
                    </Menu.Item>
                    <Menu.Item icon={<PieChartOutlined />}>
                      <Link to="/inqueries/reports">Leads Reports</Link>
                    </Menu.Item>
                    <Menu.Item icon={<PieChartOutlined />}>
                      <Link to="/payments/outstanding">Outstanding Reports</Link>
                    </Menu.Item>
                  </SubMenu>
                </Menu>
              </div>
              <div className="mb-4">
                <Menu theme="dark" mode="inline">
                  <Menu.Item icon={<LogoutOutlined />}>
                    <Link to="/">Logout</Link>
                  </Menu.Item>
                  <Menu.Item
                    icon={
                      <Avatar
                        style={{ marginLeft: "-4px" }}
                        icon={<UserOutlined />}
                        shape="square"
                        size="small"
                      />
                    }
                  >
                    <Link to="/">{localStorage.getItem("user_name")}</Link>
                  </Menu.Item>
                </Menu>
              </div>
            </div>
          </Affix>
        </Sider>

        <Content>
          <div className="content"> {props.children}</div>
        </Content>
      </Layout>
    </>
  );
};

export default LayoutMain;
