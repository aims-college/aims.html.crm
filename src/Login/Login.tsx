import { Button, Col, Input, Row } from "antd";
import {
  EyeInvisibleOutlined,
  EyeTwoTone,
  UserOutlined,
} from "@ant-design/icons";
import React from "react";
import "./Login.scss";
import configureStore, { history } from "store";
import { connect } from "react-redux";
import {
  DownOutlined,
  PhoneOutlined,
  MailOutlined,
  MailFilled,
  MailTwoTone,
  LockFilled,
} from "@ant-design/icons";
import * as Actions from "Actions";
import { $Select, $Input, $DatePicker } from "Components/antd";
import { Form, Formik, FormikContextType } from "formik";
import Validations from "./Validations";
var logo = require("./logo.png");

const Login = ({ postlogin, history }: any) => {
  return (
    <>
      <div className="parent clearfix login-bg">
        <div className="bg-illustration">
          <div className="login-word">
            <img src={logo} className="login-logo" />
            <div className="login-text">
              <span>Education is the ticket to</span>
              <span>success</span>
              <span className='login-text-from'>- Jaime Escalante -</span>
            </div>
          </div>
        </div>

        <div className="login">
          <div className="container">
            <h1 className="m-0 mt-5 pt-5">Welcome</h1>
            <p className="text-muted">Login to access to your account</p>

            <div className="login-form">
              <Formik
                initialValues={{ email: "", password: "" }}
                validationSchema={Validations}
                enableReinitialize
                validateOnBlur
                validateOnMount
                validateOnChange
                onSubmit={(values) => {
                  postlogin(values, history);
                }}
              >
                {({ values, handleSubmit, dirty }) => (
                  <Form
                    className="ant-form ant-form-vertical"
                    onSubmit={handleSubmit}
                  >
                    <div className="login-inputs">
                      <div className="row pt-6 mb-4">
                        <$Input
                          name="email"
                          prefix={<MailFilled />}
                          size="large"
                          placeholder="Email Address"
                          style={{ borderRadius: "25px" }}
                          required
                        />
                      </div>
                      <div className="row ">
                        <$Input
                          placeholder="Password"
                          style={{ borderRadius: "25px" }}
                          type="password"
                          required
                          size="large"
                          name="password"
                          prefix={<LockFilled />}
                        />
                      </div>

                      <div className="row mb-3 text-align">
                        {" "}
                        <Button
                          type="primary"
                          className="mr-2 mt-4 login-button"
                          size="large"
                          disabled={!dirty}
                          shape="round"
                          onClick={(e: any) => handleSubmit(e)}
                        >
                          Login in
                        </Button>{" "}
                      </div>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {};
};

const mapDispatchToProps = {
  postlogin: Actions.user.login.post,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
