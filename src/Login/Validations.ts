import * as Yup from "yup";

export default () => {
  return Yup.object().shape({
    email: Yup.string().email().nullable().required("email is required"),
    password: Yup.string().required("password is required"),
  });
};
