import { Button, Col, Input, List, Row } from "antd";
import {
  DoubleRightOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
  UserOutlined,
} from "@ant-design/icons";
import React from "react";
import "./MenuTile.scss";
import configureStore, { history } from "store";
import { connect } from "react-redux";
import {
  DownOutlined,
  PhoneOutlined,
  MailOutlined,
  MailFilled,
  MailTwoTone,
  LockFilled,
} from "@ant-design/icons";
import * as Actions from "Actions";
import { $Select, $Input, $DatePicker } from "Components/antd";
import { Form, Formik, FormikContextType } from "formik";
import { Link } from "react-router-dom";

var logo = require("./logo.png");

const MenuTile = ({ postlogin, history }: any) => {
  return (
    <>
      <div className="parent clearfix menu-bg">
        <div className="bg-illustration">
          <div className="login-word">
            <img src={logo} className="login-logo" />
            <div className="login-text">
              <span>Education is the most powerful</span>
              <span>weapon which you can use</span>
              <span>to change the world</span>
              <span className="login-text-from">- Nelson Mandela -</span>
            </div>
          </div>
        </div>

        <div className="menu">
          <div className="container">
            <h1 className="m-0 mt-5 pt-5"></h1>
            <p className="text-muted"></p>
            <div className="menu-tile-list">
              <ul>
                <li>
                  <Link to="/inqueries">CRM <DoubleRightOutlined /></Link>
                </li>
                <li>
                  <Link to="/registrations">Registrations <DoubleRightOutlined /></Link>
                </li>
                <li>
                  <Link to="/payments">Fees <DoubleRightOutlined /></Link>
                </li>
                <li>
                  <Link to="/inqueries/reports">Reports <DoubleRightOutlined /></Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {};
};

const mapDispatchToProps = {
  postlogin: Actions.user.login.post,
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuTile);
