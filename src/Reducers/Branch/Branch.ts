import { initialState } from "./State";

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "GET_BRANCH_DATA":
      return Object.assign({}, state, {
        branches: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_BRANCH_DATA_SUCCESS":
      return Object.assign({}, state, {
        branches: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_BRANCH_DATA_FAIL":
      return Object.assign({}, state, {
        branches: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    default:
      return state;
  }
};
