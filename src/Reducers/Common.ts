export const initialState = {
  date: {
    format: "YYYY-MM-DD",
  },
};

export default (state = initialState, action: any) => {
  switch (action.type) {
    default:
      return state;
  }
};
