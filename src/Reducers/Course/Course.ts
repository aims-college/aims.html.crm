import { initialState } from "./State";

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "GET_COURSE_DATA":
      return Object.assign({}, state, {
        courses: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_COURSE_DATA_SUCCESS":
      return Object.assign({}, state, {
        courses: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_COURSE_DATA_FAIL":
      return Object.assign({}, state, {
        courses: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    case "GET_COURSE_TYPE_DATA":
      return Object.assign({}, state, {
        type: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_COURSE_TYPE_DATA_SUCCESS":
      return Object.assign({}, state, {
        type: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_COURSE_TYPE_DATA_FAIL":
      return Object.assign({}, state, {
        type: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    case "GET_FACULTY_DATA":
      return Object.assign({}, state, {
        faculties: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_FACULTY_DATA_SUCCESS":
      return Object.assign({}, state, {
        faculties: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_FACULTY_DATA_FAIL":
      return Object.assign({}, state, {
        faculties: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    default:
      return state;
  }
};
