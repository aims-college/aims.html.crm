
export const initialState = {
  faculties: {
    isLoading: true,
    data: [],
    isError: false,
  },
  courses: {
    isLoading: true,
    data: [],
    isError: false,
  },
  type:{
    isLoading: true,
    data: [],
    isError: false,
  }
};
