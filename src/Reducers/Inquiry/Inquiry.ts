import { initialState } from "./State";

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "GET_INQUIRY_DATA":
      return Object.assign({}, state, {
        inquiries: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_INQUIRY_DATA_SUCCESS":
      return Object.assign({}, state, {
        inquiries: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_INQUIRY_DATA_FAIL":
      return Object.assign({}, state, {
        inquiries: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    case "GET_INQUIRY_TYPE_DATA":
      return Object.assign({}, state, {
        type: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_INQUIRY_TYPE_DATA_SUCCESS":
      return Object.assign({}, state, {
        type: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_INQUIRY_TYPE_DATA_FAIL":
      return Object.assign({}, state, {
        type: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });

    case "EDIT_INQUIRY_DATA":
      return Object.assign({}, state, {
        edit: {
          isLoading: true,
          data: initialState.edit.data,
          isError: false,
        },
      });
    case "EDIT_INQUIRY_DATA_SUCCESS":
      return Object.assign({}, state, {
        edit: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });
    case "EDIT_INQUIRY_DATA_FAIL":
      return Object.assign({}, state, {
        edit: {
          isLoading: false,
          data: {},
          isError: true,
        },
      });
    case "UPDATE_INQUIRY":
      return Object.assign({}, state, {
        edit: {
          isLoading: true,
          data: {},
          isError: false,
        },
      });
    case "UPDATE_INQUIRY_SUCCESS":
      return Object.assign({}, state, {
        edit: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });
    case "UPDATE_INQUIRY_FAIL":
      return Object.assign({}, state, {
        edit: {
          isLoading: false,
          data: {},
          isError: true,
        },
      });

    case "CREATE_INQUIRY":
      return Object.assign({}, state, {
        edit: {
          isLoading: true,
          data: {},
          isError: false,
        },
      });
    case "CREATE_INQUIRY_SUCCESS":
      return Object.assign({}, state, {
        edit: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });
    case "CREATE_INQUIRY_FAIL":
      return Object.assign({}, state, {
        edit: {
          isLoading: false,
          data: {},
          isError: true,
        },
      });

    case "GET_TRANSFER_DATA":
      return Object.assign({}, state, {
        transfer: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_TRANSFER_SUCCESS":
      return Object.assign({}, state, {
        transfer: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });
    case "GET_TRANSFER_FAIL":
      return Object.assign({}, state, {
        transfer: {
          isLoading: false,
          data: {},
          isError: true,
        },
      });

    case "GET_REPORTS_DATA":
      return Object.assign({}, state, {
        reports: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_REPORTS_DATA_SUCCESS":
      return Object.assign({}, state, {
        reports: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });
    case "GET_REPORTS_DATA_FAIL":
      return Object.assign({}, state, {
        reports: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });

    default:
      return state;
  }
};
