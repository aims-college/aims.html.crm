export const initialState = {
  inquiries: {
    isLoading: true,
    data: [],
    isError: false,
  },
  reports: {
    isLoading: true,
    data: [],
    isError: false,
  },
  transfer: {
    isLoading: true,
    data: [],
    isError: false,
  },
  type: {
    isLoading: true,
    data: [],
    isError: false,
  },
  edit: {
    isLoading: true,
    data: {
      id: -1,
      inquiry_type_id: 0,
      inquirer_type: "",
      inquirer_name: "",
      assignToId: -1,
      nic: "",
      email: "",
      contact_no: "",
      course_id: "",
      inquiry_date: "",
      postponed_date: "",
      status: "",
      created_at: "",
      updated_at: "",
      inquiry_followups: [],
      inquiry_references: []
    },
    isError: false,
  },
  new: {
    isLoading: true,
    data: {
      id: -1,
      inquiry_type_id: 1,
      inquirer_type: "",
      inquirer_name: "",
      nic: "",
      email: "",
      assignToId: -1,
      contact_no: "",
      course_id: "",
      inquiry_date: "",
      status: "",
      created_at: "",
      updated_at: "",
      inquiry_followups: [{
        comment: null,
        isNew: false,
        postponed_date: null
      }],
      inquiry_references: []
    },
    isError: false,
  },
};
