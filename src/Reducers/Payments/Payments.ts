import { initialState } from "./State";

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "GET_PAYMENTS":
      return Object.assign({}, state, {
        payments: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_PAYMENTS_SUCCESS":
      return Object.assign({}, state, {
        payments: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_PAYMENTS_FAIL":
      return Object.assign({}, state, {
        payments: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });

    case "GET_ALL_CURRENCIES":
      return Object.assign({}, state, {
        currencies: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_ALL_CURRENCIES_SUCCESS":
      return Object.assign({}, state, {
        currencies: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_ALL_CURRENCIES_FAIL":
      return Object.assign({}, state, {
        currencies: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });

    case "GET_BUSY_PAYMENTS":
      return Object.assign({}, state, {
        busy: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_BUSY_PAYMENTS_SUCCESS":
      return Object.assign({}, state, {
        busy: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_BUSY_PAYMENTS_FAIL":
      return Object.assign({}, state, {
        busy: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });

    case "GET_BANK_ACCOUNTS":
      return Object.assign({}, state, {
        bankAccounts: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_BANK_ACCOUNTS_SUCCESS":
      return Object.assign({}, state, {
        bankAccounts: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_BANK_ACCOUNTS_FAIL":
      return Object.assign({}, state, {
        bankAccounts: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    case "GET_OUTSTANDING":
      return Object.assign({}, state, {
        outstanding: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_OUTSTANDING_SUCCESS":
      return Object.assign({}, state, {
        outstanding: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_OUTSTANDING_FAIL":
      return Object.assign({}, state, {
        outstanding: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });

      case "GET_OUTSTANDING_STUDENT":
        return Object.assign({}, state, {
          outstandingStudent: {
            isLoading: true,
            data: [],
            isError: false,
          },
        });
      case "GET_OUTSTANDING_STUDENT_SUCCESS":
        return Object.assign({}, state, {
          outstandingStudent: {
            isLoading: false,
            data: action.payload.data,
            isError: false,
          },
        });
  
      case "GET_OUTSTANDING_STUDENT_FAIL":
        return Object.assign({}, state, {
          outstandingStudent: {
            isLoading: false,
            data: [],
            isError: true,
          },
        });
    case "GET_PAYMENTS_BY_REG_ID":
      return Object.assign({}, state, {
        exising: {
          ...initialState.exising,
          isLoading: true,
          isError: false,
        },
        studentPayment: {
          ...initialState.studentPayment,
        },
      });
    case "GET_PAYMENTS_BY_REG_ID_SUCCESS":
      return Object.assign({}, state, {
        exising: {
          ...initialState.exising,
          payments: action.payload.data.payments,
          installments: action.payload.data.course_installments,
          isLoading: false,
          isError: false,
          course_fee: action.payload.data.course_fee,
          discount_amount: action.payload.data.discount_amount,
          branch_name: action.payload.data.branch_name,
          course_name: action.payload.data.course_name,
          student_name: action.payload.data.student_name,
        },
      });

    case "GET_PAYMENTS_BY_REG_ID_FAIL":
      return Object.assign({}, state, {
        exising: {
          ...initialState.exising,
          isLoading: false,
          isError: true,
        },
      });

    case "SAVE_PAYMENT":
      return Object.assign({}, state, {
        studentPayment: {
          ...initialState.studentPayment,
          isLoading: true,
          isError: false,
        },
      });
    case "SAVE_PAYMENT_SUCCESS":
      const getPaymentRecordByType = (type: number) => {
        try {
          const {
            reg_no,
            payment_method,
            amount,
            reference_url,
            payment_date,
            receipt_no,
            student_code,
            student_name,
            description,
            received_user_name,
            payment_currency,
          } = action.payload.data.filter(
            (payment: any) => payment.payment_type == type
          )[0];

          return {
            reg_no,
            payment_method,
            amount,
            reference_url,
            payment_date,
            receipt_no,
            student_code,
            student_name,
            description,
            received_user_name,
            payment_currency,
          };
        } catch (error) {
          return {
            reg_no: "",
            payment_method: "",
            amount: 0,
            reference_url: "",
            payment_date: "",
            receipt_no: "",
            student_code: "",
            student_name: "",
            description: "",
            received_user_name: "",
            payment_currency: "",
          };
        }
      };

      return Object.assign({}, state, {
        studentPayment: {
          ...initialState.studentPayment,

          localPaymentMethod: getPaymentRecordByType(1).payment_method,
          uniPaymentMethod: getPaymentRecordByType(2).payment_method,
          otherPaymentMethod: getPaymentRecordByType(3).payment_method,

          localRef: {
            name: getPaymentRecordByType(1).reference_url,
            url: getPaymentRecordByType(1).reference_url,
          },
          uniRef: {
            name: getPaymentRecordByType(2).reference_url,
            url: getPaymentRecordByType(2).reference_url,
          },
          otherRef: {
            name: getPaymentRecordByType(3).reference_url,
            url: getPaymentRecordByType(3).reference_url,
          },
          localAmount: getPaymentRecordByType(1).amount,
          uniAmount: getPaymentRecordByType(2).amount,
          otherAmount: getPaymentRecordByType(3).amount,
          localPaidCurrency: getPaymentRecordByType(1).payment_currency,
          uniPaidCurrency: getPaymentRecordByType(2).payment_currency,
          otherPaidCurrency: getPaymentRecordByType(3).payment_currency,
          localReceiptNo: getPaymentRecordByType(1).receipt_no,
          uniReceiptNo: getPaymentRecordByType(2).receipt_no,
          otherReceiptNo: getPaymentRecordByType(3).receipt_no,
          localPaymentDate: getPaymentRecordByType(1).payment_date,
          uniPaymentDate: getPaymentRecordByType(2).payment_date,
          otherPaymentDate: getPaymentRecordByType(3).payment_date,
          localDescription: getPaymentRecordByType(1).description,
          uniDescription: getPaymentRecordByType(2).description,
          otherDescription: getPaymentRecordByType(3).description,
          studentName:
            getPaymentRecordByType(1).student_name ||
            getPaymentRecordByType(2).student_name ||
            getPaymentRecordByType(3).student_name,
          studentCode:
            getPaymentRecordByType(1).student_code ||
            getPaymentRecordByType(2).student_code ||
            getPaymentRecordByType(3).student_code,
          received_user_name:
            getPaymentRecordByType(1).received_user_name ||
            getPaymentRecordByType(2).received_user_name ||
            getPaymentRecordByType(3).received_user_name,
          registrationNo:
            getPaymentRecordByType(1).reg_no ||
            getPaymentRecordByType(2).reg_no ||
            getPaymentRecordByType(3).reg_no,
        },
        printDrawer: {
          isVisible: true,
        },
      });

    case "SAVE_PAYMENT_FAIL":
      return Object.assign({}, state, {
        studentPayment: {
          ...initialState.studentPayment,
          isLoading: false,
          isError: true,
        },
      });

    case "PRINT_DRAWER_CLOSE":
      return Object.assign({}, state, {
        printDrawer: {
          isVisible: false,
        },
        studentPayment: {
          ...initialState.studentPayment,
          isLoading: false,
          isError: false,
        },
        exising: {
          ...initialState.exising,
          isLoading: false,
          isError: false,
        },
      });
    default:
      return state;
  }
};
