import { initialState } from "./State";

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "GET_REGISTRATION_REPORTS_DATA":
      return Object.assign({}, state, {
        reports: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_REGISTRATION_REPORTS_DATA_SUCCESS":
      return Object.assign({}, state, {
        reports: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });
    case "GET_REGISTRATION_REPORTS_DATA_FAIL":
      return Object.assign({}, state, {
        reports: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    case "GET_INQUIRY_BY_ID":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          isLoading: true,
        },
      });
    case "GET_INQUIRY_BY_ID_SUCCESS":
      return Object.assign({}, state, {
        student: {
          isLoading: false,
          data: {
            ...initialState.student.data,
            fName: action.payload.data.inquirer_name.split(" ")[0],
            lName: action.payload.data.inquirer_name.split(" ")[1],
            nic: action.payload.data.nic,
            primaryContact: action.payload.data.contact_no,
            course_name: action.payload.data.course_name,
            secondaryContact: action.payload.data.contact_no_2,
            course_id: action.payload.data.course_id,
            email: action.payload.data.email,
            courseCode: action.payload.data.course_code,
            courseFee: action.payload.data.course_fee,
            courseRegistrationFee: action.payload.data.course_registration_fee,
            courseDuration: action.payload.data.course_duration,
            isError: false,
          },
        },
      });

    case "GET_INQUIRY_BY_ID_FAIL":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          isError: true,
          isLoading: false,
        },
      });

    case "GET_DISCOUNTS_DATA":
      return Object.assign({}, state, {
        discounts: {
          ...initialState.discounts,
          isLoading: true,
        },
      });
    case "GET_DISCOUNTS_DATA_SUCCESS":
      return Object.assign({}, state, {
        discounts: {
          isLoading: false,
          data: action.payload.data,
          isError: false,
        },
      });

    case "GET_DISCOUNTS_DATA_FAIL":
      return Object.assign({}, state, {
        discounts: {
          ...initialState.discounts,
          isError: true,
          isLoading: false,
        },
      });
    case "GET_REGISTRATION_NUMBER_DATA":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          isLoading: true,
        },
      });
    case "GET_REGISTRATION_NUMBER_DATA_SUCCESS":
      return Object.assign({}, state, {
        student: {
          ...state.student,
          registrationNo: action.payload.data,
          isLoading: false,
        },
      });

    case "GET_REGISTRATION_NUMBER_DATA_FAIL":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          isLoading: false,
        },
      });

    case "GET_REGISTRATIONS":
      return Object.assign({}, state, {
        all: {
          ...initialState.all,
          isLoading: true,
        },
      });
    case "GET_REGISTRATIONS_SUCCESS":
      return Object.assign({}, state, {
        all: {
          ...state.all,
          data: action.payload.data,
          isLoading: false,
        },
      });

    case "GET_REGISTRATIONS_FAIL":
      return Object.assign({}, state, {
        all: {
          ...initialState.all,
          isLoading: false,
        },
      });

      case "GET_BUSY_REGISTRATIONS":
        return Object.assign({}, state, {
          busy: {
            ...initialState.busy,
            isLoading: true,
          },
        });
      case "GET_BUSY_REGISTRATIONS_SUCCESS":
        return Object.assign({}, state, {
          busy: {
            ...state.all,
            data: action.payload.data,
            isLoading: false,
          },
        });
  
      case "GET_BUSY_REGISTRATIONS_FAIL":
        return Object.assign({}, state, {
          busy: {
            ...initialState.busy,
            isLoading: false,
          },
        });

        case "GET_BUSY_INSTALLMENTS":
          return Object.assign({}, state, {
            busyInstallments: {
              ...initialState.busyInstallments,
              isLoading: true,
            },
          });
        case "GET_BUSY_INSTALLMENTS_SUCCESS":
          return Object.assign({}, state, {
            busyInstallments: {
              ...state.all,
              data: action.payload.data,
              isLoading: false,
            },
          });
    
        case "GET_BUSY_INSTALLMENTS_FAIL":
          return Object.assign({}, state, {
            busyInstallments: {
              ...initialState.busyInstallments,
              isLoading: false,
            },
          });

    case "SAVE_REGISTRATION":
      return Object.assign({}, state, {
        printDrawer: {
          isVisible: false,
        },
      });
    case "SAVE_REGISTRATION_SUCCESS":
      const getPaymentRecordByType = (type: number) => {
        try {
          const {
            reg_no,
            payment_method,
            amount,
            reference_url,
            payment_date,
            receipt_no,
            student_code,
            student_name,
            description,
            received_user_name,
            payment_currency,
          } = action.payload.data.payments.filter(
            (payment: any) => payment.payment_type == type
          )[0];

          return {
            reg_no,
            payment_method,
            amount,
            reference_url,
            payment_date,
            receipt_no,
            student_code,
            student_name,
            description,
            received_user_name,
            payment_currency,
          };
        } catch (error) {
          return {
            payment_currency: "",
            received_user_name: "",
            reg_no: "",
            payment_method: "",
            amount: 0,
            reference_url: "",
            payment_date: "",
            receipt_no: "",
            student_code: "",
            student_name: "",
            description: "",
          };
        }
      };

      return Object.assign({}, state, {
        student: {
          ...state.student,
          data: {
            ...initialState.student.data,
            registrationNo:
              action.payload.data.student.student_registration.reg_no,
            registerDate:
              action.payload.data.student.student_registration.register_date,

            uniPaymentMethod: getPaymentRecordByType(2).payment_method,
            localPaymentMethod: getPaymentRecordByType(1).payment_method,
            otherPaymentMethod: getPaymentRecordByType(3).payment_method,
            uniRef: {
              name: getPaymentRecordByType(2).reference_url,
              url: getPaymentRecordByType(2).reference_url,
            },
            localRef: {
              name: getPaymentRecordByType(1).reference_url,
              url: getPaymentRecordByType(1).reference_url,
            },
            otherRef: {
              name: getPaymentRecordByType(3).reference_url,
              url: getPaymentRecordByType(3).reference_url,
            },
            uniAmount: getPaymentRecordByType(2).amount,
            uniPaidCurrency: getPaymentRecordByType(2).payment_currency,
            localAmount: getPaymentRecordByType(1).amount,
            localPaidCurrency: getPaymentRecordByType(1).payment_currency,
            otherAmount: getPaymentRecordByType(3).amount,
            otherPaidCurrency: getPaymentRecordByType(3).payment_currency,
            uniReceiptNo: getPaymentRecordByType(2).receipt_no,
            localReceiptNo: getPaymentRecordByType(1).receipt_no,
            otherReceiptNo: getPaymentRecordByType(3).receipt_no,
            studentName:
              getPaymentRecordByType(1).student_name ||
              getPaymentRecordByType(2).student_name ||
              getPaymentRecordByType(3).student_name,
            studentCode:
              getPaymentRecordByType(1).student_code ||
              getPaymentRecordByType(2).student_code ||
              getPaymentRecordByType(3).student_code,
            localPaymentDate: getPaymentRecordByType(1).payment_date,
            uniPaymentDate: getPaymentRecordByType(2).payment_date,
            otherPaymentDate: getPaymentRecordByType(3).payment_date,
            uniDescription: getPaymentRecordByType(2).description,
            localDescription: getPaymentRecordByType(1).description,
            otherDescription: getPaymentRecordByType(3).description,
            received_user_name:
              getPaymentRecordByType(1).received_user_name ||
              getPaymentRecordByType(2).received_user_name ||
              getPaymentRecordByType(3).received_user_name,
          },
        },
        printDrawer: {
          isVisible: true,
        },
      });

    case "SAVE_REGISTRATION_FAIL":
      return Object.assign({}, state, {
        printDrawer: {
          isVisible: false,
        },
      });

    case "PRINT_DRAWER_CLOSE":
      return Object.assign({}, state, {
        printDrawer: {
          isVisible: false,
        },
      });

    case "GET_REGISTRATION_BY_ID":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          isLoading: true,
        },
      });
    case "GET_REGISTRATION_BY_ID_SUCCESS":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          data: {
            ...initialState.student,
            student_id: action.payload.data.id,
            student_registration_id:
              action.payload.data.student_registration.id,
            registrationNo: action.payload.data.student_registration.reg_no,
            registerDate:
              action.payload.data.student_registration.register_date,
            fName: action.payload.data.first_name,
            middleName: action.payload.data.middle_name,
            lName: action.payload.data.last_name,
            nic: action.payload.data.nic,
            studentName:
              action.payload.data.first_name +
              " " +
              action.payload.data.middle_name +
              " " +
              action.payload.data.last_name,
            birthdate: action.payload.data.dob,
            street: action.payload.data.current_address,
            city: action.payload.data.current_address_city,
            country: action.payload.data.current_address_country,
            countryOfCitizenship: action.payload.data.country_of_citizenship,
            nameInCertificate: action.payload.data.name_in_certificate,
            studentCode: action.payload.data.student_registration.student_code,
            gender: action.payload.data.gender,
            civilStatus: action.payload.data.civil_status,
            primaryContact: action.payload.data.contact_no_1,
            secondaryContact: action.payload.data.contact_no_2,
            course_id: action.payload.data.student_registration.course_id,
            email: action.payload.data.email,
            branch: Number(action.payload.data.student_registration.branch_id),
            courseCode: "",
            branchCode: "",
            educational: action.payload.data.student_education.map(
              (student_education: any) => ({
                ...student_education,
                edu_level: student_education.edu_level,
                inclusive_date: student_education.inclusive_date,
                school_name: student_education.school_name,
                degree: student_education.degree,
                major_field: student_education.major_field,
                url: student_education.ref_url,
                name: student_education.ref_url,
                is_new: 0,
                is_deleted: 0,
                is_updated: 1,
              })
            ),
            coursesTaken: action.payload.data.student_prerequisite_course.map(
              (student_prerequisite_course: any) => ({
                ...student_prerequisite_course,
                level: student_prerequisite_course.level,
                course_description:
                  student_prerequisite_course.course_description,
                school_name: student_prerequisite_course.school_name,
                credit_units: student_prerequisite_course.credit_units,
                grade: student_prerequisite_course.grade,
                year_taken: student_prerequisite_course.year_taken,
                url: student_prerequisite_course.ref_url,
                name: student_prerequisite_course.ref_url,
                is_new: 0,
                is_deleted: 0,
                is_updated: 1,
              })
            ),
            courseMedium: action.payload.data.student_registration.medium,
            workExperiance: action.payload.data.student_work_experience.map(
              (student_work_experience: any) => ({
                ...student_work_experience,
                position: student_work_experience.position,
                major_responsibility:
                  student_work_experience.major_responsibility,
                company_name: student_work_experience.company_name,
                start_date: student_work_experience.start_date,
                end_date: student_work_experience.end_date,
                gross_annual_compensation:
                  student_work_experience.gross_annual_compensation,
                url: student_work_experience.ref_url,
                name: student_work_experience.ref_url,
                is_new: 0,
                is_deleted: 0,
                is_updated: 1,
              })
            ),
            discountId: "",
            approvalId: "",
            courseFee: action.payload.data.student_registration.course_fee,
            course_name: action.payload.data.student_registration.course_name,
            courseRegistrationFee:
              action.payload.data.student_registration.registration_fee,
            discountAmount:
              action.payload.data.student_registration.discount_amount,
            batchCode: action.payload.data.student_registration.batch_code,
            inquiry_id: -1,
            courseDuration: 0,
            installments: action.payload.data.student_course_installment.map(
              (student_course_installment: any) => ({
                ...student_course_installment,
              })
            ),
            documents: action.payload.data.student_document_checklist.map(
              (student_document_checklist: any) => ({
                ...student_document_checklist,
                checked:
                  student_document_checklist.checked == "1" ? true : false,
                url: student_document_checklist.url,
                name: student_document_checklist.url,
              })
            ),
            method: action.payload.data.student_course_installment.length,
            uniPaymentMethod: "bank",
            localPaymentMethod: "cash",
            uniRef: { name: null, url: null },
            localRef: { name: null, url: null },
            uniAmount: 0,
            localAmount: 0,
            uniReceiptNo: "",
            localReceiptNo: "",
            localPaymentDate: "",
            uniPaymentDate: "",
          },
          isLoading: false,
        },
      });

    case "GET_REGISTRATION_BY_ID_FAIL":
      return Object.assign({}, state, {
        student: {
          ...initialState.student,
          isLoading: false,
        },
      });

    default:
      return state;
  }
};
