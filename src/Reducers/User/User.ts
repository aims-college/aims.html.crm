import { initialState } from "./State";

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "GET_USERS_DATA":
      return Object.assign({}, state, {
        users: {
          isLoading: true,
          data: [],
          isError: false,
        },
      });
    case "GET_USERS_DATA_SUCCESS":
      return Object.assign({}, state, {
        users: {
          isLoading: false,
          data: action.payload.data
            .map((data: any) => ({
              ...data,
              fullName: `${data.f_name} ${data.l_name} (${data.email})`,
            }))
            // .filter(
            //   ({user_role_name}: any) =>
            //     user_role_name == "front_officer" ||
            //     user_role_name == "front_office_supervisor"
            // ),
            ,
          isError: false,
        },
      });

    case "GET_USERS_DATA_FAIL":
      return Object.assign({}, state, {
        users: {
          isLoading: false,
          data: [],
          isError: true,
        },
      });
    default:
      return state;
  }
};
