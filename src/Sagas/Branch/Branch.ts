import { take, put, call, select, takeLatest, all } from "redux-saga/effects";
import * as Actions from "Actions";
import * as API from "Services";

export const BranchSagas = {
  self: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.branch.self.get, params);
        yield put(Actions.branch.self.success(data));
      } catch (error) {
        yield put(Actions.branch.self.fail(error));
      }
    },
  },
};

export default [takeLatest("GET_BRANCH_DATA", BranchSagas.self.get)];
