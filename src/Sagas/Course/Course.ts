import { take, put, call, select, takeLatest, all } from "redux-saga/effects";
import * as Actions from "Actions";
import * as API from "Services";
import { message } from "antd";

export const CourseSagas = {
  self: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.course.self.get, params);
        yield put(Actions.course.self.success(data));
      } catch (error) {
        yield put(Actions.course.self.fail(error));
      }
    },
  },
  type: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.inquery.type.get, 1);
        yield put(Actions.course.type.success(data));
      } catch (error) {
        yield put(Actions.course.type.fail(error));
      }
    },
  },
  faculty: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.course.faculty.get, {});
        yield put(Actions.course.faculty.success(data));
      } catch (error) {
        yield put(Actions.course.faculty.fail(error));
      }
    },
  },
  create: {
    post: function* (action: any) {
      try {
        message.loading("Course is being saving");
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.course.create.post, params);
        yield put(Actions.course.create.success(data));
        message.success("Course saved succesfully");
        window.location.pathname = '/courses'
      } catch (error) {
        yield put(Actions.course.create.fail(error));
        message.error("Course saved unsuccesfully");
      }
    },
  },
};

export default [
  takeLatest("GET_COURSE_DATA", CourseSagas.self.get),
  takeLatest("GET_COURSE_TYPE_DATA", CourseSagas.type.get),
  takeLatest("GET_FACULTY_DATA", CourseSagas.faculty.get),
  takeLatest("CREATE_COURSE", CourseSagas.create.post),
];
