import { take, put, call, select, takeLatest, all } from "redux-saga/effects";
import * as Actions from "Actions";
import * as API from "Services";
import { message } from "antd";
export const InquerySagas = {
  self: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.inquery.self.get, params);

        yield put(Actions.inquery.self.success(data));
      } catch (error) {
        yield put(Actions.inquery.self.fail(error));
        message.error({
          content: "Something wrong",
          key: "1",
          duration: 2,
        });
      }
    },
    transfer: {
      get: function* (action: any) {
        try {
          const params = action?.payload?.data ?? {};
          const { data } = yield call(API.inquery.transfer.get, params);
          yield put(Actions.inquery.transfer.success(data));
        } catch (error) {
          yield put(Actions.inquery.transfer.fail(error));
          message.error({
            content: "Something wrong",
            key: "2",
            duration: 2,
          });
        }
      },
      approve: function* (action: any) {
        try {
          const params = action?.payload?.data ?? {};
          const { data } = yield call(API.inquery.approve.post, params);
          yield put(Actions.inquery.approve.success(data));
          yield put(Actions.inquery.transfer.get(null));
        } catch (error) {
          yield put(Actions.inquery.approve.fail(error));
          message.error({
            content: "Something wrong",
            key: "3",
            duration: 2,
          });
        }
      },
    },
    update: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        message.loading({ content: "Updating...", key: "inquery_update" });
        const { data, response } = yield call(API.inquery.self.update, params);
        yield put(Actions.inquery.update.success(data));
        response == "success" &&
          message.success({
            content: "Inquery Successfully Updated!",
            key: "inquery_update",
            duration: 2,
          });
        if (response == "success") {
          window.location.pathname = "/inqueries";
        }
      } catch (error) {
        yield put(Actions.inquery.update.fail(error));
        message.warning({
          content: "Something wrong",
          key: "inquery_update",
          duration: 2,
        });
      }
    },
    create: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        message.loading({ content: "saving...", key: "inquery_create" });
        const { data, response } = yield call(API.inquery.self.create, params);
        yield put(Actions.inquery.create.success(data));
        response == "success" &&
          message.success({
            content: "Inquery Successfully Created!",
            key: "inquery_create",
            duration: 2,
          });
        if (response == "success") {
          window.location.pathname = "/inqueries";
        }
      } catch (error) {
        yield put(Actions.inquery.create.fail(error));
        message.error({
          content: "Something wrong",
          key: "4",
          duration: 2,
        });
      }
    },
    byId: function* (action: any) {
      try {
        const id = action?.payload?.data ?? {};
        const { data, response } = yield call(API.inquery.self.byId, id);
        const { reg_no } = yield call(
          API.registration.registrationNumber.get,
          {}
        );

        yield put(Actions.inquery.byId.success(data));
        yield put(Actions.registration.registrationNumber.success(reg_no));
      } catch (error) {
        yield put(Actions.inquery.byId.fail(error));
        yield put(Actions.registration.registrationNumber.fail(error));
      }
    },
  },
  all: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.inquery.all.get, params);
        yield put(Actions.inquery.self.success(data));
      } catch (error) {
        yield put(Actions.inquery.self.fail(error));
        message.error({
          content: "Something wrong",
          key: "1",
          duration: 2,
        });
      }
    },
  },
  type: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.inquery.type.get, 1);
        yield put(Actions.inquery.type.success(data));
      } catch (error) {
        yield put(Actions.inquery.type.fail(error));
        message.error({
          content: "Something wrong",
          key: "5",
          duration: 2,
        });
      }
    },
  },
  edit: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};

        const { data } = yield call(API.inquery.type.get, 1);
        yield put(Actions.inquery.type.success(data));

        const course = yield call(API.course.self.get, params);
        yield put(Actions.course.self.success(course.data));

        yield put(
          Actions.inquery.edit.success({
            ...params,
            inquiry_type_id: parseInt(params.inquiry_type_id),
            course_id: parseInt(params.course_id),
          })
        );
      } catch (error) {
        yield put(Actions.inquery.edit.fail(error));
        message.error({
          content: "Something wrong",
          key: "6",
          duration: 2,
        });
      }
    },
  },
  reports: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.inquery.reports.get, params);
        yield put(Actions.inquery.reports.success(data));
      } catch (error) {
        yield put(Actions.inquery.reports.fail(error));
        message.error({
          content: "Something wrong",
          key: "7",
          duration: 2,
        });
      }
    },
  },
};

export default [
  takeLatest("GET_INQUIRY_DATA", InquerySagas.all.get),
  takeLatest("GET_INQUIRY_TYPE_DATA", InquerySagas.type.get),
  takeLatest("EDIT_INQUIRY_DATA", InquerySagas.edit.get),
  takeLatest("UPDATE_INQUIRY", InquerySagas.self.update),
  takeLatest("CREATE_INQUIRY", InquerySagas.self.create),
  takeLatest("GET_TRANSFER_DATA", InquerySagas.self.transfer.get),
  takeLatest("GET_TRANSFER_APPROVE_DATA", InquerySagas.self.transfer.approve),
  takeLatest("GET_REPORTS_DATA", InquerySagas.reports.get),
  takeLatest("GET_INQUIRY_BY_ID", InquerySagas.self.byId),
];
