import { take, put, call, select, takeLatest, all } from "redux-saga/effects";
import * as Actions from "Actions";
import * as API from "Services";
import configureStore, { history } from "store";
import { message } from "antd";

export const PaymentSaga = {
  all: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.all.get, params);
        yield put(Actions.payments.all.success(data));
      } catch (error) {
        yield put(Actions.payments.all.fail(error));
      }
    },
  },
  save: {
    post: function* (action: any) {
      try {
        message.loading("Payment is being saving");
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.save.post, params);
        yield put(Actions.payments.save.success(data));
        message.success("Payment saved successfully");
      } catch (error) {
        yield put(Actions.payments.save.fail(error));
        message.error("Payment saved unsuccessfully");
      }
    },
  },
  paymentsByRegId: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.paymentsByRegId.get, params);
        yield put(Actions.payments.paymentsByRegId.success(data));
      } catch (error) {
        yield put(Actions.payments.paymentsByRegId.fail(error));
        message.warn("Invalid Registration No");
      }
    },
  },
  currencies: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.allCurrencies.get, params);
        yield put(Actions.payments.currencies.success(data));
      } catch (error) {
        yield put(Actions.payments.currencies.fail(error));
      }
    },
  },
  busy: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const data = yield call(API.payment.busy.get, params);

        if (data) {
          yield put(Actions.payments.busy.success(data));
        } else {
          yield put(Actions.payments.busy.fail([]));
        }
      } catch (error) {
        yield put(Actions.payments.busy.fail(error));
      }
    },
  },
  bankAccounts: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.bankAccounts.get, params);

        yield put(Actions.payments.bankAccounts.success(data));
      } catch (error) {
        yield put(Actions.payments.bankAccounts.fail(error));
      }
    },
  },
  outstanding: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.outstanding.get, params);

        yield put(Actions.payments.outstanding.success(data));
      } catch (error) {
        yield put(Actions.payments.outstanding.fail(error));
      }
    },
    getStudents: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.payment.outstanding.getStudents, params);

        yield put(Actions.payments.outstandingStudent.success(data));
      } catch (error) {
        yield put(Actions.payments.outstandingStudent.fail(error));
      }
    },
  },
};

export default [
  takeLatest("GET_PAYMENTS", PaymentSaga.all.get),
  takeLatest("SAVE_PAYMENT", PaymentSaga.save.post),
  takeLatest("GET_PAYMENTS_BY_REG_ID", PaymentSaga.paymentsByRegId.get),
  takeLatest("GET_BUSY_PAYMENTS", PaymentSaga.busy.get),
  takeLatest("GET_BANK_ACCOUNTS", PaymentSaga.bankAccounts.get),
  takeLatest("GET_ALL_CURRENCIES", PaymentSaga.currencies.get),
  takeLatest("GET_OUTSTANDING", PaymentSaga.outstanding.get),
  takeLatest("GET_OUTSTANDING_STUDENT", PaymentSaga.outstanding.getStudents),
];
