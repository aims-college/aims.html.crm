import { take, put, call, select, takeLatest, all } from "redux-saga/effects";
import * as Actions from "Actions";
import * as API from "Services";
import { message } from "antd";

export const RegistrationSagas = {
  discounts: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.registration.discounts.get, params);
        yield put(Actions.registration.discounts.success(data));
      } catch (error) {
        yield put(Actions.registration.discounts.fail(error));
      }
    },
  },
  registrationNumber: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data, reg_no } = yield call(
          API.registration.registrationNumber.get,
          params
        );
        yield put(Actions.registration.registrationNumber.success(reg_no));
      } catch (error) {
        yield put(Actions.registration.registrationNumber.fail(error));
      }
    },
  },
  save: {
    post: function* (action: any) {
      try {
        message.loading("Registration is being saving");
        const params = action?.payload?.data ?? {};
        const { student, payments } = yield call(
          API.registration.save.post,
          params
        );
        yield put(Actions.registration.save.success({ student, payments }));
        message.success("Registration Saved Successfully");
      } catch (error) {
        message.error("Registration Saved Unsuccessfully");
        yield put(Actions.registration.save.fail(error));
      }
    },
  },
  update: {
    post: function* (action: any) {
      try {
        message.loading("Registration is being Updating");
        const params = action?.payload?.data ?? {};
        const { student, payments } = yield call(
          API.registration.update.post,
          params
        );
        yield put(Actions.registration.update.success({ student, payments }));
        message.success("Registration Updated Successfully");
      } catch (error) {
        message.error("Registration Updated Unsuccessfully");
        yield put(Actions.registration.update.fail(error));
      }
    },
  },
  all: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.registration.all.get, params);
        yield put(Actions.registration.all.success(data));
      } catch (error) {
        yield put(Actions.registration.all.fail(error));
      }
    },
  },
  byId: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.registration.byId.get, params);
        yield put(Actions.registration.byId.success(data));
      } catch (error) {
        yield put(Actions.registration.byId.fail(error));
      }
    },
  },
  reports: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.registration.reports.get, params);
        yield put(Actions.registration.reports.success(data));
      } catch (error) {
        yield put(Actions.registration.reports.fail(error));
        message.error({
          content: "Something wrong",
          key: "7",
          duration: 2,
        });
      }
    },
  },
  busy: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const data = yield call(API.registration.busy.get, params);
        yield put(Actions.registration.busy.success(data));
      } catch (error) {
        yield put(Actions.registration.busy.fail(error));
      }
    },
  },
  busyInstallments: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const data = yield call(API.registration.busyInstallments.get, params);
        yield put(Actions.registration.busyInstallments.success(data));
      } catch (error) {
        yield put(Actions.registration.busyInstallments.fail(error));
      }
    },
  },
};

export default [
  takeLatest("GET_DISCOUNTS_DATA", RegistrationSagas.discounts.get),
  takeLatest(
    "GET_REGISTRATION_NUMBER_DATA",
    RegistrationSagas.registrationNumber.get
  ),
  takeLatest("SAVE_REGISTRATION", RegistrationSagas.save.post),
  takeLatest("UPDATE_REGISTRATION", RegistrationSagas.update.post),
  takeLatest("GET_REGISTRATIONS", RegistrationSagas.all.get),
  takeLatest("GET_REGISTRATION_BY_ID", RegistrationSagas.byId.get),
  takeLatest("GET_REGISTRATION_BY_ID", RegistrationSagas.byId.get),
  takeLatest("GET_REGISTRATION_REPORTS_DATA", RegistrationSagas.reports.get),
  takeLatest("GET_BUSY_REGISTRATIONS", RegistrationSagas.busy.get),
  takeLatest("GET_BUSY_INSTALLMENTS", RegistrationSagas.busyInstallments.get),
];
