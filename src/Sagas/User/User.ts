import { take, put, call, select, takeLatest, all } from "redux-saga/effects";
import * as Actions from "Actions";
import * as API from "Services";
import configureStore, { history } from "store";
import {message} from 'antd'

export const UsersSagas = {
  users: {
    get: function* (action: any) {
      try {
        const params = action?.payload?.data ?? {};
        const { data } = yield call(API.user.users.get, params);
        yield put(Actions.user.users.success(data));
      } catch (error) {
        yield put(Actions.user.users.fail(error));
      }
    },
  },
  login: {
    post: function* (action: any) {
      try {
        message.loading("please wait...");
        const params = action?.payload?.data ?? {};
        const { token, id, f_name, l_name } = yield call(API.user.login.post, params);
        yield put(Actions.user.login.success(token));
        localStorage.setItem("token", token);
        localStorage.setItem("user_id", id);
        localStorage.setItem("user_name", `${f_name} ${l_name}`);
        window.location.pathname = '/home'

      } catch (error) {
        message.error("Something wrong!");
        yield put(Actions.user.login.fail(error));
      }
    },
  },
};

export default [
  takeLatest("GET_USERS_DATA", UsersSagas.users.get),
  takeLatest("POST_LOGIN", UsersSagas.login.post),
];
