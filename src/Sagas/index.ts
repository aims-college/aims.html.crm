import { InquerySagas } from "./Inquery";
import { CourseSagas } from "./Course";
import { UserSagas } from "./User";
import { RegistrationSagas } from "./Registration";
import { BranchSagas } from "./Branch";
import { PaymentSagas } from "./Payments";

export default [
  ...InquerySagas,
  ...CourseSagas,
  ...UserSagas,
  ...RegistrationSagas,
  ...BranchSagas,
  ...PaymentSagas
];
