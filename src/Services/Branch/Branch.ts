import { AzureHttp } from "Helpers";

export default {
  self: {
    get: (params: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get("get_branches", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
};
