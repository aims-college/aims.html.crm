import { AzureHttp } from "Helpers";

export default {
  self: {
    get: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get("get_courses", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  type: {
    get: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get("get_course_types", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  faculty: {
    get: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get("get_faculties", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  create: {
    post: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("create_course", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  edit: {
    post: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("update_course", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
};
