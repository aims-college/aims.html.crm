import { AzureHttp } from "Helpers";

export default {
  self: {
    get: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "get_available_discounts",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  create: {
    post: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("create_discount", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  update: {
    post: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("update_discount", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
};
