import { AzureHttp } from "Helpers";

export default {
    self:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_inquiries_by_user",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
      update: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.post(
              "update_inquiry",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
      create: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.post(
              "create_inquiry",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
      byId: (id: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.getById(
              "get_inquiry_by_id",
              id
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
    },
    all: {
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_all_inquiries",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
    },
    type:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_inquiry_types",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
    },
    transfer:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_tranfer_inquiries_by_user",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
    },
    approve:{
      post: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.post(
              "approve_inquiry_transfer_request",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
    },
    reports:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_inquiry_reports",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
    }
};