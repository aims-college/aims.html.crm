import { AzureHttp } from "Helpers";

export default {
  all:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_all_payments",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    save:{
      post: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.post(
              "add_student_payment",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    paymentsByRegId:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_payments_by_student",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    allCurrencies:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_all_currencies",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    busy:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "export_payments",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    bankAccounts:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_all_bank_accounts",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    outstanding:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_outstanding_report",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      },
      getStudents: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_outstanding_student_report",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    }
};