import { AzureHttp } from "Helpers";

export default {
  discounts: {
    get: (params: number): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "get_available_discounts",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  registrationNumber: {
    get: (params: {}): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "generate_registration_number",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  save: {
    post: (params: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("register_new_student", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  update: {
    post: (params: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("update_student", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },

  all: {
    get: (params: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "get_all_student_registrations",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  byId: {
    get: (id: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.getById(
            "get_student_registration_by_id",
            id
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  applyForApproval: {
    post: (params: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post(
            "create_discount_approval",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  approval: {
    get: (): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get("get_discount_approvals", {});
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  approvedOrRejectedDiscount: {
    post: (params: any): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.post("approve_discount_approvals", params);
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  checkDiscountApproval: {
    get: (id: number): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.getById("get_discount_approval_status", id);
        
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  reports:{
    get: (params: number): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "get_student_registration_reports",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  busy:{
    get: (params: number): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "export_registrations",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    }
  },
  busyInstallments:{
    get: (params: number): Promise<any> => {
      return new Promise<any>(async (resolve, reject) => {
        try {
          const { data } = await AzureHttp.get(
            "export_installment_plans",
            params
          );
          resolve(data);
        } catch (error) {
          reject(error);
        }
      });
    }
  },
};
