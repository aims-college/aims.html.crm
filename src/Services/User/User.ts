import { AzureHttp } from "Helpers";

export default {
  users:{
      get: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.get(
              "get_all_users",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    },
    login:{
      post: (params: number): Promise<any> => {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const { data } = await AzureHttp.post(
              "login",
              params
            );
            resolve(data);
          } catch (error) {
            reject(error);
          }
        });
      }
    }
};