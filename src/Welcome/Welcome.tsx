import React, { useEffect, useState } from "react";
import { Login } from "Login";
import { WelcomeHome } from "Home";

const Welcome = () => {
  const [state, setState] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setState(true);
    }, 3000);
  }, []);

  return (
    <>
      {!state && <WelcomeHome />} {state && <Login />}
    </>
  );
};

export default Welcome;
