import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import * as Reducers from 'Reducers';

import { History } from 'history'

const reducers = {
  ...Reducers
}

const rootReducer = (history: History) => combineReducers({
  router: connectRouter(history),
  ...reducers
})

export default rootReducer;