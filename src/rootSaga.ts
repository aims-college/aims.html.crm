import { all } from 'redux-saga/effects';
import Sagas from 'Sagas'

let sagas: any[] = [
 ...Sagas
];

function* rootSaga() {
  yield all(sagas);
}

export default rootSaga;